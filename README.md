Make Widget

# Install
To install the project go into `cd client` folder and execute `yarn install`

After installation you can execute `yarn start` the default browser will open the homepage of the widget the url will be `http://localhost:3000`

If you want to run the application with the server (along with server-side rendering, proxy...) first run `yarn build` then `yarn server`.

You can access the widget from `http://localhost:9009`.

FYI: you will need to set at least one GET parameter: `operation_id`.

# Deploy

# Test

# Contribute
