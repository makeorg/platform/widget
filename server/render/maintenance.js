const fs = require('fs');
const path = require('path');
import { PUBLIC_DIR } from '../paths';

const maintenancefileData = fs.readFileSync(
  path.join(PUBLIC_DIR + '/maintenance.html'),
  'utf8'
);

export function maintenanceRender(req, res, next) {
  try {
    res.send(maintenancefileData);
  } catch (error) {
    res.status(404).send('We are in maintenance');
  }
}
