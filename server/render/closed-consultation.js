import path from 'path';
import fs from 'fs';
import { BUILD_DIR } from '../paths';

export const closedConsultationHtml = async (res, question) => {
  try {
    const closedfileData = fs.readFileSync(
      path.join(BUILD_DIR, 'closed-consultation.html'),
      'utf8'
    );

    const renderedHtml = closedfileData
      .replace('___QUESTION___', question.question)
      .replace('___ABOUT_URL___', question.aboutUrl);

    return res.send(renderedHtml);
  } catch (err) {
    console.error('read err', err);
    return res.status(404).end();
  }
};
