const fs = require('fs');
const path = require('path');
import { BUILD_DIR } from '../paths';

const versionData = fs.readFileSync(path.join(BUILD_DIR + '/version'), 'utf8');

export function versionRender(req, res, next) {
  try {
    res.json(JSON.parse(versionData));
  } catch (error) {
    res.status(404).send('Version file not found');
  }
}

export function robotRender(req, res) {
  const robotContent = 'User-agent: *\nDisallow: /';
  res.type('text/plain');
  res.send(robotContent);
}
