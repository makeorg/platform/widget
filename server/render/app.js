import path from 'path';
import fs from 'fs';
import React from 'react';
import { configureStore } from '../../src/shared/store';
import { renderStylesToString } from 'emotion-server';
import configuration from '../configuration';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import App from '../../src/app';
import { BUILD_DIR } from '../paths';

export const appHtml = async (res, initialState) => {
  fs.readFile(path.join(BUILD_DIR, 'index.html'), 'utf8', (err, indexHtml) => {
    if (err) {
      console.error('read err', err);
      res.status(404).end();
    }

    const store = configureStore(initialState);

    const ReactApp = (
      <Provider store={store}>
        <App />
      </Provider>
    );

    const html = renderStylesToString(renderToString(ReactApp));

    const RenderedApp = indexHtml
      .replace('__SSR__', html)
      .replace('"__REDUX__"', JSON.stringify(initialState))
      .replace('__API_URL__', configuration.apiUrl);

    res.send(RenderedApp);
  });
};
