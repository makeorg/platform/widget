require('babel-register')({
  ignore: [/(node_modules)/],
  presets: ['es2015', 'react-app'],
  plugins: [
    'babel-plugin-emotion',
    [
      'transform-assets',
      {
        extensions: ['svg', 'png'],
        name: 'static/media/[name].[hash:8].[ext]',
      },
    ],
    'css-modules-transform',
  ],
});

module.exports = require('./index.js');
