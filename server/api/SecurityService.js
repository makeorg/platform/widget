import axios from 'axios';

const PATH_POST_CHECK_HASH = '/security/secure-hash';

export const API_URL =
  process.env.API_URL || 'https://api.preprod.makeorg.tech';

export default class SecurityService {
  static checkSecureHash(hash, value) {
    return axios({
      method: 'POST',
      url: `${API_URL}${PATH_POST_CHECK_HASH}`,
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      data: JSON.stringify({
        hash,
        value,
      }),
    })
      .then(response => response.status)
      .catch(error => {
        throw error;
      });
  }
}
