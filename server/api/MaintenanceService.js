import axios from 'axios';

export default class MaintenanceService {
  static getMaintenanceConfig() {
    return axios({
      method: 'GET',
      url: `${process.env.CONFIG_URL}`,
    })
      .then(response => response.data)
      .catch(error => {
        throw Error(
          `maintenance service error when calling ${process.env.CONFIG_URL}`
        );
      });
  }
}
