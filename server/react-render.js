import i18next from '../src/shared/i18n';
import QuestionService from '../src/api/QuestionService';
import { closedConsultationHtml } from './render/closed-consultation';
import { appHtml } from './render/app';
import {
  DEFAULT_SOURCE,
  DEFAULT_COLOR,
  DEFAULT_FONT,
} from '../src/constants/enum';
import ApiService from '../src/api/ApiService';
import { TRANSLATION_NAMESPACE } from '../src/shared/constants/env';

async function getQuestion(questionSlug) {
  return QuestionService.getDetail(questionSlug);
}

const isInProgress = (startDate, endDate) => {
  const today = new Date();
  const end = new Date(endDate);
  const start = new Date(startDate);

  return start <= today && today < end;
};

module.exports = async function reactRender(req, res, next) {
  const { questionSlug } = req.query;

  if (!questionSlug) {
    console.log(
      `Error getting questionSlug from OriginalUrl ${req.originalUrl}`
    );
    return res.status(404).send('Not found');
  }

  const colorArray = req.query.color ? req.query.color.split(':') : [];
  const themeColor =
    colorArray.length > 0
      ? `rgba(${Number.parseInt(colorArray[0])},${Number.parseInt(
          colorArray[1]
        )},${Number.parseInt(colorArray[2])},${Number.parseInt(colorArray[3])})`
      : DEFAULT_COLOR;
  const tagsIds = !req.query.tagsIds ? [] : req.query.tagsIds;
  const country = req.query.country || 'FR';
  const language = req.query.language || 'fr';

  ApiService.language = language;
  ApiService.country = country;

  try {
    const question = await getQuestion(questionSlug);

    if (res.unsecure) {
      console.log(
        `Unsecure widget for "${questionSlug}" on source : "${
          req.query.source
        }"`
      );
      return closedConsultationHtml(res, question);
    }

    if (!isInProgress(question.startDate, question.endDate)) {
      return closedConsultationHtml(res, question);
    }

    const tradLanguage = `${language}-${country}`;

    i18next.changeLanguage(tradLanguage);

    const initialState = {
      config: {
        question,
        tagsIds,
        showPannel: false,
        title: req.query.title || '',
        source: req.query.source || DEFAULT_SOURCE,
        themeColor: themeColor,
        font: req.query.font || DEFAULT_FONT,
        country,
        language,
        socialConnect: req.query.socialConnect === 'false' ? false : true,
        translations: i18next.getResourceBundle(
          tradLanguage,
          TRANSLATION_NAMESPACE
        ),
      },
    };

    appHtml(res, initialState);
  } catch (e) {
    console.log(e);
    return res.status(400).send({
      error: e.message,
    });
  }
};
