module.exports = {
  apiUrl: process.env.API_URL || 'https://api.preprod.makeorg.tech',
  port: process.env.PORT || 9009,
  host: process.env.HOST || 'localhost',
};
