const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const serveStatic = require('serve-static');
const { BUILD_DIR, PUBLIC_DIR } = require('./paths');

require('./browser-polyfill');
const reactRender = require('./react-render');
const { maintenanceRender } = require('./render/maintenance');
const technical = require('./render/technical');

const { secureMiddleware } = require('./middleware/secure');
const { maintenanceMiddleware } = require('./middleware/maintenance');

// App
const app = express();
app.use(compression());

app.use(bodyParser.json());

app.get('/maintenance', maintenanceRender);
app.get('/version', technical.versionRender);
app.get('/robot.txt', technical.robotRender);
app.get('/', maintenanceMiddleware, secureMiddleware, reactRender);

// Static files
app.use(
  express.static(BUILD_DIR, {
    maxAge: '1y',
    setHeaders: setCustomCacheControl,
  })
);

app.use(
  express.static(PUBLIC_DIR, {
    maxAge: '1y',
    setHeaders: setCustomCacheControl,
  })
);

function setCustomCacheControl(res, path) {
  if (serveStatic.mime.lookup(path) === 'text/html') {
    // Custom Cache-Control for HTML files
    res.setHeader('Cache-Control', 'public, max-age=0');
  }
}

module.exports = app;
