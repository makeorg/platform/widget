import SecurityService from '../api/SecurityService';

export const secureMiddleware = async (req, res, next) => {
  const {
    hash,
    color,
    questionSlug,
    tagsIds,
    country,
    language,
    title,
    source,
    owner,
  } = req.query;

  if (!hash) {
    res.unsecure = true;
    return next();
  }

  // Should be the same encoder than widget generator
  const fixedEncodeURIComponent = str =>
    encodeURIComponent(str).replace(
      /[!'()*]/g,
      c => '%' + c.charCodeAt(0).toString(16)
    );

  const tags = tagsIds ? tagsIds.join('&tagsIds[]=') : '';
  const urlToCheck = `?questionSlug=${questionSlug}&tagsIds[]=${tags}&title=${fixedEncodeURIComponent(
    title
  )}&source=${source}&color=${color}&country=${country}&language=${language}&owner=${owner}`;

  try {
    const checkHashStatus = await SecurityService.checkSecureHash(
      hash,
      urlToCheck
    );

    if (checkHashStatus !== 204) {
      res.unsecure = true;
      return next();
    }

    next();
  } catch (error) {
    res.unsecure = true;
    return next();
  }
};
