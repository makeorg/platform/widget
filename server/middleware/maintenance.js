import MaintenanceService from '../api/MaintenanceService';

const cache = require('memory-cache');

const MAINTENACE_SERVICE_RESPONSE_CACHE_KEY =
  'MAINTENACE_SERVICE_RESPONSE_CACHE_KEY';

const getMaintenanceConfigResponse = async () => {
  const content = cache.get(MAINTENACE_SERVICE_RESPONSE_CACHE_KEY);

  if (content) {
    return content;
  }

  try {
    const response = await MaintenanceService.getMaintenanceConfig();
    cache.put(MAINTENACE_SERVICE_RESPONSE_CACHE_KEY, response, 300000);

    return response;
  } catch (error) {
    throw error;
  }
};

export const maintenanceMiddleware = async (req, res, next) => {
  const { source } = req.query;
  try {
    const maintenanceConfigResponse = await getMaintenanceConfigResponse();
    const { blockAll, blockedSources } = maintenanceConfigResponse;
    if (blockAll || blockedSources.includes(source)) {
      return res.redirect('/maintenance');
    }

    next();
  } catch (error) {
    console.error(error);
    next();
  }
};
