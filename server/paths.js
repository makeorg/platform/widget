const path = require('path');

const BUILD_DIR = path.resolve(__dirname, '..', 'build');
const PUBLIC_DIR = path.resolve(__dirname, '..', 'public');

module.exports = {
  BUILD_DIR,
  PUBLIC_DIR,
};
