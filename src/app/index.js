import React, { Component } from 'react';
import { connect } from 'react-redux';
import { injectGlobal } from 'emotion';
import {
  MainContent,
  InnerContent,
  ContentWrapper,
} from './Styled/MainElements';
import ProposalSubmitContainer from '../containers/ProposalSubmit';
import FooterComponent from '../components/FooterComponent';
import IntroComponent from '../components/IntroComponent';
import ProposalStackContainer from '../containers/ProposalStack';
import SlidingPannelComponent from '../components/SlidingPannelComponent';
import MakePresentationComponent from '../components/MakePresentationComponent';
import RegisterFormComponent from '../components/RegisterFormComponent';
import LoginFormComponent from '../components/LoginFormComponent';
import ForgotPasswordFormComponent from '../components/ForgotPasswordFormComponent';
import { ExpirationSession } from '../components/Session/Expiration';
import {
  PANNEL_RENDER_REGISTER,
  PANNEL_RENDER_LOGIN,
  PANNEL_RENDER_FORGOTPASSWORD,
  PANNEL_RENDER_PRESENTATION,
  PANNEL_RENDER_PRIVACY_POLICY,
  PANNEL_QUIT_PRIVACY_POLICY,
} from '../shared/constants/pannel';
import Tracking from '../services/Tracking';
import { Colors } from '../assets/styles/colors';
import { Webfonts } from '../assets/styles/webfonts';
import '../assets/styles/animations.css';
import '../assets/styles/link.css';
import {
  setSessionExpirationDate,
  setExpiredSession,
} from '../shared/store/actions/sessionAction';
import ApiService from '../api/ApiService';
import PrivacyPolicyComponent from '../components/PrivacyPolicyComponent';
import QuitPrivacyPolicyComponent from '../components/QuitPrivacyPolicyComponent';

const pannelContent = componentType => {
  switch (componentType) {
    case PANNEL_RENDER_PRESENTATION:
      return <MakePresentationComponent />;
    case PANNEL_RENDER_REGISTER:
      return <RegisterFormComponent />;
    case PANNEL_RENDER_LOGIN:
      return <LoginFormComponent />;
    case PANNEL_RENDER_FORGOTPASSWORD:
      return <ForgotPasswordFormComponent />;
    case PANNEL_RENDER_PRIVACY_POLICY:
      return <PrivacyPolicyComponent />;
    case PANNEL_QUIT_PRIVACY_POLICY:
      return <QuitPrivacyPolicyComponent />;
    default:
      return null;
  }
};

injectGlobal`
    @font-face {
        font-family: ${Webfonts.CircularStdBold.name};
        src: url(${Webfonts.CircularStdBold.woff2}) format('woff2'),
        url(${Webfonts.CircularStdBold.woff}) format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: ${Webfonts.CircularStdBook.name};
        src: url(${Webfonts.CircularStdBook.woff2}) format('woff2'),
        url(${Webfonts.CircularStdBook.woff}) format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: ${Webfonts.TradeGothicLTStd.name};
        src: url(${Webfonts.TradeGothicLTStd.woff2}) format('woff2'),
        url(${Webfonts.TradeGothicLTStd.woff}) format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }

    html,
    body,
    #root,
    .App {
        height: 100%;
        overflow: hidden;
    }

    body {
        background-color: rgba(0, 0, 0, 0.05);
    }

    .App {
        display: flex;
        flex-flow: column;
        justify-content: space-between;
        height: 100%;
    }

    body,
    input,
    select,
    button {
        font-family: ${Webfonts.CircularStdBold.family};
    }

    html,
    body,
    #root,
    .App,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    p,
    ul,
    li,
    button {
        padding: 0;
        margin: 0;
    }

    button,
    iframe {
        background: none;
        border: none;
    }

    button:hover,
    label:hover {
        cursor: pointer;
    }

    :focus {
        outline-color: ${Colors.MakeBlue};
    }

    button:disabled {
        cursor: not-allowed;
    }
`;

class App extends Component {
  constructor() {
    super();
    this.state = {
      isPannelDisplayed: false,
      pannelRenderComponentType: 'presentation',
    };
  }

  componentDidMount() {
    const { tagsIds, handleExpirationDate } = this.props;
    Tracking.trackDisplayIntro(tagsIds.join(', '));

    handleExpirationDate(ApiService.sessionIdExpiration);
    // set method to update Session Expiration
    ApiService.updateSessionExpiration = () =>
      handleExpirationDate(ApiService.sessionIdExpiration);
  }

  componentDidUpdate() {
    const {
      handleExpirationDate,
      handleExpiredSession,
      expirationDate,
    } = this.props;
    const storedDate = ApiService.sessionIdExpiration;
    const sessionExpirationDate = new Date(storedDate);
    const currentDate = new Date();
    const timeBeforeExpire =
      sessionExpirationDate.getTime() - currentDate.getTime();

    if (
      (typeof timeBeforeExpire === 'number' && isNaN(timeBeforeExpire)) ||
      timeBeforeExpire < 0
    ) {
      return undefined;
    }

    const timer = setTimeout(() => {
      if (expirationDate !== storedDate) {
        handleExpirationDate(storedDate);
      } else {
        handleExpiredSession();
      }
    }, timeBeforeExpire);

    return () => clearTimeout(timer);
  }

  render() {
    const {
      tagsIds,
      title,
      themeColor,
      pannelRender,
      isSequenceCollapsed,
      question,
      isExpired,
    } = this.props;
    const { isPannelDisplayed } = this.state;
    return (
      <React.Fragment>
        <div className="App">
          <MainContent>
            <InnerContent>
              <IntroComponent
                tagsIds={tagsIds}
                title={title}
                themeColor={themeColor}
                canPropose={question.canPropose}
              />
              {isExpired ? (
                <ExpirationSession />
              ) : (
                <React.Fragment>
                  {question.canPropose && <ProposalSubmitContainer />}
                  <ContentWrapper
                    className={isSequenceCollapsed ? 'locked' : ''}
                  >
                    <ProposalStackContainer
                      tagsIds={tagsIds}
                      toggleStack={this.toggleStack}
                    />
                  </ContentWrapper>
                </React.Fragment>
              )}
            </InnerContent>
            <FooterComponent isPannelDisplayed={isPannelDisplayed} />
          </MainContent>
          <SlidingPannelComponent isPannelDisplayed={isPannelDisplayed}>
            {pannelContent(pannelRender)}
          </SlidingPannelComponent>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { tagsIds, themeColor, title, pannelRender, question } = state.config;
  const { isSequenceCollapsed } = state.sequence;
  const { expirationDate, isExpired } = state.session;

  return {
    tagsIds,
    title,
    pannelRender,
    themeColor,
    isSequenceCollapsed,
    question,
    expirationDate,
    isExpired,
  };
}

const mapDispatchToProps = dispatch => ({
  handleExpirationDate: date => dispatch(setSessionExpirationDate(date)),
  handleExpiredSession: () => dispatch(setExpiredSession()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
