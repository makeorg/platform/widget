import styled from '@emotion/styled';
import { rem } from 'polished';

export const MainContent = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  overflow: hidden;
`;

export const InnerContent = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  height: calc(100vh - ${rem('44px')});
  width: 100%;
  overflow-y: auto;
`;

export const ContentWrapper = styled.div`
  position: relative;
  width: 100%;
  height: calc(100% - ${rem('44px')});
  min-height: ${rem('375px')};
`;
