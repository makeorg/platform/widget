export const CLICK_LEARN_MORE = 'click-learn-more';
export const CLICK_LEARN_MORE_VIDEO = 'click-learn-more-video';
export const CLICK_LEARN_MORE_LINK = 'click-learn-more-link';
export const CLICK_PROPOSAL_VOTE = 'click-proposal-vote';
export const CLICK_PROPOSAL_UNVOTE = 'click-proposal-unvote';
export const CLICK_PROPOSAL_QUALIFY = 'click-proposal-qualify';
export const CLICK_PROPOSAL_UNQUALIFY = 'click-proposal-unqualify';
export const CLICK_SEQUENCE_NEXT_CARD = 'click-sequence-next-card';
export const CLICK_SEQUENCE_PREVIOUS_CARD = 'click-sequence-previous-card';
export const CLICK_PROPOSAL_SUBMIT_FORM_OPEN =
  'click-proposal-submit-form-open';
export const CLICK_PROPOSAL_PUSH_CARD_IGNORE =
  'click-proposal-push-card-ignore';
export const CLICK_SIGN_UP_CARD_EMAIL = 'click-sign-up-card-email';
export const CLICK_SIGN_UP_CARD_FACEBOOK = 'click-sign-up-card-facebook';
export const CLICK_SIGN_UP_CARD_SIGN_IN = 'click-sign-up-card-sign-in';
export const CLICK_PROPOSAL_SUBMIT = 'click-proposal-submit';
export const DISPLAY_PROPOSAL_PUSH_CARD = 'display-proposal-push-card';
export const DISPLAY_INTRO = 'display-intro';
export const DISPLAY_SIGN_UP_CARD = 'display-sign-up-card';
export const DISPLAY_SIGN_UP_FORM = 'display-signup-form';
export const DISPLAY_SIGN_IN_FORM = 'display-signin-form';
export const DISPLAY_PROPOSAL_SUBMIT_JOURNEY =
  'display-proposal-submit-journey';
export const DISPLAY_PROPOSAL_SUBMIT_VALIDATION =
  'display-proposal-submit-validation';
export const AUTHEN_SOCIAL_SUCCESS = 'authen-social-success';
export const AUTHEN_SOCIAL_FAILURE = 'authen-social-failure';
export const SIGN_UP_EMAIL_SUCCESS = 'signup-email-success';
export const SIGN_UP_EMAIL_FAILURE = 'signup-email-failure';
export const SIGN_IN_EMAIL_SUCCESS = 'signin-email-success';
export const SIGN_IN_EMAIL_FAILURE = 'signin-email-failure';
export const SKIP_SIGN_UP_CARD = 'skip-sign-up-card';
export const DISPLAY_FINAL_CARD = 'display-finale-card';
/* Moderation Text */
export const DISPLAY_MODERATION_TEXT = 'display-moderation-text';
export const CLICK_MODERATION_LINK = 'click-moderation-link';
