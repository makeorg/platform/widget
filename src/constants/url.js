export const CGU_LINK = 'conditions-dutilisation';
export const DATA_POLICY_LINK = 'politique-donnees';
export const MODERATION_CHARTER_FR_LINK = 'https://about.make.org/moderation';
export const MODERATION_CHARTER_EN_LINK =
  'https://about.make.org/en/moderation';
export const LEGAL_NOTICE_LINK = 'legal-mentions';
export const CONTACT_LINK = 'contact';
