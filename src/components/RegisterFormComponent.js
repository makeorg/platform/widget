import React, { Component } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import MakeLogoWhite from '../assets/images/logo-white.svg';
import SlidingPannel from './SlidingPannel';
import Authentification from './Authentification';
import Form from './Form';
import { userActions } from '../shared/store/actions/userAction';
import { showLogin } from '../shared/store/actions/pannelAction';
import { errorTranslation } from '../helpers/errorHelper';
import * as Helpers from '../helpers/url';
import { throttle } from '../shared/helpers/throttle';
import LegalConsent from './LegalConsentComponent';

const fieldErrors = (field, errors) => {
  if (errors === undefined || !Array.isArray(errors)) {
    return null;
  }

  const error = errors.find(error => error.field === field);

  if (error === undefined) {
    return null;
  }

  return Object.keys(error).length === 0
    ? undefined
    : errorTranslation(error.message);
};

class RegisterFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: '',
        password: '',
        firstname: '',
        age: '',
        postalCode: '',
        legalMinorConsent: false,
        legalAdvisorApproval: false,
        approvePrivacyPolicy: false,
      },
      showPassword: false,
      needLegalConsent: false,
    };
    this.throttleSubmit = throttle(this.handleSubmit);
  }

  showLoginPannel = () => {
    this.props.dispatch(showLogin());
  };

  togglePasswordVisibility = () => {
    this.setState(prevState => {
      return {
        showPassword: !prevState.showPassword,
      };
    });
  };

  handleLegalField = (fieldName, value) => {
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [fieldName]: value,
      },
    });
  };

  handleChange = event => {
    const { id, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [id]: value,
      },
    });
  };

  handleCheck = () => {
    const { user } = this.state;
    this.setState(prevState => ({
      user: {
        ...user,
        approvePrivacyPolicy: !prevState.user.approvePrivacyPolicy,
      },
    }));
  };

  handleSubmit = () => {
    const { user } = this.state;
    const { dispatch } = this.props;

    dispatch(userActions.register(user));
    this.setState({ needLegalConsent: false });
  };

  toggleLegalConsent = event => {
    event.preventDefault();
    this.setState(prevState => {
      return {
        needLegalConsent: !prevState.needLegalConsent,
      };
    });
  };
  render() {
    const { user, showPassword, needLegalConsent } = this.state;
    const { errors } = this.props;
    const emailError = fieldErrors('email', errors);
    const passwordError = fieldErrors('password', errors);
    const firstnameError = fieldErrors('firstname', errors);
    const ageError = fieldErrors('dateOfBirth', errors);
    const postalCodeError = fieldErrors('postalCode', errors);
    const privacyPolicyError = fieldErrors('approvePrivacyPolicy', errors);
    const cguLink = Helpers.localizeCguLink();
    const userIsAChild = user.age && user.age < 15;
    const FORM_NAME = 'user_register';
    const privacyPolicyLink = Helpers.localizePrivacyPolicy();

    return (
      <React.Fragment>
        <LegalConsent
          needLegalConsent={needLegalConsent}
          toggleLegalConsent={this.toggleLegalConsent}
          handleSubmit={this.throttleSubmit}
          handleLegalField={this.handleLegalField}
        />
        <SlidingPannel.Wrapper className={needLegalConsent && 'hidden'}>
          <SlidingPannel.Logo src={MakeLogoWhite} alt="Make.org" />
          <SlidingPannel.Title>
            {i18next.t('register.intro')}
          </SlidingPannel.Title>
          <SlidingPannel.Sep />
          <SlidingPannel.Description>
            {i18next.t('register.title')}&nbsp;
          </SlidingPannel.Description>
          <Form
            id={FORM_NAME}
            name={FORM_NAME}
            onSubmit={
              userIsAChild ? this.toggleLegalConsent : this.throttleSubmit
            }
          >
            <Form.FormHeader>
              <SlidingPannel.TitleGrey>
                {i18next.t('register.register_with_social')}&nbsp;
                <Authentification.FacebookLoginLinkComponent />
                &nbsp;{i18next.t('common.or')}&nbsp;
                <Authentification.GoogleLoginLinkComponent />
              </SlidingPannel.TitleGrey>
              <SlidingPannel.SepWrapper>
                <SlidingPannel.LargeSep />
                <SlidingPannel.SepText>
                  {i18next.t('common.or')}
                </SlidingPannel.SepText>
                <SlidingPannel.LargeSep />
              </SlidingPannel.SepWrapper>
              <SlidingPannel.Title>
                {i18next.t('register.register_with_email')}
              </SlidingPannel.Title>
            </Form.FormHeader>
            <Form.FormInputsWrapper>
              <Form.InputWrapper hasError={emailError}>
                <Form.Label htmlFor="email">
                  <Form.EmailIcon />
                </Form.Label>
                <Form.Input
                  id="email"
                  value={user.email}
                  required="true"
                  placeholder={i18next.t('common.form.email_label')}
                  onChange={this.handleChange}
                />
              </Form.InputWrapper>
              {emailError && <Form.InputError>{emailError}</Form.InputError>}
              <Form.InputWrapper hasError={passwordError}>
                <Form.Label htmlFor="password">
                  <Form.PasswordIcon />
                </Form.Label>
                <Form.Input
                  id="password"
                  type={this.state.showPassword ? 'text' : 'password'}
                  value={user.password}
                  required="true"
                  placeholder={i18next.t('common.form.password_label')}
                  onChange={this.handleChange}
                />
                <Form.PasswordToggle
                  type="button"
                  onClick={this.togglePasswordVisibility}
                >
                  {showPassword ? (
                    <Form.ShowPasswordIcon />
                  ) : (
                    <Form.HidePasswordIcon />
                  )}
                </Form.PasswordToggle>
              </Form.InputWrapper>
              {passwordError && (
                <Form.InputError>{passwordError}</Form.InputError>
              )}
              <Form.InputWrapper hasError={firstnameError}>
                <Form.Label htmlFor="firstname">
                  <Form.FirstNameIcon />
                </Form.Label>
                <Form.Input
                  id="firstname"
                  value={user.firstname}
                  required="true"
                  placeholder={i18next.t('common.form.firstname_label')}
                  onChange={this.handleChange}
                />
              </Form.InputWrapper>
              {firstnameError && (
                <Form.InputError>{firstnameError}</Form.InputError>
              )}
              <Form.InputWrapper hasError={ageError}>
                <Form.Label htmlFor="age">
                  <Form.AgeIcon />
                </Form.Label>
                <Form.Input
                  id="age"
                  type="number"
                  min="8"
                  max="120"
                  value={user.age}
                  required="true"
                  placeholder={i18next.t('common.form.age_label')}
                  onChange={this.handleChange}
                />
              </Form.InputWrapper>
              {ageError && <Form.InputError>{ageError}</Form.InputError>}
              <Form.InputWrapper hasError={postalCodeError}>
                <Form.Label htmlFor="age">
                  <Form.PostalCodeIcon />
                </Form.Label>
                <Form.Input
                  id="postalCode"
                  type="text"
                  value={user.postalCode}
                  placeholder={i18next.t('common.form.postalcode_label')}
                  onChange={this.handleChange}
                  maxLength={5}
                  pattern="^[0-9]{5}"
                />
              </Form.InputWrapper>
              {postalCodeError && (
                <Form.InputError>{postalCodeError}</Form.InputError>
              )}
            </Form.FormInputsWrapper>
            <SlidingPannel.AlternativeDescription>
              {i18next.t('register.cgu_text_first_part')}
              <SlidingPannel.Link href={cguLink} target="_blank" rel="noopener">
                {i18next.t('register.cgu_link')}
              </SlidingPannel.Link>
              <Form.NewWindowWhiteIcon />
              {i18next.t('register.cgu_text_second_part')}
            </SlidingPannel.AlternativeDescription>
            <Form.CheckboxWrapper
              as="label"
              htmlFor="privacy_policy"
              hasError={privacyPolicyError}
            >
              <Form.HiddenCheckbox
                id="privacy_policy"
                required="true"
                checked={user.approvePrivacyPolicy}
                onChange={this.handleCheck}
                type="checkbox"
              />
              <Form.CheckboxStyle
                className="checkboxStyle"
                checked={user.approvePrivacyPolicy}
              >
                <Form.SvgCheck />
              </Form.CheckboxStyle>
              <Form.CheckboxText>
                {i18next.t('register.privacy_policy_text')}
                <SlidingPannel.Link
                  href={privacyPolicyLink}
                  target="_blank"
                  rel="noopener"
                >
                  {i18next.t('register.privacy_policy')}
                </SlidingPannel.Link>
                <Form.NewWindowWhiteIcon />
                {i18next.t('register.privacy_make')}
              </Form.CheckboxText>
            </Form.CheckboxWrapper>
            {privacyPolicyError && (
              <Form.InputError>{privacyPolicyError}</Form.InputError>
            )}
            <Form.SubmitButton type="submit" form={FORM_NAME}>
              <Form.OkIcon />
              {i18next.t('register.cta')}
            </Form.SubmitButton>
            <Form.ConnectionLink>
              {i18next.t('authentification.have_an_account')}{' '}
              <SlidingPannel.Link onClick={this.showLoginPannel}>
                {i18next.t('authentification.i_connect')}.
              </SlidingPannel.Link>
            </Form.ConnectionLink>
          </Form>
        </SlidingPannel.Wrapper>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { errors } = state.registration;
  return {
    errors,
  };
}

export default connect(mapStateToProps)(RegisterFormComponent);
