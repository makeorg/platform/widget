import styled from '@emotion/styled';
import { rem } from 'polished';
import { Webfonts } from '../../assets/styles/webfonts';
import { Colors } from '../../assets/styles/colors';

export const LegalConsentLogo = styled.img`
  display: block;
  margin: 0 auto 30px;
`;

export const LegalConsentTitle = styled.h2`
  color: ${Colors.PureBlack};
  text-transform: uppercase;
  font-family: ${Webfonts.TradeGothicLTStd.name};
  text-transform: uppercase;
  font-size: ${rem('16px')};
  text-align: center;
`;

export const LegalConsentParagraph = styled.p`
  width: 100%;
  font-family: ${Webfonts.CircularStdBook.name};
  color: ${Colors.PureBlack};
  text-align: center;
  font-size: ${rem('14px')};
  line-height: ${rem('24px')};
  margin-top: 10px;
  &.checkbox-wrapper {
    text-align: left;
    margin: 0 0 15px;
  }
  input {
    margin-right: 10px;
  }
`;

export const LegalConsentButtonWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const LegalConsentCancelButton = styled.button`
  font-family: ${Webfonts.CircularStdBook.name};
  text-decoration: underline;
  font-size: ${rem('14px')};
  color: rgb(119, 119, 119);
`;
