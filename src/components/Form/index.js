import styled from '@emotion/styled';

import { margin, rem } from 'polished';
import {
  FormInputsWrapper,
  FormInputsWrapperAlt,
  InputWrapper,
  Label,
  Input,
  InputError,
  PasswordToggle,
} from './TextInput';
import { SubmitButton } from './SubmitButton';
import {
  EmailIcon,
  FirstNameIcon,
  PasswordIcon,
  ShowPasswordIcon,
  HidePasswordIcon,
  OkIcon,
  AgeIcon,
  PostalCodeIcon,
  NewWindowWhiteIcon,
  NewWindowBlackIcon,
  SvgCheck,
} from './Icon';
import { Colors } from '../../assets/styles/colors';
import {
  LegalConsentLogo,
  LegalConsentTitle,
  LegalConsentParagraph,
  LegalConsentButtonWrapper,
  LegalConsentCancelButton,
} from './Legal';
import { Webfonts } from '../../assets/styles/webfonts';
import { Breakpoints } from '../../assets/styles/breakpoints';

const Form = styled.form`
  display: flex;
  flex-flow: column;
  align-items: center;
  width: 100%;
`;

const FormHeader = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  width: 100%;
  ${margin(rem('20px'), '')}
`;

const ConnectionLink = styled.div`
  font-size: ${rem('14px')};
  line-height: ${rem('24px')};
  color: rgba(255, 255, 255, 0.65);
  margin-top: ${rem('10px')};
  font-family: ${Webfonts.CircularStdBook.family};
`;

const CheckboxWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-bottom: ${rem('20px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    margin-bottom: ${rem('30px')};
  }
`;

const HiddenCheckbox = styled.input`
  // Hide checkbox visually but remain accessible to screen readers.
  // Source: https://polished.js.org/docs/#hidevisually
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
  &:focus + .checkboxStyle {
    box-shadow: 0 0 0 2px ${Colors.GreyBorders};
  }
`;

const CheckboxStyle = styled.div`
  display: inline-flex;
  position: relative;
  width: 16px;
  height: 16px;
  min-width: 16px;
  max-width: 16px;
  background-color: ${Colors.PureWhite};
  z-index: 0;
  flex: 1 1 auto;
  margin-top: ${rem('2px')};
  margin-right: ${rem('8px')};
  svg {
    fill: ${Colors.MakeRed};
    position: absolute;
    left: 10%;
    z-index: 1;
    visibility: ${props => (props.checked ? 'visible' : 'hidden')};
  }
`;

const CheckboxText = styled.p`
  font-size: ${rem('12px')};
  line-height: 2;
  color: rgba(255, 255, 255, 0.65);
  font-family: ${Webfonts.CircularStdBook.family};
`;

const FormErrors = styled.ul`
  color: red;
  font-size: ${rem('12px')};
  margin-top: ${rem('10px')};
  margin-bottom: ${rem('10px')};
  color: ${Colors.MakeRed};
`;

const FormError = styled.li`
  list-style: none;
`;

// FormHeader Component
Form.FormHeader = FormHeader;

//TextInput Component
Form.FormInputsWrapper = FormInputsWrapper;
Form.FormInputsWrapperAlt = FormInputsWrapperAlt;
Form.InputWrapper = InputWrapper;
Form.Label = Label;
Form.Input = Input;
Form.InputError = InputError;
Form.PasswordToggle = PasswordToggle;

//SubmitButton Component
Form.SubmitButton = SubmitButton;

Form.ConnectionLink = ConnectionLink;

// Checkbox Component
Form.CheckboxWrapper = CheckboxWrapper;
Form.HiddenCheckbox = HiddenCheckbox;
Form.CheckboxStyle = CheckboxStyle;
Form.CheckboxText = CheckboxText;

// Icon
Form.EmailIcon = EmailIcon;
Form.FirstNameIcon = FirstNameIcon;
Form.AgeIcon = AgeIcon;
Form.PasswordIcon = PasswordIcon;
Form.ShowPasswordIcon = ShowPasswordIcon;
Form.HidePasswordIcon = HidePasswordIcon;
Form.OkIcon = OkIcon;
Form.PostalCodeIcon = PostalCodeIcon;
Form.NewWindowWhiteIcon = NewWindowWhiteIcon;
Form.NewWindowBlackIcon = NewWindowBlackIcon;
Form.SvgCheck = SvgCheck;

// LegalConsent
Form.LegalConsentLogo = LegalConsentLogo;
Form.LegalConsentTitle = LegalConsentTitle;
Form.LegalConsentParagraph = LegalConsentParagraph;
Form.LegalConsentButtonWrapper = LegalConsentButtonWrapper;
Form.LegalConsentCancelButton = LegalConsentCancelButton;

// Errors Component
Form.FormErrors = FormErrors;
Form.FormError = FormError;

export default Form;
