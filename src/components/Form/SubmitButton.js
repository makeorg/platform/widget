import styled from '@emotion/styled';

import { size, rem, padding } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';

export const SubmitButton = styled.button`
  font-size: ${rem('14px')};
  background-color: ${Colors.MakeRed};
  font-family: ${Webfonts.TradeGothicLTStd.name};
  color: ${Colors.PureWhite};
  text-transform: uppercase;
  ${padding(rem('8px'), rem('15px'), rem('6px'))};
  border-radius: ${rem('20px')};
  &:disabled {
    background-color: ${Colors.GreyBorders};
    color: ${Colors.GreyDarkText};
  }
`;

export const SubmitIconSvg = styled.svg`
  ${size(rem('13px'), rem('13px'))}
  margin-right: ${rem('5px')};
`;

export const SubmitIconG = styled.g`
  ${size(rem('13px'), rem('13px'))}
`;

export const SubmitIconPath = styled.path`
  fill: ${Colors.PureWhite};
`;
