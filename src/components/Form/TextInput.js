import styled from '@emotion/styled';

import { size, rem, placeholder, textInputs } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';

export const FormInputsWrapper = styled.div`
  width: 100%;
  max-width: ${rem('370px')};
  text-align: center;
`;

export const FormInputsWrapperAlt = styled(FormInputsWrapper)`
  margin-top: ${rem('10px')};
`;

export const InputWrapper = styled.div`
  display: flex;
  background-color: ${Colors.PureWhite};
  border-radius: ${rem('30px')};
  padding: ${rem('5px')} ${rem('20px')};
  border: ${rem('1px')} solid
    ${props => (props.hasError ? '#ed1844' : Colors.GreyBorders)};
  margin-bottom: ${rem('10px')};
  > ${textInputs('focus')} {
    outline: none;
  }
`;

export const Label = styled.label`
  ${size(rem('20px'), rem('20px'))}
  margin-right: ${rem('15px')};
`;

export const LabelIconSvg = styled.svg`
  ${size(rem('20px'), rem('20px'))}
`;

export const LabelIconG = styled.g`
  ${size(rem('20px'), rem('20px'))}
`;

export const LabelIconPath = styled.path`
  fill: ${Colors.MakeRed};
`;

export const PasswordIconSvg = styled.svg`
  ${size(rem('14px'), rem('14px'))}
`;

export const PasswordIconG = styled.g`
  ${size(rem('14px'), rem('14px'))}
`;

export const PasswordIconShowPath = styled.path`
  fill: ${Colors.PureBlack};
`;

export const PasswordIconHidePath = styled.path`
  fill: ${Colors.GreyText};
`;

export const NewWindowInconSvg = styled.svg`
  ${size(rem('9px'), rem('9px'))};
  margin-left: ${rem('2px')};
`;

export const NewWindowWhiteIconPath = styled.path`
  fill: ${Colors.PureWhite};
`;

export const NewWindowBlackIconPath = styled.path`
  fill: ${Colors.PureBlack};
`;

export const CheckIconSvg = styled.svg`
  ${size(rem('14px'), rem('14px'))};
`;

export const CheckIconPath = styled.path`
  fill: ${Colors.MakeRed};
`;

export const PasswordToggle = styled.button`
  display: flex;
  align-items: center;
`;

export const Input = styled.input`
  ${size('', '100%')};
  ${placeholder({ color: Colors.GreyText, 'font-size': '14px' })};
  border: none;
  font-size: ${rem('14px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    font-size: ${rem('16px')};
    ${placeholder({ 'font-size': '16px' })};
  }
`;

export const InputError = styled.span`
  color: ${Colors.MakeRed};
`;
