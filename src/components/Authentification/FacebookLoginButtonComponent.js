import React from 'react';
import { FacebookButton, ButtonLabel } from './Button';
import { FacebookIcon } from './Icon';
import FacebookLoginComponent from './FacebookLoginComponent';

const renderFacebookLogin = renderProps => (
  <FacebookButton onClick={renderProps.onClick}>
    <FacebookIcon />
    <ButtonLabel>Facebook</ButtonLabel>
  </FacebookButton>
);

const FacebookLoginButtonComponent = props => {
  return <FacebookLoginComponent {...props} render={renderFacebookLogin} />;
};

export default FacebookLoginButtonComponent;
