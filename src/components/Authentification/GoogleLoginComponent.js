import React, { Component } from 'react';
import { connect } from 'react-redux';
import GoogleLogin from 'react-google-login';
import { GOOGLE_PROVIDER_ENUM } from '../../api/UserService';
import { userActions } from '../../shared/store/actions/userAction';

class GoogleLoginComponent extends Component {
  constructor(props) {
    super(props);

    this.handleGoogleLoginCallback = this.handleGoogleLoginCallback.bind(this);
  }

  handleGoogleLoginCallback = response => {
    const { dispatch, privacyPolicyDate } = this.props;

    dispatch(
      userActions.socialLoginWithPrivacyCheck(
        GOOGLE_PROVIDER_ENUM,
        response.accessToken,
        privacyPolicyDate
      )
    );
  };

  render() {
    return (
      <GoogleLogin
        {...this.props}
        clientId="810331964280-qtdupbrjusihad3b5da51i5p66qpmhmr.apps.googleusercontent.com"
        buttonText="Google +"
        style={{}}
        onSuccess={this.handleGoogleLoginCallback}
        onFailure={this.handleGoogleLoginCallback}
      >
        {this.props.children}
      </GoogleLogin>
    );
  }
}

function mapStateToProps(state) {
  const { privacyPolicyDate } = state.config;
  return {
    privacyPolicyDate,
  };
}

export default connect(mapStateToProps)(GoogleLoginComponent);
