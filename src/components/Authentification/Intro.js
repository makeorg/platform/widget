import styled from '@emotion/styled';
import { rem, margin, size } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';
import { Webfonts } from '../../assets/styles/webfonts';

export const Intro = styled.div`
  color: ${props => (props.GreyIntro ? Colors.GreyDarkText : Colors.PureBlack)};
  text-align: center;
  white-space: pre-wrap;
  font-family: ${Webfonts.CircularStdBook.family};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    font-size: ${rem('14px')};
  }
`;

export const IntroAlt = styled(Intro)`
  font-size: ${rem('14px')};
  font-family: ${Webfonts.CircularStdBold.family};
  color: ${props => (props.GreyIntro ? Colors.GreyDarkText : Colors.PureBlack)};
  text-align: center;
`;

export const IntroLink = styled.a`
  color: ${Colors.PureBlack};
  text-decoration: underline;
`;

export const IntroConnectLink = styled.button`
  color: ${Colors.ThemePrimaryColor};
  text-decoration: underline;
`;

export const IntroLinkIconSvg = styled.svg`
  ${size(rem('16px'), rem('16px'))};
  display: inline-block;
  vertical-align: middle;
  ${margin('', '', '', rem('5px'))}
`;

export const IntroLinkIconG = styled.g`
  ${size(rem('16px'), rem('16px'))};
`;

export const IntroLinkIconPath = styled.path`
  fill: ${Colors.PureBlack};
`;

export const IntroAuthentification = styled.h2`
  font-family: ${Webfonts.TradeGothicLTStd.family};
  font-size: ${rem('16px')};
  text-transform: uppercase;
`;

export const IntroPrivacy = styled.p`
  font-size: ${rem('14px')};
  color: ${Colors.GreyText};
`;

export const IntroPrivacyLink = styled.a`
  color: ${Colors.PureBlack};
  text-transform: uppercase;
`;
