import React, { Component } from 'react';
import { connect } from 'react-redux';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { FACEBOOK_PROVIDER_ENUM } from '../../api/UserService';
import { userActions } from '../../shared/store/actions/userAction';

class FacebookLoginComponent extends Component {
  constructor(props) {
    super(props);

    this.handleFacebookLoginCallback = this.handleFacebookLoginCallback.bind(
      this
    );
  }

  handleFacebookLoginCallback = response => {
    const { dispatch, privacyPolicyDate } = this.props;
    dispatch(
      userActions.socialLoginWithPrivacyCheck(
        FACEBOOK_PROVIDER_ENUM,
        response.accessToken,
        privacyPolicyDate
      )
    );
  };

  render() {
    return (
      <FacebookLogin
        {...this.props}
        appId={'317128238675603'}
        version={'10.0'}
        fields={'name,email,picture'}
        callback={this.handleFacebookLoginCallback}
        onClick={this.props.onClick}
        disableMobileRedirect={true}
      />
    );
  }
}

function mapStateToProps(state) {
  const { privacyPolicyDate } = state.config;
  return {
    privacyPolicyDate,
  };
}

export default connect(mapStateToProps)(FacebookLoginComponent);
