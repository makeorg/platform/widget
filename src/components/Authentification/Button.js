import styled from '@emotion/styled';
import { rem, size } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';
import { Webfonts } from '../../assets/styles/webfonts';

export const ButtonsWrapper = styled.div`
  display: flex;
  flex-flow: row;
  width: 100%;
  justify-content: center;
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    flex-flow: wrap;
  }
`;

export const ButtonsWrapperAlt = styled(ButtonsWrapper)`
  margin-top: ${rem('10px')};
  max-width: ${rem('180px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    justify-content: space-between;
    flex-flow: row;
    max-width: ${rem('300px')};
  }
`;

export const Button = styled.button`
  display: flex;
  font-size: ${rem('12px')};
  font-weight: bold;
  color: ${Colors.PureWhite};
  flex-flow: column;
  justify-content: center;
  text-transform: uppercase;
  width: ${rem('70px')};
  height: ${rem('70px')};
  border-radius: 50%;
  align-items: center;
  box-shadow: 0 1px 1px 0 ${Colors.GreyDarkText};
  font-family: ${Webfonts.TradeGothicLTStd.family};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    font-size: ${rem('14px')};
    flex-flow: row;
    height: 100%;
    width: auto;
    padding: ${rem('8px')} ${rem('20px')} ${rem('5px')};
    border-radius: ${rem('20px')};
    border-radius: ${rem('20px')};
  }
  @media (max-width: ${rem(Breakpoints.smallmobile)}) {
    width: ${rem('60px')};
    height: ${rem('60px')};
  }
`;

export const ButtonLabel = styled.span`
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    line-height: 0;
    transform: translateY(2px);
  }
`;

export const FacebookButton = styled(Button)`
  background-color: ${Colors.FacebookBlue};
`;

export const GoogleButton = styled(Button)`
  background-color: ${Colors.GoogleRed};
  margin: 0 ${rem('10px')};
`;

export const EmailButton = styled(Button)`
  background-color: ${Colors.ThemePrimaryColor};
`;

export const SocialLink = styled.span`
  font-size: ${rem('14px')};
  font-family: ${Webfonts.TradeGothicLTStd.family};
  color: ${Colors.PureWhite};
  text-transform: uppercase;
  text-decoration: underline;
`;

export const SocialIconSvg = styled.svg`
  ${size(rem('17px'), rem('17px'))};
  margin-bottom: ${rem('5px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    margin-bottom: ${rem('0px')};
    margin-right: ${rem('5px')};
  }
`;

export const SocialIconG = styled.g`
  ${size('100%', '100%')}
`;

export const SocialIconPath = styled.path`
  fill: ${Colors.PureWhite};
`;
