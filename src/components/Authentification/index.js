import styled from '@emotion/styled';
import { size, rem } from 'polished';
import {
  Intro,
  IntroAlt,
  IntroLink,
  IntroConnectLink,
  IntroLinkIconSvg,
  IntroLinkIconG,
  IntroLinkIconPath,
  IntroAuthentification,
  IntroPrivacy,
  IntroPrivacyLink,
} from './Intro';
import {
  ButtonsWrapper,
  ButtonsWrapperAlt,
  EmailButton,
  SocialIconSvg,
  SocialIconG,
  SocialIconPath,
  ButtonLabel,
} from './Button';
import { EmailIcon, IntroLinkIcon } from './Icon';
import FacebookLoginButtonComponent from './FacebookLoginButtonComponent';
import GoogleLoginButtonComponent from './GoogleLoginButtonComponent';
import FacebookLoginLinkComponent from './FacebookLoginLinkComponent';
import GoogleLoginLinkComponent from './GoogleLoginLinkComponent';

const Authentification = styled.section`
  ${size('100%', '100%')};
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-flow: column;
  transition: opacity ease-in 0.5s;
  margin-top: ${rem('10px')};
`;

const AuthnIntroAlt = styled(IntroAlt)`
  margin: ${rem('10px')} auto;
`;

// Intro Component
Authentification.Intro = Intro;
Authentification.AuthnIntroAlt = AuthnIntroAlt;
Authentification.IntroLink = IntroLink;
Authentification.IntroConnectLink = IntroConnectLink;
Authentification.IntroLinkIconSvg = IntroLinkIconSvg;
Authentification.IntroLinkIconG = IntroLinkIconG;
Authentification.IntroLinkIconPath = IntroLinkIconPath;
Authentification.IntroAuthentification = IntroAuthentification;
Authentification.IntroPrivacy = IntroPrivacy;
Authentification.IntroPrivacyLink = IntroPrivacyLink;

// Buttons component
Authentification.ButtonsWrapper = ButtonsWrapper;
Authentification.ButtonsWrapperAlt = ButtonsWrapperAlt;
Authentification.EmailButton = EmailButton;
Authentification.FacebookLoginButtonComponent = FacebookLoginButtonComponent;
Authentification.GoogleLoginButtonComponent = GoogleLoginButtonComponent;
Authentification.FacebookLoginLinkComponent = FacebookLoginLinkComponent;
Authentification.GoogleLoginLinkComponent = GoogleLoginLinkComponent;
Authentification.SocialIconSvg = SocialIconSvg;
Authentification.SocialIconG = SocialIconG;
Authentification.SocialIconPath = SocialIconPath;
Authentification.ButtonLabel = ButtonLabel;

// Icon Component
Authentification.EmailIcon = EmailIcon;
Authentification.IntroLinkIcon = IntroLinkIcon;

export default Authentification;
