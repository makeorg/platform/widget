import React from 'react';
import GoogleLoginComponent from './GoogleLoginComponent';
import { SocialLink } from './Button';

const GoogleLoginLinkComponent = props => {
  return (
    <GoogleLoginComponent {...props}>
      <SocialLink>Google</SocialLink>
    </GoogleLoginComponent>
  );
};

export default GoogleLoginLinkComponent;
