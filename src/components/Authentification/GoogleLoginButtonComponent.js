import React from 'react';
import { GoogleButton, ButtonLabel } from './Button';
import { GoogleIcon } from './Icon';
import GoogleLoginComponent from './GoogleLoginComponent';

const GoogleLoginButtonComponent = props => {
  return (
    <GoogleLoginComponent {...props} tag={GoogleButton}>
      <GoogleIcon />
      <ButtonLabel>Google</ButtonLabel>
    </GoogleLoginComponent>
  );
};

export default GoogleLoginButtonComponent;
