import React from 'react';
import { SocialLink } from './Button';
import FacebookLoginComponent from './FacebookLoginComponent';

const renderFacebookLogin = renderProps => (
  <SocialLink onClick={renderProps.onClick}>Facebook</SocialLink>
);

const FacebookLoginLinkComponent = props => {
  return <FacebookLoginComponent {...props} render={renderFacebookLogin} />;
};

export default FacebookLoginLinkComponent;
