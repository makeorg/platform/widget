import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import i18next from 'i18next';
import QualificationService from '../api/QualificationService';
import Qualification from './ProposalQualification';
import {
  userHasQualified,
  getQualificationCount,
} from '../helpers/qualificationHelper';
import Tracking from '../services/Tracking';
import { throttle } from '../shared/helpers/throttle';

export const doUnqualifying = (prevState, qualificationKey) => {
  const userQualifications = prevState.userQualifications.filter(
    qualification => qualification.qualificationKey !== qualificationKey
  );

  return {
    ...prevState,
    userQualifications: userQualifications,
  };
};

export const doQualifying = (prevState, qualifications) => {
  return {
    userQualifications: [...prevState.userQualifications, qualifications],
  };
};

/**
 * The Qualification component
 * @param       object props
 * @constructor
 */
class ProposalQualificationComponent extends Component {
  constructor(props) {
    const userQualifications = props.qualificationsResult.filter(
      qualification => qualification.hasQualified === true
    );
    super(props);
    this.state = {
      userQualifications: userQualifications,
    };

    this.throttleQualification = throttle(this.handleQualification);
  }

  componentDidMount() {
    this.setState({
      engageAnimation: true,
    });
  }

  /** Todo: refactor this method **/
  handleQualification = qualificationKey => {
    if (
      this.state.userQualifications.filter(
        qualification => qualification.qualificationKey === qualificationKey
      ).length > 0
    ) {
      const { proposalId, voteKey, cardIndex, proposalKey } = this.props;
      QualificationService.unqualify(
        proposalId,
        voteKey,
        qualificationKey,
        proposalKey
      ).then(
        result => {
          this.setState(prevState =>
            doUnqualifying(prevState, qualificationKey)
          );
          Tracking.trackUnqualify(
            proposalId,
            voteKey,
            qualificationKey,
            cardIndex
          );
        },
        error => {
          console.log('error when unqualifying:', error);
        }
      );
    } else {
      const { proposalId, voteKey, cardIndex, proposalKey } = this.props;
      QualificationService.qualify(
        proposalId,
        voteKey,
        qualificationKey,
        proposalKey
      ).then(
        result => {
          this.setState(prevState => doQualifying(prevState, result));
          Tracking.trackQualify(
            proposalId,
            voteKey,
            qualificationKey,
            cardIndex
          );
        },
        error => {
          console.log('error when qualifying:', error);
        }
      );
    }
  };

  render() {
    const { engageNextAnimation, qualificationsResult, color } = this.props;
    const { userQualifications, engageAnimation } = this.state;

    return (
      <Qualification>
        <CSSTransition
          in={engageAnimation}
          timeout={500}
          classNames="qualification"
          onEntered={engageNextAnimation}
        >
          <Qualification.QualificationList>
            {qualificationsResult.map(qualification => (
              <Qualification.QualificationListItem
                key={qualification.qualificationKey}
              >
                <Qualification.QualificationButton
                  qualified={userHasQualified(
                    userQualifications,
                    qualification.qualificationKey
                  )}
                  color={color}
                  onClick={() =>
                    this.throttleQualification(qualification.qualificationKey)
                  }
                >
                  <Qualification.QualificationLabel>
                    {i18next.t(
                      `qualification.${qualification.qualificationKey}`
                    )}
                  </Qualification.QualificationLabel>
                  <Qualification.QualificationCounter>
                    {getQualificationCount(
                      userQualifications,
                      qualification.qualificationKey
                    )}
                  </Qualification.QualificationCounter>
                </Qualification.QualificationButton>
              </Qualification.QualificationListItem>
            ))}
          </Qualification.QualificationList>
        </CSSTransition>
      </Qualification>
    );
  }
}

ProposalQualificationComponent.propTypes = {
  qualificationsResult: PropTypes.array.isRequired,
  voteKey: PropTypes.string.isRequired,
  proposalId: PropTypes.string.isRequired,
  cardIndex: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  proposalKey: PropTypes.string.isRequired,
};

export default ProposalQualificationComponent;
