import styled from '@emotion/styled';

import { rem } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';

export const UserInformations = styled.div`
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: ${rem('12px')};
  color: ${Colors.GreyText};
`;

export const UserProposal = styled.h2`
  font-size: ${rem('14px')};
  line-height: 1.31;
  text-align: center;
  max-width: 100%;
`;
