import styled from '@emotion/styled';

import { size, rem } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';

export const CounterWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  ${size(rem('46px'), rem('46px'))};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    ${size(rem('56px'), rem('56px'))};
  }
`;

export const Counter = styled.div`
  position: relative;
  font-size: ${rem('12px')};
  color: ${Colors.GreyText};
`;

export const ActiveCard = styled.span`
  font-size: ${rem('14px')};
  color: ${Colors.PureBlack};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    font-size: ${rem('16px')};
  }
`;
