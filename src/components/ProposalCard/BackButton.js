import styled from '@emotion/styled';

export const BackButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  top: 50%;
  left: 0;
  z-index: 1;
  transform: translateY(-50%);
`;

export const BackButton = styled.button``;
