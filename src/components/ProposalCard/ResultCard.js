import styled from '@emotion/styled';

import { rem } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';
import { Webfonts } from '../../assets/styles/webfonts';

export const ResultTitle = styled.h2`
  font-family: ${Webfonts.TradeGothicLTStd.family};
  font-size: ${rem('16px')};
  color: ${Colors.PureBlack};
  text-transform: uppercase;
  margin-bottom: ${rem('20px')};
`;

export const ResultDescriptionWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-flow: row;
  align-items: center;
`;

export const ResultDescription = styled.div`
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: ${rem('14px')};
  color: ${Colors.GreyText};
  width: 100%;
  max-width: ${rem('350px')};
`;

export const ResultDescriptionAlt = styled.p`
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: ${rem('14px')};
  color: ${Colors.GreyText};
  line-height: 1.5;
  text-align: center;
  > span {
    font-family: ${Webfonts.CircularStdBold.family};
    color: ${props => props.themeColor};
  }
`;

export const ResultPercent = styled.div`
  font-size: ${rem('44px')};
  color: ${props => props.themeColor};
`;

export const ResultRegisterTitle = styled.div`
  font-size: ${rem('16px')};
  font-family: ${Webfonts.TradeGothicLTStd.family};
  text-transform: uppercase;
  text-align: center;
  line-height: 1.7;
  max-width: ${rem('400px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    margin-top: ${rem('20px')};
    margin-bottom: ${rem('10px')};
  }
`;

export const ResultConnectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  max-width: ${rem('400px')};
  margin-top: ${rem('20px')};
`;

export const Link = styled.a`
  text-decoration: underline;
  color: ${Colors.ThemePrimaryColor};
`;
