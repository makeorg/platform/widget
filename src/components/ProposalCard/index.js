import styled from '@emotion/styled';

import { rem, padding, size, margin } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { BackButtonWrapper, BackButton } from './BackButton';
import { BackIcon } from './Icon';
import { UserInformations, UserProposal } from './UserInformations';
import {
  Link,
  ResultTitle,
  ResultDescription,
  ResultDescriptionAlt,
  ResultPercent,
  ResultDescriptionWrapper,
  ResultRegisterTitle,
  ResultConnectWrapper,
} from './ResultCard';

const ProposalCard = styled.article`
  position: relative;
  display: flex;
  flex-flow: column;
  align-items: center;
  height: calc(100% - ${rem('40px')});
  ${padding(rem('20px'))};
`;

const Header = styled.header`
  display: flex;
  position: relative;
  z-index: 0;
  width: calc(100% - ${rem('40px')});
  justify-content: center;
  padding: 0 ${rem('20px')};
`;

const Separator = styled.div`
  ${size(rem('2px'), rem('60px'))};
  background-color: ${Colors.GreyBorders};
  ${margin(rem('10px'), '', rem('15px'))};
`;

// BackButton Elements
ProposalCard.BackButtonWrapper = BackButtonWrapper;
ProposalCard.BackButton = BackButton;
ProposalCard.BackIcon = BackIcon;

// User infromations Elements
ProposalCard.UserInformations = UserInformations;
ProposalCard.UserProposal = UserProposal;

// Content Elements
ProposalCard.Header = Header;
ProposalCard.Separator = Separator;

// Result Card Elements
ProposalCard.ResultTitle = ResultTitle;
ProposalCard.ResultDescriptionWrapper = ResultDescriptionWrapper;
ProposalCard.ResultDescription = ResultDescription;
ProposalCard.ResultDescriptionAlt = ResultDescriptionAlt;
ProposalCard.ResultPercent = ResultPercent;
ProposalCard.ResultRegisterTitle = ResultRegisterTitle;
ProposalCard.ResultConnectWrapper = ResultConnectWrapper;
ProposalCard.Link = Link;

export default ProposalCard;
