import styled from '@emotion/styled';
import { size, rem, padding } from 'polished';
import { Webfonts } from '../../assets/styles/webfonts';

const CookieBanner = styled.div`
  position: relative;
  z-index: 100;
  font-size: ${rem('12px')};
  font-family: ${Webfonts.CircularStdBook.name};
  ${size('', '100%')};
  ${padding(rem('5px'), rem('10px'))};
  box-sizing: border-box;
  background-color: #c2c2c2;
  color: rgba(0, 0, 0, 0.65);
  line-height: 1.5;
  text-align: center;
  a {
    font-size: ${rem('12px')};
    color: rgba(0, 0, 0, 0.65);
    font-family: ${Webfonts.CircularStdBook.name};
    text-decoration: underline;
  }
`;

export default CookieBanner;
