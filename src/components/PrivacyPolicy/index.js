import styled from '@emotion/styled';
import { Webfonts } from '../../assets/styles/webfonts';
import { padding, rem } from 'polished';
import { Colors } from '../../assets/styles/colors';

const TitleTextStyle = styled.h2`
  color: ${Colors.PureWhite};
  font-family: ${Webfonts.CircularStdBold.family};
  font-size: ${rem('18px')};
  margin-bottom: 10px;
`;

const BodyTextStyle = styled.p`
  color: ${Colors.PureWhite};
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: ${rem('16px')};
  margin-bottom: 10px;
`;

const PrivacyPolicyLink = styled.a`
  color: ${Colors.PureWhite};
  font-family: ${Webfonts.CircularStdBook};
  font-size: ${rem('16px')};
  text-decoration: underline;
`;

const PrivacyPolicy = styled.div`
  width: 80%;
  height: auto;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: 4em;
`;

const PrivacyPolicyConsentSubmitWrapper = styled.div`
  display: flex;
  flex-direction: row-reverse;
  width: 100%;
`;

const PrivacyPolicyCheckboxText = styled.p`
  font-size: ${rem('16px')};
  color: ${Colors.PureWhite};
  font-family: ${Webfonts.CircularStdBook.family};
`;

const PrivacyStayButton = styled.button`
  font-size: ${rem('16px')};
  background-color: ${Colors.MakeRed};
  font-family: ${Webfonts.TradeGothicLTStd.name};
  color: ${Colors.PureWhite};
  text-transform: uppercase;
  ${padding(rem('8px'), rem('15px'), rem('6px'))};
  border-radius: ${rem('20px')};
`;

const PrivacyQuitButton = styled.button`
  font-size: ${rem('16px')};
  background-color: ${Colors.PureWhite};
  font-family: ${Webfonts.TradeGothicLTStd.name};
  color: ${Colors.PureBlack};
  text-transform: uppercase;
  ${padding(rem('8px'), rem('15px'), rem('6px'))};
  border-radius: ${rem('20px')};
  margin-right: ${rem('16px')};
`;

PrivacyPolicy.Title = TitleTextStyle;
PrivacyPolicy.Description = BodyTextStyle;
PrivacyPolicy.Link = PrivacyPolicyLink;
PrivacyPolicy.CheckboxText = PrivacyPolicyCheckboxText;
PrivacyPolicy.SubmitButtonWrapper = PrivacyPolicyConsentSubmitWrapper;
PrivacyPolicy.PrivacyStayButton = PrivacyStayButton;
PrivacyPolicy.PrivacyQuitButton = PrivacyQuitButton;
export default PrivacyPolicy;
