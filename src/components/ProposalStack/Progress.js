import styled from '@emotion/styled';
import { rem } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';

export const WrapperProgress = styled.div`
  position: relative;
  z-index: 70;
  background: rgb(216, 216, 216);
  border-radius: ${rem('12px')};
  display: block;
  width: ${rem('6px')};
  height: calc(100% - ${rem('15px')});
  margin-left: ${rem('15px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    margin-left: ${rem('25px')};
  }
`;

export const ProgressBar = styled.div`
  position: absolute;
  z-index: 75;
  top: 0;
  left: 0;
  border-radius: ${rem('12px')};
  background: ${Colors.MakeBlue};
  display: block;
  width: ${rem('6px')};
  height: ${props => props.progressPercent}%;
  max-height: 100%;
`;

export const ProgressTooltip = styled.div`
  position: absolute;
  top: ${props => props.progressPercent}%;
  right: ${rem('10px')};
  transform: translateY(-50%);
  z-index: 75;
  background: rgb(64, 64, 64);
  color: ${Colors.PureWhite};
  font-size: ${rem('12px')};
  padding: ${rem('5px')} ${rem('10px')};
  border-radius: ${rem('10px')};
  ::before {
    content: '';
    position: absolute;
    z-index: 75;
    top: 50%;
    right: ${rem('-3px')};
    width: ${rem('6px')};
    height: ${rem('6px')};
    transform: translateY(-50%) rotate(-135deg);
    background-color: rgb(64, 64, 64);
  }
`;
