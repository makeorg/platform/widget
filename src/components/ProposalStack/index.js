import styled from '@emotion/styled';
import { size, rem, transitions } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { BackArrowButton, BackButton } from './BackButton';
import { BackArrowIcon } from './Icon';
import { WrapperProgress, ProgressBar, ProgressTooltip } from './Progress';
import { Breakpoints } from '../../assets/styles/breakpoints';

const ProposalList = styled.div`
  display: flex;
  position: relative;
  z-index: 0;
  width: 100%;
  height: 100%;
  transition: transform linear 0.5s;
`;

const ListWrapper = styled.div`
  position: relative;
  display: flex;
  width: calc(100% - ${rem('30px')});
  max-width: ${rem(Breakpoints.appMaxWidth)};
  margin: 0 auto;
  overflow: hidden;
  height: calc(100% - ${rem('30px')});
  padding: ${rem('30px')} ${rem('15px')} 0;
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    padding: ${rem('30px')} ${rem('30px')} 0;
    width: calc(100% - ${rem('60px')});
  }
`;

const List = styled.ul`
  position: relative;
  width: 100%;
  height: 100%;
`;

const ProposalItem = styled.li`
  ${size('100%', '100%')};
  list-style: none;
  background-color: ${Colors.PureWhite};
  box-shadow: 0 0 2px 0 ${Colors.GreyText};
  border: 1px solid transparent;
  position: absolute;
  left: 0;
  bottom: 0;
  z-index: ${props => props.index || 0};
  transform: scaleX(${props => props.scaling || 0})
    translateY(-${props => props.position || 0}px);
  ${transitions('transform linear 0.75s')}
`;

//ProposalList Component
ProposalList.ListWrapper = ListWrapper;
ProposalList.List = List;
ProposalList.ProposalItem = ProposalItem;

//BackArrowButton Component
ProposalList.BackArrowButton = BackArrowButton;
ProposalList.BackArrowIcon = BackArrowIcon;
ProposalList.BackButton = BackButton;

//Progress Bar
ProposalList.WrapperProgress = WrapperProgress;
ProposalList.ProgressBar = ProgressBar;
ProposalList.ProgressTooltip = ProgressTooltip;

export default ProposalList;
