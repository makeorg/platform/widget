import styled from '@emotion/styled';
import { size, rem, padding } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';

export const BackArrowButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  ${size(rem('63px'), rem('63px'))};
  border-radius: 50%;
  position: absolute;
  z-index: 1;
  top: 0;
  left: 50%;
  background-color: ${Colors.GreyExtraLight};
  transform: translate(-50%, -100%);
  opacity: ${props => (props.displayBackButton ? 1 : 0)};
  visibility: ${props => (props.displayBackButton ? 'visible' : 'hidden')};
`;

export const BackButton = styled.button`
  position: absolute;
  top: -1%;
  left: 50%;
  transform: translateX(-50%);
  font-family: ${Webfonts.TradeGothicLTStd.family};
  color: ${Colors.PureWhite};
  font-size: ${rem('14px')};
  border-radius: ${rem('20px')};
  text-transform: uppercase;
  opacity: ${props => (props.displayBackButton ? 1 : 0)};
  visibility: ${props => (props.displayBackButton ? 'visible' : 'hidden')};
  width: 100%;
  max-width: ${rem('215px')};
  ${padding(rem('11px'), rem('30px'), rem('5px'))};
  background-color: ${Colors.MakeRed};
  z-index: 100;
`;
