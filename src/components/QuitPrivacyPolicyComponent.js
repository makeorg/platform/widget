import React, { Component } from 'react';
import Form from './Form';
import PrivacyPolicy from './PrivacyPolicy';
import i18next from 'i18next';
import { connect } from 'react-redux';
import * as Helpers from '../helpers/url';
import {
  closePannel,
  showPrivacyPolicy,
} from '../shared/store/actions/pannelAction';

class QuitPrivacyPolicyComponent extends Component {
  constructor(props) {
    super(props);

    this.closePannel = this.closePannel.bind(this);
    this.displayPolicy = this.displayPolicy.bind(this);
  }

  closePannel() {
    this.props.dispatch(closePannel());
  }

  displayPolicy() {
    const { dispatch, extraProps } = this.props;
    return dispatch(showPrivacyPolicy(extraProps));
  }

  render() {
    const privacyPolicyLink = Helpers.localizePrivacyPolicy();

    return (
      <PrivacyPolicy>
        <PrivacyPolicy.Title>
          {i18next.t('quit_privacy_policy.title')}
        </PrivacyPolicy.Title>
        <PrivacyPolicy.Description>
          {i18next.t('quit_privacy_policy.description')}{' '}
          <PrivacyPolicy.Link
            href={privacyPolicyLink}
            target="_blank"
            rel="noopener"
          >
            {i18next.t('quit_privacy_policy.link')}
          </PrivacyPolicy.Link>
          <Form.NewWindowWhiteIcon />
          {i18next.t('quit_privacy_policy.description_second')}
        </PrivacyPolicy.Description>
        <PrivacyPolicy.SubmitButtonWrapper>
          <PrivacyPolicy.PrivacyStayButton onClick={this.displayPolicy}>
            {i18next.t('quit_privacy_policy.stay')}
          </PrivacyPolicy.PrivacyStayButton>
          <PrivacyPolicy.PrivacyQuitButton onClick={this.closePannel}>
            {i18next.t('quit_privacy_policy.quit')}
          </PrivacyPolicy.PrivacyQuitButton>
        </PrivacyPolicy.SubmitButtonWrapper>
      </PrivacyPolicy>
    );
  }
}

function mapStateToProps(state) {
  const { extraProps } = state.config;
  return {
    extraProps,
  };
}

export default connect(mapStateToProps)(QuitPrivacyPolicyComponent);
