import styled from '@emotion/styled';

import { size, rem, margin } from 'polished';
import { Webfonts } from '../../assets/styles/webfonts';
import { Colors } from '../../assets/styles/colors';

export const Title = styled.h2`
  color: ${Colors.PureWhite};
  text-transform: uppercase;
  font-family: ${Webfonts.TradeGothicLTStd.name};
  text-transform: uppercase;
  font-size: ${rem('14px')};
  line-height: ${rem('17px')};
`;

export const TitleGrey = styled(Title)`
  color: rgba(255, 255, 255, 0.65);
`;

export const SepWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  ${margin(rem('10px'), '')}
  width: 100%;
`;

export const Sep = styled.div`
  background-color: rgba(255, 255, 255, 0.65);
  ${size(rem('2px'), rem('58px'))};
  ${margin(rem('15px'), '')};
  &.grey {
    background-color: rgb(148, 148, 148);
  }
  &.adjusted-margins {
    ${margin(rem('30px'), '')};
  }
`;

export const LargeSep = styled(Sep)`
  ${size(rem('2px'), rem('165px'))};
  ${margin('0', '')};
`;

export const SepText = styled.p`
  font-size: ${rem('14px')};
  color: ${Colors.PureWhite};
  ${margin('', rem('10px'))};
  text-transform: uppercase;
`;
