import styled from '@emotion/styled';

import { rem, padding, transitions } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Breakpoints } from '../../assets/styles/breakpoints';
import { CloseButton } from './CloseButton';
import { Logo } from './Logo';
import { Title, TitleGrey, SepWrapper, Sep, LargeSep, SepText } from './Title';
import { Description, AlternativeDescription } from './Description';
import { ResponsiveVideoContainer, ResponsiveVideoIframe } from './Video';
import { Link } from './Link';
import { LinkIcon, CloseIcon } from './Icon';

const SlidingPannel = styled.div`
  position: fixed;
  bottom: ${rem('-15px')};
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  height: 100%;
  background-color: ${Colors.MakeBlue};
  box-shadow: 1px 1px 10px ${Colors.PureBlack};
  z-index: 100;
  transform: translateY(${props => props.pannel || 100}%);
  ${transitions('transform  ease-in 0.5s')};
  overflow-y: auto;
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    left: ${rem('15px')};
    width: calc(100% - ${rem('30px')});
    z-index: 1;
  }
`;

export const WhiteInner = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  min-height: 455px;
  background-color: ${Colors.PureWhite};
  &.hidden {
    display: none;
    visibility: hidden;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  max-width: ${rem(Breakpoints.appMaxWidth)};
  width: calc(100% - ${rem('30px')});
  ${padding(rem('15px'))};
  &.hidden {
    display: none;
    visibility: hidden;
  }
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    ${padding(rem('30px'))};
    width: calc(100% - ${rem('60px')});
  }
`;

// MakePresentation Component
SlidingPannel.WhiteInner = WhiteInner;
SlidingPannel.Wrapper = Wrapper;

// Logo Component
SlidingPannel.Logo = Logo;

// Title Component
SlidingPannel.Title = Title;
SlidingPannel.TitleGrey = TitleGrey;
SlidingPannel.SepWrapper = SepWrapper;
SlidingPannel.Sep = Sep;
SlidingPannel.LargeSep = LargeSep;
SlidingPannel.SepText = SepText;

// Description Component
SlidingPannel.Description = Description;

// Description Component
SlidingPannel.Description = Description;
SlidingPannel.AlternativeDescription = AlternativeDescription;

// Video Component
SlidingPannel.ResponsiveVideoContainer = ResponsiveVideoContainer;
SlidingPannel.ResponsiveVideoIframe = ResponsiveVideoIframe;

// Link Component
SlidingPannel.Link = Link;
SlidingPannel.LinkIcon = LinkIcon;

// CloseButton Component
SlidingPannel.CloseButton = CloseButton;
SlidingPannel.CloseIcon = CloseIcon;

export default SlidingPannel;
