import styled from '@emotion/styled';
import { size, rem, margin, position } from 'polished';

export const ResponsiveVideoContainer = styled.div`
  position: relative;
  padding-bottom: 56.25%;
  ${size('0', '100%')}
  overflow: hidden;
`;

export const ResponsiveVideoIframe = styled.iframe`
  position: absolute;
  ${position('0', '', '', '0')};
  ${size('100%', '100%')};
  ${margin(rem('5px'), '')}
`;
