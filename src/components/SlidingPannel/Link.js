import styled from '@emotion/styled';
import { Colors } from '../../assets/styles/colors';

export const Link = styled.a`
  color: ${Colors.PureWhite};
  text-decoration: underline;
  cursor: pointer;
  &:focus {
    outline-color: ${Colors.PureWhite};
  }
`;
