import styled from '@emotion/styled';

import { size, rem, margin } from 'polished';

export const Logo = styled.img`
  ${size('', rem('58px'))};
  ${margin('', '', rem('15px'), '')};
`;
