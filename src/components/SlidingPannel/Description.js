import styled from '@emotion/styled';
import { Webfonts } from '../../assets/styles/webfonts';

import { rem } from 'polished';

export const Description = styled.p`
  font-size: ${rem('14px')};
  font-family: ${Webfonts.CircularStdBook.family};
  line-height: ${rem('24px')};
  color: rgba(255, 255, 255, 0.65);
  align-self: flex-start;
`;

export const AlternativeDescription = styled(Description)`
  font-size: ${rem('12px')};
  font-family: ${Webfonts.CircularStdBook.family};
  line-height: 1.33;
  margin: ${rem('15px')} 0;
`;
