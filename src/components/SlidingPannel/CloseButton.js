import styled from '@emotion/styled';
import { size, rem, position } from 'polished';
import { Colors } from '../../assets/styles/colors';

export const CloseButton = styled.button`
  position: absolute;
  ${position(rem('20px'), rem('20px'), '', '')};
  ${size(rem('20px'), rem('20px'))}
  &:focus {
    outline-color: ${Colors.PureWhite};
  }
`;
