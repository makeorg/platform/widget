import React, { Component } from 'react';
import Form from './Form';
import PrivacyPolicy from './PrivacyPolicy';
import i18next from 'i18next';
import { connect } from 'react-redux';
import { userActions } from '../shared/store/actions/userAction';
import { throttle } from '../shared/helpers/throttle';
import * as Helpers from '../helpers/url';

class PrivacyPolicyComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isChecked: false,
    };

    this.throttleSubmit = throttle(this.handleSubmit);
  }

  handleCheck = () => {
    const { isChecked } = this.state;

    this.setState({
      isChecked: !isChecked,
    });
  };

  handleSubmit = () => {
    const { extraProps, dispatch } = this.props;
    if (extraProps.isSocial) {
      dispatch(
        userActions.loginSocial(
          extraProps.provider,
          extraProps.token,
          extraProps.isSocial
        )
      );
    } else {
      dispatch(
        userActions.login(
          extraProps.email,
          extraProps.password,
          extraProps.isSocial
        )
      );
    }
  };

  render() {
    const { isChecked } = this.state;
    const FORM_NAME = 'privacy_policy_form';
    const privacyPolicyLink = Helpers.localizePrivacyPolicy();

    return (
      <PrivacyPolicy>
        <PrivacyPolicy.Title>
          {i18next.t('privacy_policy_consent.title')}
        </PrivacyPolicy.Title>
        <PrivacyPolicy.Description>
          {i18next.t('privacy_policy_consent.description')}{' '}
          <PrivacyPolicy.Link
            href={privacyPolicyLink}
            target="_blank"
            rel="noopener"
          >
            {i18next.t('privacy_policy_consent.description_link')}
          </PrivacyPolicy.Link>
          <Form.NewWindowWhiteIcon />.
        </PrivacyPolicy.Description>
        <Form id={FORM_NAME} name={FORM_NAME} onSubmit={this.throttleSubmit}>
          <Form.CheckboxWrapper as="label" htmlFor="login_privacy_policy">
            <Form.HiddenCheckbox
              id="login_privacy_policy"
              required="true"
              checked={isChecked}
              onChange={this.handleCheck}
              type="checkbox"
            />
            <Form.CheckboxStyle className="checkboxStyle" checked={isChecked}>
              <Form.SvgCheck />
            </Form.CheckboxStyle>
            <PrivacyPolicy.CheckboxText>
              {i18next.t('privacy_policy_consent.checkbox_text')}
            </PrivacyPolicy.CheckboxText>
          </Form.CheckboxWrapper>
          <PrivacyPolicy.SubmitButtonWrapper>
            <Form.SubmitButton
              type="submit"
              form={FORM_NAME}
              disabled={!isChecked}
            >
              {i18next.t('privacy_policy_consent.submit_button')}
            </Form.SubmitButton>
          </PrivacyPolicy.SubmitButtonWrapper>
        </Form>
      </PrivacyPolicy>
    );
  }
}

function mapStateToProps(state) {
  const { extraProps } = state.config;
  return {
    extraProps,
  };
}

export default connect(mapStateToProps)(PrivacyPolicyComponent);
