import React from 'react';
import i18next from 'i18next';
import ProposalSubmit from './ProposalSubmit/Styled';
import { getBaitText } from '../shared/constants/proposal';

export const ProposalBait = () => <span>{getBaitText()}</span>;
export const ProposalLength = ({ length }) => (
  <ProposalSubmit.Length>{length}/140</ProposalSubmit.Length>
);

const ProposalSubmitComponent = ({
  isTyping,
  content,
  length,
  canSubmit,
  handleChange,
  handleFocus,
  handleBlur,
  handleSubmit,
}) => {
  return (
    <ProposalSubmit>
      <ProposalSubmit.Form onSubmit={handleSubmit}>
        {(content || isTyping) && <ProposalBait />}
        <input
          id="proposal"
          placeholder={isTyping || content ? '' : getBaitText()}
          type="text"
          value={content}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
        <ProposalLength length={length} />
        <ProposalSubmit.Button type="submit" disabled={!canSubmit}>
          {i18next.t('proposal_submit.button')}
        </ProposalSubmit.Button>
      </ProposalSubmit.Form>
    </ProposalSubmit>
  );
};

export default ProposalSubmitComponent;
