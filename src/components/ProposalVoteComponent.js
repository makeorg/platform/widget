import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import VoteService from '../api/VoteService';
import ProposalResultsVoteComponent from './ProposalResultsVoteComponent';
import ProposalVote from './ProposalVote';
import { vote, unvote } from '../shared/store/actions/voteAction';
import Tracking from '../services/Tracking';
import { throttle } from '../shared/helpers/throttle';
import {
  VOTE_AGREE_KEY,
  VOTE_DISAGREE_KEY,
  VOTE_NEUTRAL_KEY,
  COLOR_AGREE,
  COLOR_DISAGREE,
  COLOR_NEUTRAL,
  ORIENTATION_AGREE,
  ORIENTATION_DISAGREE,
  ORIENTATION_NEUTRAL,
} from '../constants/enum';

/**
 * The agree vote button component
 * @param       obejct props
 * @constructor
 */
class ProposalVoteComponent extends Component {
  constructor(props) {
    super(props);

    const userVote = props.proposalVotes.find(vote => vote.hasVoted === true);

    this.state = {
      hasVoted: userVote && userVote.hasVoted,
      proposalVotes: props.proposalVotes,
      userVoteResult: userVote,
      voteKeyColor: '',
      voteKeyOrientation: '',
    };
    this.throttleVote = throttle(this.handleVote);
  }

  componentDidMount() {
    const userVotes = this.props.proposalVotes.filter(
      vote => vote.hasVoted === true
    );
    if (userVotes.length > 0) {
      this.handleVote(vote(this.props.proposalId, this.props.proposalVotes));
    }
  }

  handleVote = (voteKey, voteKeyColor, voteKeyOrientation) => {
    const {
      proposalVotes,
      proposalId,
      proposalKey,
      handleUnvoteAction,
      handleVoteAction,
    } = this.props;

    if (this.state.hasVoted) {
      VoteService.unvote(proposalId, voteKey, proposalKey)
        .then(result => {
          const updatedVotes = proposalVotes.map(vote => {
            return vote.voteKey === result.voteKey ? result : vote;
          });

          this.setState({
            hasVoted: false,
            proposalVotes: updatedVotes,
            userVoteResult: result,
            voteKeyColor: '',
            voteKeyOrientation: '',
          });
          handleUnvoteAction(proposalId);
          Tracking.trackUnvote(proposalId, voteKey, this.props.cardIndex);
        })
        .catch(error => {
          console.log('error when unvoting:', error);
        });
    } else {
      // Do vote
      VoteService.vote(proposalId, voteKey, proposalKey)
        .then(result => {
          const updatedVotes = proposalVotes.map(vote => {
            return vote.voteKey === result.voteKey ? result : vote;
          });

          this.setState({
            hasVoted: true,
            proposalVotes: updatedVotes,
            userVoteResult: result,
            selectedVoteKey: voteKey,
            voteKeyColor: voteKeyColor,
            voteKeyOrientation: voteKeyOrientation,
          });
          handleVoteAction(proposalId, updatedVotes);
          Tracking.trackVote(proposalId, voteKey, this.props.cardIndex);
        })
        .catch(error => {
          console.log('error when voting:', error);
        });
    }
  };

  render() {
    const {
      proposalId,
      cardIndex,
      decrementCardCounter,
      proposalKey,
    } = this.props;

    if (this.state.hasVoted) {
      return (
        <ProposalResultsVoteComponent
          proposalVotes={this.state.proposalVotes}
          decrementCardCounter={decrementCardCounter}
          userVoteResult={this.state.userVoteResult}
          onHandleUnvote={this.throttleVote}
          proposalId={proposalId}
          cardIndex={cardIndex}
          proposalKey={proposalKey}
        />
      );
    }

    return (
      <ProposalVote>
        <ProposalVote.VoteButton
          color={COLOR_AGREE}
          onClick={() =>
            this.throttleVote(VOTE_AGREE_KEY, COLOR_AGREE, ORIENTATION_AGREE)
          }
        >
          <ProposalVote.VoteIcon
            color={COLOR_AGREE}
            orientation={ORIENTATION_AGREE}
          />
        </ProposalVote.VoteButton>
        <ProposalVote.VoteButton
          color={COLOR_DISAGREE}
          onClick={() =>
            this.throttleVote(
              VOTE_DISAGREE_KEY,
              COLOR_DISAGREE,
              ORIENTATION_DISAGREE
            )
          }
        >
          <ProposalVote.VoteIcon
            color={COLOR_DISAGREE}
            orientation={ORIENTATION_DISAGREE}
          />
        </ProposalVote.VoteButton>
        <ProposalVote.VoteButton
          color={COLOR_NEUTRAL}
          onClick={() =>
            this.throttleVote(
              VOTE_NEUTRAL_KEY,
              COLOR_NEUTRAL,
              ORIENTATION_NEUTRAL
            )
          }
        >
          <ProposalVote.VoteIcon
            color={COLOR_NEUTRAL}
            orientation={ORIENTATION_NEUTRAL}
          />
        </ProposalVote.VoteButton>
      </ProposalVote>
    );
  }
}

ProposalVoteComponent.propTypes = {
  proposalVotes: PropTypes.array.isRequired,
  proposalId: PropTypes.string.isRequired,
  cardIndex: PropTypes.number.isRequired,
  decrementCardCounter: PropTypes.func.isRequired,
  proposalKey: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  const { expirationDate } = state.session;

  return {
    expirationDate,
  };
}

const mapDispatchToProps = dispatch => ({
  handleVoteAction: (proposalId, updatedVotes) =>
    dispatch(vote(proposalId, updatedVotes)),
  handleUnvoteAction: proposalId => dispatch(unvote(proposalId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalVoteComponent);
