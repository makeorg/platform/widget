import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import Authentification from './Authentification';

class AuthentificationComponent extends Component {
  render() {
    return (
      <Authentification>
        <Authentification.Intro>
          <Authentification.IntroAuthentification>
            {i18next.t('authentification.create_account')}
          </Authentification.IntroAuthentification>
        </Authentification.Intro>
        <Authentification.AuthnIntroAlt GreyIntro>
          {i18next.t('authentification.registration')}
        </Authentification.AuthnIntroAlt>
        <Authentification.ButtonsWrapper>
          {this.props.socialConnect && (
            <Fragment>
              <Authentification.FacebookLoginButtonComponent />
              <Authentification.GoogleLoginButtonComponent />
            </Fragment>
          )}
          <Authentification.EmailButton onClick={this.props.handleShowRegister}>
            <Authentification.EmailIcon />
            <Authentification.ButtonLabel>
              {i18next.t('common.email')}
            </Authentification.ButtonLabel>
          </Authentification.EmailButton>
        </Authentification.ButtonsWrapper>
        <Authentification.AuthnIntroAlt GreyIntro>
          {i18next.t('authentification.have_an_account')}
          &nbsp;
          <Authentification.IntroConnectLink
            onClick={this.props.handleShowLogin}
          >
            {i18next.t('authentification.i_connect')}
          </Authentification.IntroConnectLink>
        </Authentification.AuthnIntroAlt>
      </Authentification>
    );
  }
}

function mapStateToProps(state) {
  const { socialConnect } = state.config;
  return {
    socialConnect,
  };
}

export default connect(mapStateToProps)(AuthentificationComponent);
