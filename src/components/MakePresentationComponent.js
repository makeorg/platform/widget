import React, { Component } from 'react';
import i18next from 'i18next';
import MakeLogoWhite from '../assets/images/logo-white.svg';
import SlidingPannel from './SlidingPannel';
import Tracking from '../services/Tracking';

class MakePresentationComponent extends Component {
  constructor(props) {
    super(props);

    this.clickWhoAre = this.clickWhoAre.bind(this);
  }

  clickWhoAre() {
    Tracking.trackClickLearnMoreLink();
  }

  render() {
    return (
      <SlidingPannel.Wrapper>
        <SlidingPannel.Logo src={MakeLogoWhite} alt="Make.org" />
        <SlidingPannel.Title>
          {i18next.t('presentational.title')}
        </SlidingPannel.Title>
        <SlidingPannel.Sep />
        <SlidingPannel.Description>
          {i18next.t('presentational.description1')}
        </SlidingPannel.Description>
        <SlidingPannel.Description>
          {i18next.t('presentational.description2')}
        </SlidingPannel.Description>
        <SlidingPannel.Description>
          {i18next.t('presentational.description3')}
        </SlidingPannel.Description>
      </SlidingPannel.Wrapper>
    );
  }
}

export default MakePresentationComponent;
