import React, { Component } from 'react';
import styled from '@emotion/styled';

import PropTypes from 'prop-types';
import i18next from 'i18next';
import TagService from '../api/TagService';
import { rem } from 'polished';
import { Breakpoints } from '../assets/styles/breakpoints';
import { Webfonts } from '../assets/styles/webfonts';

export const IntroWrapper = styled.header`
  width: calc(100% - ${rem('30px')});
  max-width: ${rem(Breakpoints.appMaxWidth)};
  margin: 0 auto;
  padding: 0 ${rem('15px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    padding: 0 ${rem('30px')};
    width: calc(100% - ${rem('60px')});
  }
`;

export const Intro = styled.p`
  font-size: ${rem('20px')};
  line-height: 1.18;
  color: ${props => props.themeColor};
  margin-top: ${rem('5px')};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    font-size: ${rem('30px')};
    line-height: 1.13;
  }
`;

export const TagLink = styled.a`
  text-decoration: underline;
`;

export const Title = styled.h1`
  font-family: ${Webfonts.TradeGothicLTStd.family};
  text-transform: uppercase;
  font-size: ${rem('12px')};
  line-height: 1.43;
  margin: ${rem('5px')} 0 ${rem('10px')};
`;

export const IntroTitle = ({ title, tags }) => {
  if (title !== '') {
    return title;
  }

  return (
    <span>
      {i18next.t('intro.go_ahead')} <TagLink>{tags}</TagLink>
    </span>
  );
};

class IntroComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tags: [],
    };
  }

  componentDidMount() {
    const { tagsIds } = this.props;
    if (tagsIds.length > 0) {
      return TagService.getById(tagsIds[0])
        .then(response => {
          this.setState(prevState => {
            return {
              tags: [...prevState.tags, ...[response]],
            };
          });
        })
        .catch(error => error);
    }
  }

  render() {
    const { title, themeColor, canPropose } = this.props;
    const tags = this.state.tags.map(tag => tag.label).join(', ');

    return (
      <IntroWrapper>
        <Intro themeColor={themeColor}>
          <IntroTitle tags={tags} title={title} />
        </Intro>
        {canPropose ? (
          <Title>{i18next.t('intro.title')}</Title>
        ) : (
          <Title>{i18next.t('intro.vote_only')}</Title>
        )}
      </IntroWrapper>
    );
  }
}

IntroComponent.propTypes = {
  tagsIds: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  themeColor: PropTypes.string.isRequired,
  canPropose: PropTypes.bool.isRequired,
};

export default IntroComponent;
