import styled from '@emotion/styled';

import { size, rem, margin, padding } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';

export const VoteDataWrapper = styled.div`
  display: flex;
  flex-flow: column;
  margin-right: ${rem('10px')};
`;

export const VoteChart = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  ${margin(rem('10px'), '', '', '')};
`;

export const VoteBarWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  ${size(rem('30px'), rem('6px'))};
  ${margin('', rem('2px'), '', rem('2px'))}
`;

export const AgreeBar = styled.div`
  width: 100%;
  height: ${props => props.percent}%;
  min-height: ${rem('5px')};
  background: ${Colors.GreenVote};
`;

export const DisagreeBar = styled.div`
  width: 100%;
  height: ${props => props.percent}%;
  min-height: ${rem('5px')};
  background: ${Colors.RedVote};
`;

export const NeutralBar = styled.div`
  width: 100%;
  height: ${props => props.percent}%;
  min-height: ${rem('5px')};
  background: ${Colors.GreyVote};
`;

export const VoteCounter = styled.p`
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: ${rem('10px')};
  text-align: center;
  font-weight: bold;
  color: ${Colors.GreyText};
  ${margin(rem('5px'), '', '', '')};
`;

export const ResultTooltip = styled.div`
  position: absolute;
  ${padding(rem('5px'), rem('10px'))};
  transform: translate(0, -50%);
  top: 50%;
  right: 100%;
  background: rgb(51, 51, 51);
  color: ${Colors.PureWhite};
  font-size: ${rem('12px')};
  > :after {
    content: '';
    position: absolute;
    bottom: 50%;
    border-bottom: ${rem('5px')} solid transparent;
    border-top: ${rem('5px')} solid transparent;
    transform: translate(0, 50%);
    left: 100%;
    border-left: ${rem('5px')} solid rgb(51, 51, 51);
  }
`;
