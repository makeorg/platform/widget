import styled from '@emotion/styled';

import { size, rem, margin, borderRadius, padding } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';
import {
  VoteDataWrapper,
  VoteChart,
  VoteBarWrapper,
  AgreeBar,
  DisagreeBar,
  NeutralBar,
  VoteCounter,
  ResultTooltip,
} from './VoteData';

const VoteResults = styled.div`
  display: flex;
  flex-flow: column;
  ${margin(rem('15px'), '', '', '')};
`;

const VoteResultsWrapper = styled.div`
  display: flex;
  align-items: space-between;
  justify-content: space-between;
`;

const NextCardButton = styled.button`
  display: flex;
  justify-content: center;
  background-color: ${Colors.MakeRed};
  color: ${Colors.PureWhite};
  ${padding(rem('8px'), '', rem('5px'))};
  font-family: ${Webfonts.TradeGothicLTStd.name};
  font-size: ${rem('14px')};
  text-transform: uppercase;
  opacity: 0;
  ${size('', '100%')};
  ${margin(rem('15px'), '', '', '')};
  ${borderRadius('left', rem('20px'))};
  ${borderRadius('right', rem('20px'))};
`;

// Vote Data elements
VoteResults.VoteDataWrapper = VoteDataWrapper;
VoteResults.VoteResultsWrapper = VoteResultsWrapper;
VoteResults.VoteChart = VoteChart;
VoteResults.VoteBarWrapper = VoteBarWrapper;
VoteResults.AgreeBar = AgreeBar;
VoteResults.DisagreeBar = DisagreeBar;
VoteResults.NeutralBar = NeutralBar;
VoteResults.VoteCounter = VoteCounter;
VoteResults.ResultTooltip = ResultTooltip;
VoteResults.NextCardButton = NextCardButton;

export default VoteResults;
