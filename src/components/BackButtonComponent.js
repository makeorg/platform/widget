import React, { Component } from 'react';
import ProposalCard from './ProposalCard';
import Tracking from '../services/Tracking';

class BackButtonComponent extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.incrementCardCounter();
    Tracking.trackClickPreviousCard(this.props.cardIndex);
  }

  render() {
    const { notFirst } = this.props;
    if (notFirst) {
      return (
        <ProposalCard.BackButton onClick={this.handleClick}>
          <ProposalCard.BackIcon />
        </ProposalCard.BackButton>
      );
    }

    return null;
  }
}

export default BackButtonComponent;
