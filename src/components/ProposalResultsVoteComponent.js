import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import i18next from 'i18next';
import ProposalVote from './ProposalVote';
import VoteResults from './ProposalResults';
import ProposalQualificationComponent from './ProposalQualificationComponent';
import {
  getVoteByVoteKey,
  getVotePercent,
  getVoteColor,
  getVoteOrientation,
} from '../helpers/voteHelper';
import {
  VOTE_AGREE_KEY,
  VOTE_DISAGREE_KEY,
  VOTE_NEUTRAL_KEY,
} from '../shared/constants/vote';
import Tracking from '../services/Tracking';

// @todo: to go into a helper

/**
 * VoteElement component
 * @param {String} voteKey
 * @param {String} proposalId
 */
const VoteElement = ({ voteKey, proposalId }) => (
  <p key={voteKey + proposalId}>{i18next.t(`vote.${voteKey}`)}</p>
);

/**
 * VoteElement component
 * @param {Integer} count
 * @param {Number} percent
 * @param {String} proposalId
 */
const VoteCounterElement = ({ count, percent, proposalId }) => (
  <p key={count + proposalId}>{percent}%</p>
);

/**
 * The agree vote button component
 * @param       obejct props
 * @constructor
 */
class ProposalResultsVoteComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      areAgreeResultsDisplayed: false,
      areDisagreeResultsDisplayed: false,
      areNeutralResultsDisplayed: false,
      loadAnimation: false,
      engageNextAnimation: false,
    };

    this.handleClickNextButton = this.handleClickNextButton.bind(this);
    this.showAgreeResults = this.showAgreeResults.bind(this);
    this.hideAgreeResults = this.hideAgreeResults.bind(this);
    this.showDisagreeResults = this.showDisagreeResults.bind(this);
    this.hideDisagreeResults = this.hideDisagreeResults.bind(this);
    this.showNeutralResults = this.showNeutralResults.bind(this);
    this.hideNeutralResults = this.hideNeutralResults.bind(this);
    this.engageNextAnimation = this.engageNextAnimation.bind(this);
  }

  componentDidMount() {
    this.setState({ loadAnimation: true });
  }

  engageNextAnimation() {
    this.setState({ engageNextAnimation: true });
  }

  handleClickNextButton() {
    this.props.decrementCardCounter();
    Tracking.trackClickNextCard(this.props.cardIndex);
  }

  showAgreeResults() {
    this.setState({
      areAgreeResultsDisplayed: true,
    });
  }

  hideAgreeResults() {
    this.setState({
      areAgreeResultsDisplayed: false,
    });
  }

  showDisagreeResults() {
    this.setState({
      areDisagreeResultsDisplayed: true,
    });
  }

  hideDisagreeResults() {
    this.setState({
      areDisagreeResultsDisplayed: false,
    });
  }

  showNeutralResults() {
    this.setState({
      areNeutralResultsDisplayed: true,
    });
  }

  hideNeutralResults() {
    this.setState({
      areNeutralResultsDisplayed: false,
    });
  }

  render() {
    const {
      userVoteResult,
      onHandleUnvote,
      proposalId,
      cardIndex,
      proposalVotes,
      proposalKey,
    } = this.props;

    const updatedVotes = proposalVotes.map(vote => {
      return vote.voteKey === userVoteResult.voteKey ? userVoteResult : vote;
    });
    const totalVoteCount = updatedVotes
      .map(vote => vote.count)
      .reduce((prev, next) => prev + next);
    const voteAgree = getVoteByVoteKey(VOTE_AGREE_KEY, updatedVotes);
    const voteDisagree = getVoteByVoteKey(VOTE_DISAGREE_KEY, updatedVotes);
    const voteNeutral = getVoteByVoteKey(VOTE_NEUTRAL_KEY, updatedVotes);

    const voteAgreePercent = getVotePercent(totalVoteCount, voteAgree.count);
    const voteDisagreePercent = getVotePercent(
      totalVoteCount,
      voteDisagree.count
    );
    const voteNeutralPercent = getVotePercent(
      totalVoteCount,
      voteNeutral.count
    );

    const color = getVoteColor(userVoteResult.voteKey);
    const orientation = getVoteOrientation(userVoteResult.voteKey);

    return (
      <VoteResults>
        <VoteResults.VoteResultsWrapper>
          <VoteResults.VoteDataWrapper>
            <CSSTransition
              in={this.state.loadAnimation}
              timeout={250}
              classNames="results"
            >
              <ProposalVote.VoteButton
                voted
                color={color}
                onClick={() => onHandleUnvote(userVoteResult.voteKey)}
              >
                <ProposalVote.VoteIcon
                  color={color}
                  orientation={orientation}
                  hasVoted={true}
                />
              </ProposalVote.VoteButton>
            </CSSTransition>
            <VoteResults.VoteChart>
              <VoteResults.VoteBarWrapper
                onMouseEnter={this.showAgreeResults}
                onMouseLeave={this.hideAgreeResults}
              >
                <VoteResults.AgreeBar percent={voteAgreePercent} />
              </VoteResults.VoteBarWrapper>
              <VoteResults.VoteBarWrapper
                onMouseEnter={this.showDisagreeResults}
                onMouseLeave={this.hideDisagreeResults}
              >
                <VoteResults.DisagreeBar percent={voteDisagreePercent} />
              </VoteResults.VoteBarWrapper>
              <VoteResults.VoteBarWrapper
                onMouseEnter={this.showNeutralResults}
                onMouseLeave={this.hideNeutralResults}
              >
                <VoteResults.NeutralBar percent={voteNeutralPercent} />
              </VoteResults.VoteBarWrapper>
              {this.state.areAgreeResultsDisplayed && (
                <VoteResults.ResultTooltip>
                  <VoteElement
                    voteKey={voteAgree.voteKey}
                    proposalId={proposalId}
                  />
                  <VoteCounterElement
                    count={voteAgree.count}
                    percent={voteAgreePercent}
                    proposalId={proposalId}
                  />
                </VoteResults.ResultTooltip>
              )}
              {this.state.areDisagreeResultsDisplayed && (
                <VoteResults.ResultTooltip>
                  <VoteElement
                    voteKey={voteDisagree.voteKey}
                    proposalId={proposalId}
                  />
                  <VoteCounterElement
                    count={voteDisagree.count}
                    percent={voteDisagreePercent}
                    proposalId={proposalId}
                  />
                </VoteResults.ResultTooltip>
              )}
              {this.state.areNeutralResultsDisplayed && (
                <VoteResults.ResultTooltip>
                  <VoteElement
                    voteKey={voteNeutral.voteKey}
                    proposalId={proposalId}
                  />
                  <VoteCounterElement
                    count={voteNeutral.count}
                    percent={voteNeutralPercent}
                    proposalId={proposalId}
                  />
                </VoteResults.ResultTooltip>
              )}
            </VoteResults.VoteChart>
            <VoteResults.VoteCounter>{totalVoteCount}</VoteResults.VoteCounter>
          </VoteResults.VoteDataWrapper>
          <ProposalQualificationComponent
            qualificationsResult={userVoteResult.qualifications}
            voteKey={userVoteResult.voteKey}
            proposalId={proposalId}
            cardIndex={cardIndex}
            color={color}
            engageNextAnimation={this.engageNextAnimation}
            proposalKey={proposalKey}
          />
        </VoteResults.VoteResultsWrapper>
        <CSSTransition
          in={this.state.engageNextAnimation}
          timeout={300}
          classNames="fadeIn"
        >
          <VoteResults.NextCardButton onClick={this.handleClickNextButton}>
            {i18next.t('sequence.next_proposal')} >
          </VoteResults.NextCardButton>
        </CSSTransition>
      </VoteResults>
    );
  }
}

ProposalResultsVoteComponent.propTypes = {
  proposalVotes: PropTypes.array.isRequired,
  proposalId: PropTypes.string.isRequired,
  cardIndex: PropTypes.number.isRequired,
  userVoteResult: PropTypes.object.isRequired,
  decrementCardCounter: PropTypes.func.isRequired,
  onHandleUnvote: PropTypes.func.isRequired,
  proposalKey: PropTypes.string.isRequired,
};

export default ProposalResultsVoteComponent;
