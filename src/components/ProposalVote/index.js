import styled from '@emotion/styled';
import { size, rem, margin } from 'polished';
import { VoteButton } from './VoteButton';
import { VoteIcon } from './Icon';

const ProposalVote = styled.div`
  display: flex;
  ${size('', '100%')};
  ${margin(rem('15px'), '', '', '')};
  max-width: ${rem('215px')};
  justify-content: space-between;
  align-items: flex-end;
`;

// Button Elements
ProposalVote.VoteButton = VoteButton;
ProposalVote.VoteIcon = VoteIcon;

export default ProposalVote;
