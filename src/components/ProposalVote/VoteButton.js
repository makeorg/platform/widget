import styled from '@emotion/styled';

import { size, rem, borderStyle } from 'polished';
import { Colors } from '../../assets/styles/colors';

export const VoteButton = styled.button`
  ${size(rem('50px'), rem('50px'))};
  background-color: ${props =>
    props.voted ? props => props.color : Colors.PureWhite};
  border-width: ${rem('2px')};
  ${borderStyle('solid')};
  border-radius: 50%;
  border-color: ${props => props.color};
  box-shadow: ${props =>
    props.voted ? '0 1px 1px 0 rgba(0, 0, 0, 0.5)' : 'none'};
`;
