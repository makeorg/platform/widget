import React from 'react';
import styled from '@emotion/styled';
import { size, rem } from 'polished';
import { Colors } from '../../assets/styles/colors';

const CheckIconSvg = styled.svg`
  $props.big ? ${size(rem('40px'), rem('40px'))} : ${size(
  rem('15px'),
  rem('15px')
)} ;
`;

const CheckIconG = styled.g`
  ${size('100%', '100%')};
`;
const CheckIconPath = styled.path`
  fill: ${Colors.GreenVote};
`;

export const CheckIcon = ({ big }) => {
  return (
    <CheckIconSvg big viewBox="0 0 42 42">
      <CheckIconG>
        <CheckIconPath d="M 39.234375 8.894531 L 35.730469 5.386719 C 35.25 4.90625 34.664062 4.667969 33.976562 4.667969 C 33.289062 4.667969 32.703125 4.90625 32.222656 5.386719 L 15.3125 22.324219 L 7.734375 14.71875 C 7.253906 14.238281 6.667969 14 5.980469 14 C 5.292969 14 4.707031 14.238281 4.226562 14.71875 L 0.722656 18.226562 C 0.242188 18.707031 0 19.292969 0 19.980469 C 0 20.667969 0.242188 21.25 0.722656 21.730469 L 10.054688 31.0625 L 13.558594 34.570312 C 14.039062 35.050781 14.625 35.292969 15.3125 35.292969 C 16 35.292969 16.585938 35.050781 17.066406 34.570312 L 20.570312 31.0625 L 39.234375 12.398438 C 39.71875 11.917969 39.957031 11.335938 39.957031 10.648438 C 39.957031 9.960938 39.71875 9.375 39.234375 8.894531 Z M 39.234375 8.894531" />
      </CheckIconG>
    </CheckIconSvg>
  );
};
