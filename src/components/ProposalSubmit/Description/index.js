import React from 'react';
import i18next from 'i18next';
import Form from '../../Form';
import {
  DescriptionWrapper,
  Description,
  DescriptionLink,
} from '../Styled/Description';
import * as Helpers from '../../../helpers/url';

class ProposalSubmitDescriptionComponent extends React.Component {
  componentDidMount() {
    const { trackModerationText } = this.props;
    trackModerationText();
  }

  render() {
    const { trackModerationLink } = this.props;
    const moderationCharterLink = Helpers.localizeModerationCharterLink();
    return (
      <DescriptionWrapper>
        <Description>
          <DescriptionLink
            target="_blank"
            href={moderationCharterLink}
            onClick={trackModerationLink}
          >
            {i18next.t('proposal_submit.moderation_charter')}
          </DescriptionLink>
          <Form.NewWindowBlackIcon />
        </Description>
      </DescriptionWrapper>
    );
  }
}

export default ProposalSubmitDescriptionComponent;
