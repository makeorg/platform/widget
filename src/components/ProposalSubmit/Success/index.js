import React from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { sequenceExpand } from '../../../shared/store/actions/sequenceAction';
import Authentification from '../../Authentification';
import ProposalSubmit from '../Styled';
import { CheckIcon } from '../../Styled/Icon';

const ProposalSubmitSuccessComponent = ({ handleExpandSequence }) => (
  <Authentification>
    <header>
      <ProposalSubmit.SuccessWrapper>
        <CheckIcon />
        <ProposalSubmit.SuccessText>
          {i18next.t('proposal_submit.submitted')}
        </ProposalSubmit.SuccessText>
      </ProposalSubmit.SuccessWrapper>
      <Authentification.Intro
        GreyIntro
        dangerouslySetInnerHTML={{
          __html: i18next.t('proposal_submit.moderation_text', {
            interpolation: { escapeValue: false },
          }),
        }}
      />
      <ProposalSubmit.SuccessWrapper>
        <ProposalSubmit.SuccessLink onClick={handleExpandSequence}>
          {i18next.t('sequence.return')}
        </ProposalSubmit.SuccessLink>
      </ProposalSubmit.SuccessWrapper>
    </header>
  </Authentification>
);

const mapDispatchToProps = dispatch => ({
  handleExpandSequence: () => dispatch(sequenceExpand()),
});

export default connect(
  null,
  mapDispatchToProps
)(ProposalSubmitSuccessComponent);
