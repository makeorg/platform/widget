import styled from '@emotion/styled';
import {
  size,
  rem,
  borderRadius,
  borderWidth,
  borderStyle,
  borderColor,
  padding,
  textInputs,
  placeholder,
} from 'polished';
import { Colors } from '../../../assets/styles/colors';
import { Breakpoints } from '../../../assets/styles/breakpoints';

export const Form = styled.form`
  ${borderRadius('top', rem('20px'))};
  ${borderRadius('bottom', rem('20px'))};
  ${borderWidth(rem('1px'))};
  ${borderStyle('solid')};
  ${borderColor(Colors.GreyBorders)};
  ${padding(rem('2px'), rem('2px'), rem('2px'), rem('10px'))};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    ${padding(rem('5px'), rem('5px'), rem('5px'), rem('20px'))};
  }
  display: flex;
  align-items: center;
  background-color: white;
  > span {
    display: flex;
    flex: 0 auto 1;
    font-size: ${rem('14px')};
    margin-right: ${rem('5px')};
  }
  > ${textInputs()} {
    display: flex;
    flex: 1;
    ${size('', '100%')};
    ${placeholder({ color: Colors.PureBlack, 'font-size': '0.875rem' })};
    border: none;
    background: none;
    font-size: ${rem('16px')};
  }
  > ${textInputs('focus')} {
    outline: none;
  }
`;
