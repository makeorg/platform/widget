import styled from '@emotion/styled';

import { rem } from 'polished';
import { Colors } from '../../../assets/styles/colors';

export const DescriptionWrapper = styled.div`
  margin-top: ${rem('10px')};
`;

export const Description = styled.p`
  font-size: ${rem('14px')};
  color: ${Colors.GreyText};
  text-align: center;
`;

export const DescriptionLink = styled.a`
  cursor: pointer;
  color: ${Colors.PureBlack};
`;
