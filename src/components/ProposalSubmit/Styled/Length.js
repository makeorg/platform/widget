import styled from '@emotion/styled';
import { rem, margin } from 'polished';
import { Webfonts } from '../../../assets/styles/webfonts';
import { Colors } from '../../../assets/styles/colors';

export const Length = styled.div`
  color: ${Colors.GreyText};
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: ${rem('12px')};
  ${margin('0', rem('5px'))};
`;
