import styled from '@emotion/styled';

import { size } from 'polished';
import { Form } from './Form';
import { Button } from './Button';
import { Length } from './Length';
import { SuccessWrapper, SuccessText, SuccessLink } from './Success';

const ProposalSubmit = styled.div`
  ${size('', '100%')};
`;

ProposalSubmit.Form = Form;
ProposalSubmit.Button = Button;
ProposalSubmit.Length = Length;
ProposalSubmit.SuccessWrapper = SuccessWrapper;
ProposalSubmit.SuccessText = SuccessText;
ProposalSubmit.SuccessLink = SuccessLink;

export default ProposalSubmit;
