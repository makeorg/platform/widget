import styled from '@emotion/styled';
import { rem } from 'polished';
import { Colors } from '../../../assets/styles/colors';
import { Webfonts } from '../../../assets/styles/webfonts';

export const SuccessWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: baseline;
  margin-bottom: ${rem('20px')};
`;

export const SuccessText = styled.span`
  font-family: ${Webfonts.TradeGothicLTStd.family};
  font-size: ${rem('20px')};
`;

export const SuccessLink = styled.button`
  cursor: pointer;
  color: ${Colors.ThemePrimaryColor};
  text-decoration: underline;
`;
