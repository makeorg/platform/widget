import styled from '@emotion/styled';
import { rem, borderRadius, padding } from 'polished';
import { Colors } from '../../../assets/styles/colors';
import { Webfonts } from '../../../assets/styles/webfonts';

export const Button = styled.button`
  ${borderRadius('top', rem('20px'))};
  ${borderRadius('bottom', rem('20px'))};
  ${padding(rem('8px'), rem('20px'), rem('3px'))};
  border: none;
  font-family: ${Webfonts.TradeGothicLTStd.family};
  font-size: ${rem('14px')};
  color: ${Colors.PureWhite};
  background-color: ${props =>
    props.disabled ? Colors.GreyExtraLight : Colors.ThemePrimaryColor};
  text-transform: uppercase;
  cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
`;
