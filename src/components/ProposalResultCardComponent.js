import React, { Component } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import Authentification from './Authentification';
import BackButtonComponent from './BackButtonComponent';
import ProposalCard from './ProposalCard';
import { CheckIcon } from './Styled/Icon';
import { showRegister } from '../shared/store/actions/pannelAction';
import { getUserVoteSharePercent } from '../helpers/voteHelper';
import Tracking from '../services/Tracking';
import * as Helpers from '../helpers/url';

export const ResultAuthentificationComponent = ({
  themeColor,
  user,
  isLoggedIn,
  handleClickAuthentificateWithEmail,
  handleClickAuthentificateWithFacebook,
}) => {
  if (!isLoggedIn) {
    return (
      <Authentification.ButtonsWrapper>
        <Authentification.FacebookLoginButtonComponent
          onClick={handleClickAuthentificateWithFacebook}
        />
        <Authentification.GoogleLoginButtonComponent />
        <Authentification.EmailButton
          onClick={handleClickAuthentificateWithEmail}
          themeColor={themeColor}
        >
          <Authentification.EmailIcon />
          <Authentification.ButtonLabel>
            {i18next.t('common.email')}
          </Authentification.ButtonLabel>
        </Authentification.EmailButton>
      </Authentification.ButtonsWrapper>
    );
  }

  return (
    <ProposalCard.ResultConnectWrapper>
      <CheckIcon big />
      <ProposalCard.ResultDescription>
        {i18next.t('result_card.thanks')}
      </ProposalCard.ResultDescription>
    </ProposalCard.ResultConnectWrapper>
  );
};

class ProposalResultCardComponent extends Component {
  constructor(props) {
    super(props);

    this.handleClickAuthentificateWithEmail = this.handleClickAuthentificateWithEmail.bind(
      this
    );
    this.handleClickAuthentificateWithFacebook = this.handleClickAuthentificateWithFacebook.bind(
      this
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.isShown) {
      Tracking.trackDisplayFinalCard();
    }
  }
  handleClickAuthentificateWithEmail() {
    this.props.showRegisteringPannel();
    Tracking.trackClickSignupCardEmail();
  }

  handleClickAuthentificateWithFacebook() {
    Tracking.trackClickSignupCardFacebook();
  }

  render() {
    const {
      themeColor,
      votes,
      user,
      totalProposals,
      isLoggedIn,
      incrementCardCounter,
    } = this.props;

    const userVoteSharePercent = getUserVoteSharePercent(votes);
    const dataPolicyLink = Helpers.localizeDataPolicyLink();

    return (
      <ProposalCard>
        <ProposalCard.Header>
          <ProposalCard.BackButtonWrapper>
            <BackButtonComponent
              notFirst={true}
              incrementCardCounter={incrementCardCounter}
              cardIndex={totalProposals}
            />
          </ProposalCard.BackButtonWrapper>
          <ProposalCard.ResultTitle>
            {i18next.t('result_card.title')}
          </ProposalCard.ResultTitle>
        </ProposalCard.Header>
        <ProposalCard.Separator />
        <ProposalCard.ResultDescriptionWrapper>
          <ProposalCard.ResultDescriptionAlt
            themeColor={themeColor}
            dangerouslySetInnerHTML={{
              __html: i18next.t('result_card.you_share', {
                percentage: `<span>${userVoteSharePercent}%</span>`,
                interpolation: { escapeValue: false },
              }),
            }}
          />
        </ProposalCard.ResultDescriptionWrapper>
        {!user && (
          <ProposalCard.ResultRegisterTitle>
            <Authentification.IntroAuthentification>
              {i18next.t('result_card.intro_authentification')}
            </Authentification.IntroAuthentification>
            <Authentification.IntroPrivacy>
              {i18next.t('common.commitment')}
              &nbsp;
              <Authentification.IntroPrivacyLink
                target="_blank"
                href={dataPolicyLink}
              >
                {i18next.t('common.personal_data')}
              </Authentification.IntroPrivacyLink>
            </Authentification.IntroPrivacy>
          </ProposalCard.ResultRegisterTitle>
        )}
        <ResultAuthentificationComponent
          themeColor={themeColor}
          user={user}
          isLoggedIn={isLoggedIn}
          handleClickAuthentificateWithEmail={
            this.handleClickAuthentificateWithEmail
          }
          handleClickAuthentificateWithFacebook={
            this.handleClickAuthentificateWithFacebook
          }
        />
      </ProposalCard>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showRegisteringPannel: () => {
      dispatch(showRegister());
    },
  };
};

const mapStateToProps = state => {
  const { themeColor } = state.config;
  const { votes } = state.vote;
  const { isLoggedIn, user } = state.authentification;

  return {
    themeColor,
    votes,
    user,
    isLoggedIn,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalResultCardComponent);
