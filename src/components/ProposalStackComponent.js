import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import * as SequenceHelper from '../helpers/sequenceHelper';
import ProposalCardComponent from './ProposalCardComponent';
import ProposalResultCardComponent from './ProposalResultCardComponent';
import ProposalList from './ProposalStack';
import i18next from 'i18next';

/**
 * stack of Proposals Component
 * @extends Component
 */
class ProposalStackComponent extends Component {
  render() {
    const {
      isSequenceCollapsed,
      isTooltipShown,
      progress,
      proposals,
      votes,
      total,
      currentIndex,
      progressPercent,
      handleExpandSequence,
      showTooltip,
      hideTooltip,
      incrementCardCounter,
      decrementCardCounter,
    } = this.props;

    return (
      <ProposalList className={isSequenceCollapsed ? 'translated-stack' : ''}>
        <ProposalList.BackArrowButton
          onClick={handleExpandSequence}
          displayBackButton={isSequenceCollapsed}
          className={isSequenceCollapsed ? 'display-back-buttons' : ''}
        >
          <ProposalList.BackArrowIcon />
        </ProposalList.BackArrowButton>
        <ProposalList.BackButton
          onClick={handleExpandSequence}
          displayBackButton={isSequenceCollapsed}
          className={isSequenceCollapsed ? 'display-back-buttons' : ''}
        >
          {i18next.t('sequence.return')}
        </ProposalList.BackButton>
        <ProposalList.ListWrapper>
          <ProposalList.List>
            {proposals.map((proposal, index) => (
              <ProposalList.ProposalItem
                key={index}
                className={
                  SequenceHelper.getIndex(index, currentIndex) >= 0
                    ? ''
                    : 'unset-card-position'
                }
                position={SequenceHelper.getCardPosition(index, currentIndex)}
                index={SequenceHelper.getZIndex(index, currentIndex)}
                scaling={SequenceHelper.getScale(index, currentIndex)}
              >
                <ProposalCardComponent
                  key={proposal.id}
                  counter={currentIndex}
                  incrementCardCounter={incrementCardCounter}
                  decrementCardCounter={decrementCardCounter}
                  index={index}
                  totalProposals={total}
                  proposal={proposal}
                />
              </ProposalList.ProposalItem>
            ))}
            {total > 0 && votes.length === total && (
              <ProposalList.ProposalItem
                key={total}
                className={
                  SequenceHelper.getIndex(total, currentIndex) >= 0
                    ? ''
                    : 'unset-card-position'
                }
                position={SequenceHelper.getCardPosition(total, currentIndex)}
                index={SequenceHelper.getZIndex(total, currentIndex)}
                scaling={SequenceHelper.getScale(total, currentIndex)}
              >
                <ProposalResultCardComponent
                  index={total}
                  totalProposals={total}
                  isShown={currentIndex + total === 0}
                  incrementCardCounter={incrementCardCounter}
                />
              </ProposalList.ProposalItem>
            )}
          </ProposalList.List>
          <ProposalList.WrapperProgress
            onMouseEnter={showTooltip}
            onMouseLeave={hideTooltip}
          >
            <ProposalList.ProgressBar progressPercent={progressPercent} />
            <CSSTransition
              in={isTooltipShown}
              timeout={1000}
              classNames="message"
              mountOnEnter
              unmountOnExit
              onEntered={hideTooltip}
            >
              <ProposalList.ProgressTooltip
                progressPercent={progress >= total ? 100 : progressPercent}
              >
                {progress >= total
                  ? `${total}/${total}`
                  : `${progress}/${total}`}
              </ProposalList.ProgressTooltip>
            </CSSTransition>
          </ProposalList.WrapperProgress>
        </ProposalList.ListWrapper>
      </ProposalList>
    );
  }
}

export default ProposalStackComponent;
