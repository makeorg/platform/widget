import styled from '@emotion/styled';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';
import { Breakpoints } from '../../assets/styles/breakpoints';

export const ExpirationSessionWrapperStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: center;
  height: 100%;
`;

export const ExpirationSessionContentStyle = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  max-width: 580px;
  height: 100%;
  padding: 10px;
  border-radius: 10px;
  background-color: ${Colors.PureWhite};
`;

export const SessionExpiredPictureStyle = styled.img`
  padding: 20px;
`;

export const SessionExpiredTitleStyle = styled.h1`
  text-transform: uppercase;
  font-family: ${Webfonts.TradeGothicLTStd.family};
  color: ${Colors.MakeRed};
  font-size: 14px;
  line-height: 1.25;
  padding-bottom: 14px;
  @media (min-width: ${Breakpoints.mobile}) {
    font-size: 16px;
  }
`;

export const SessionSeparationLine = styled.hr`
  margin: 0px;
  width: 58px;
  height: 1px;
  opacity: 0.1;
  border: solid 0.5px ${Colors.PureBlack};
  background-color: ${Colors.PureBlack};
`;

export const SessionExpiredParagraphStyle = styled.p`
  text-align: center;
  margin-top: 10px;
  padding: 0px 45px;
  color: ${Colors.PureBlack};
  font-family: ${Webfonts.CircularStdBook.family};
  font-size: 12px;
  line-height: 1.5;
  @media (min-width: 440px) {
    padding: 0px 90px;
  }
  @media (min-width: ${Breakpoints.mobile}) {
    padding: 0px 150px;
    font-size: 14px;
  }
`;

export const ReloadButtonStyle = styled.button`
  margin: 20px;
  font-family: ${Webfonts.TradeGothicLTStd.family}};
  font-size: 14px;
  line-height: 1;
  border-radius: 20px;
  text-transform: uppercase;
  padding: 12px 25px 8px;
  color: ${Colors.PureWhite};
  background-color: ${Colors.MakeRed};
  svg,
  .tofill {
    fill: ${Colors.PureWhite};
  }
  @media (min-width: ${Breakpoints.mobile}) {
    font-size: 16px;
  }
`;

export const IconWrapperStyle = styled.span`
  display: inline-flex;
  justify-content: flex-start;
  align-content: center;
  margin-right: 7px;
  svg {
    width: 9px;
    height: 11px;
  }
  @media (min-width: ${Breakpoints.mobile}) {
    svg {
      width: 11px;
      height: 13px;
    }
  }
`;
