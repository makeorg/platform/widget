import React, { Component } from 'react';
import i18next from '../../shared/i18n/index';
import SessionExpiredPicture from '../../assets/images/session-expired.svg';
import { SvgRightArrow } from '../../assets/images/RightArrow';
import {
  ExpirationSessionWrapperStyle,
  ExpirationSessionContentStyle,
  SessionExpiredPictureStyle,
  SessionExpiredTitleStyle,
  SessionSeparationLine,
  SessionExpiredParagraphStyle,
  ReloadButtonStyle,
  IconWrapperStyle,
} from './style';

export class ExpirationSession extends Component {
  handleClose = (e: SyntheticEvent<HTMLButtonElement>) => {
    e.preventDefault();
    window.location.reload();
  };

  render() {
    return (
      <React.Fragment>
        <ExpirationSessionWrapperStyle>
          <ExpirationSessionContentStyle>
            <SessionExpiredPictureStyle src={SessionExpiredPicture} alt="" />
            <SessionExpiredTitleStyle>
              {i18next.t('common.session_expired.title')}
            </SessionExpiredTitleStyle>
            <SessionSeparationLine />
            <SessionExpiredParagraphStyle>
              {i18next.t('common.session_expired.description')}
            </SessionExpiredParagraphStyle>
            <ReloadButtonStyle onClick={this.handleClose}>
              <IconWrapperStyle aria-hidden>
                <SvgRightArrow />
              </IconWrapperStyle>
              {i18next.t('common.session_expired.button_text')}
            </ReloadButtonStyle>
          </ExpirationSessionContentStyle>
        </ExpirationSessionWrapperStyle>
      </React.Fragment>
    );
  }
}
