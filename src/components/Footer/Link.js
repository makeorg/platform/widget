import styled from '@emotion/styled';
import { rem, margin } from 'polished';
import { Colors } from '../../assets/styles/colors';
import { Webfonts } from '../../assets/styles/webfonts';

export const Link = styled.button`
  text-transform: uppercase;
  text-decoration: underline;
  font-family: ${Webfonts.TradeGothicLTStd.name};
  color: ${Colors.ThemePrimaryColor};
  ${margin('', rem('5px'))}
`;
