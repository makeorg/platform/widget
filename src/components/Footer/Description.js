import styled from '@emotion/styled';
import { rem } from 'polished';
import { Webfonts } from '../../assets/styles/webfonts';

export const Description = styled.span`
  font-size: ${rem('12px')};
  text-transform: uppercase;
  font-family: ${Webfonts.TradeGothicLTStd.name};
`;
