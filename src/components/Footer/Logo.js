import styled from '@emotion/styled';
import { size, rem, margin } from 'polished';

export const Logo = styled.img`
  display: inline-block;
  vertical-align: middle;
  ${size('', rem('45px'))};
  ${margin('0', rem('10px'))}
`;
