import styled from '@emotion/styled';
import { size, rem, padding } from 'polished';
import { Logo } from './Logo';
import { Description } from './Description';
import { Link } from './Link';
import { Colors } from '../../assets/styles/colors';
export const Footer = styled.footer`
  position: relative;
  display: flex;
  z-index: 99;
  ${size('', '100%')};
  ${padding(rem('10px'), '')};
  align-items: center;
  justify-content: center;
  background-color: ${Colors.DarkGreyBackground};
  box-shadow: 0 -2px 2px 0 rgba(0, 0, 0, 0.3);
`;

// Navbar Elements
Footer.Logo = Logo;
Footer.Description = Description;
Footer.Link = Link;

export default Footer;
