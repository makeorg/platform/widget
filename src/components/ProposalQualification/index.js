import styled from '@emotion/styled';

import { size, rem } from 'polished';
import {
  QualificationButton,
  QualificationLabel,
  QualificationCounter,
} from './QualificationButton';

const Qualification = styled.div`
  display: flex;
  min-width: ${rem('150px')};
`;

const QualificationList = styled.ul`
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  align-items: center;
  ${size('', '100%')};
`;

const QualificationListItem = styled.li`
  list-style: none;
  ${size('', '100%')};
`;

// Button Elements
Qualification.QualificationList = QualificationList;
Qualification.QualificationListItem = QualificationListItem;
Qualification.QualificationButton = QualificationButton;
Qualification.QualificationLabel = QualificationLabel;
Qualification.QualificationCounter = QualificationCounter;

export default Qualification;
