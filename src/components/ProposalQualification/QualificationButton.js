import styled from '@emotion/styled';

import {
  size,
  rem,
  padding,
  borderRadius,
  borderWidth,
  borderStyle,
} from 'polished';
import { Colors } from '../../assets/styles/colors';

export const QualificationButton = styled.button`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${props =>
    props.qualified ? props => props.color : Colors.PureWhite};
  color: ${props =>
    props.qualified ? Colors.PureWhite : props => props.color};
  border-color: ${props => props.color};
  ${borderWidth(rem('1px'))};
  ${borderStyle('solid')};
  ${padding(rem('5px'), rem('10px'))};
  font-size: ${rem('14px')};
  ${size('', '100%')};
  ${borderRadius('left', rem('36px'))};
  ${borderRadius('right', rem('36px'))};
  box-shadow: ${props =>
    props.qualified ? '0 1px 1px 0 rgba(0, 0, 0, 0.5)' : 'none'};
`;

export const QualificationLabel = styled.span`
  font-size: ${rem('14px')};
  margin-right: ${rem('10px')};
`;

export const QualificationCounter = styled.span`
  font-size: ${rem('16px')};
`;
