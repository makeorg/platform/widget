import React, { Component } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { userActions } from '../shared/store/actions/userAction';
import { showLogin } from '../shared/store/actions/pannelAction';
import { throttle } from '../shared/helpers/throttle';
import MakeLogoWhite from '../assets/images/logo-white.svg';
import SlidingPannel from './SlidingPannel';
import Form from './Form';

const ForgotPasswordForm = ({
  email,
  errors,
  isSuccess,
  handleChange,
  handleSubmit,
  showLoginPannel,
}) => {
  if (isSuccess) {
    return (
      <SlidingPannel.Description>
        {i18next.t('forgot_password.form.success')}
      </SlidingPannel.Description>
    );
  }

  return (
    <Form onSubmit={handleSubmit}>
      <SlidingPannel.Description>
        {i18next.t('forgot_password.form.description')}
      </SlidingPannel.Description>

      {errors.length > 0 && (
        <Form.FormErrors>
          {errors.map((error, index) => (
            <Form.FormError key={index}>{error.message}</Form.FormError>
          ))}
        </Form.FormErrors>
      )}
      <Form.FormInputsWrapper>
        <Form.InputWrapper>
          <Form.Label htmlFor="email">
            <Form.EmailIcon />
          </Form.Label>
          <Form.Input
            id="email"
            value={email}
            required="true"
            placeholder={i18next.t('common.form.email_label')}
            onChange={handleChange}
          />
        </Form.InputWrapper>
      </Form.FormInputsWrapper>
      <Form.SubmitButton>
        <Form.OkIcon />
        {i18next.t('forgot_password.form.resend_email')}
      </Form.SubmitButton>
      <Form.ConnectionLink>
        {i18next.t('common.return_to')}{' '}
        <SlidingPannel.Link onClick={showLoginPannel}>
          {i18next.t('common.connexion_screen')}
        </SlidingPannel.Link>
      </Form.ConnectionLink>
    </Form>
  );
};

class ForgotPasswordFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };

    this.throttleSubmit = throttle(this.handleSubmit);
  }

  showLoginPannel = () => {
    this.props.dispatch(showLogin());
  };

  handleChange = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.setState({ submitted: true });
    const { email } = this.state;
    const { dispatch } = this.props;
    if (email) {
      dispatch(userActions.forgotPassword(email));
    }
  };

  render() {
    const { errors, isSuccess } = this.props;
    const { email } = this.state;

    return (
      <SlidingPannel.Wrapper>
        <SlidingPannel.Logo src={MakeLogoWhite} alt="Make.org" />
        <SlidingPannel.Title>
          {i18next.t('forgot_password.title')}
        </SlidingPannel.Title>
        <SlidingPannel.Sep />
        <ForgotPasswordForm
          email={email}
          errors={errors}
          isSuccess={isSuccess}
          handleChange={this.handleChange}
          handleSubmit={this.throttleSubmit}
          showLoginPannel={this.showLoginPannel}
        />
      </SlidingPannel.Wrapper>
    );
  }
}

function mapStateToProps(state) {
  const { errors, isSuccess } = state.forgotPassword;
  return {
    errors,
    isSuccess,
  };
}
export default connect(mapStateToProps)(ForgotPasswordFormComponent);
