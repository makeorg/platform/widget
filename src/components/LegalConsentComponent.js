import React, { Component } from 'react';
import i18next from 'i18next';
import MakeLogo from '../assets/images/logo.svg';
import ConsentLogo from '../assets/images/consent.png';
import SlidingPannel from './SlidingPannel';
import Form from './Form';

class LegalConsent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minorConsent: false,
      parentalConsent: false,
    };
  }

  toggleMinorConsent = () => {
    const { handleLegalField } = this.props;
    this.setState(prevState => {
      handleLegalField('legalMinorConsent', !prevState.minorConsent);
      return {
        minorConsent: !prevState.minorConsent,
      };
    });
  };

  toggleParentalConsent = () => {
    const { handleLegalField } = this.props;
    this.setState(prevState => {
      handleLegalField('legalAdvisorApproval', !prevState.parentalConsent);
      return {
        parentalConsent: !prevState.parentalConsent,
      };
    });
  };

  render() {
    const { needLegalConsent, toggleLegalConsent, handleSubmit } = this.props;
    const { minorConsent, parentalConsent } = this.state;
    const enableSubmit = minorConsent && parentalConsent;
    const FORM_NAME = 'user_consent';

    return (
      <SlidingPannel.WhiteInner className={!needLegalConsent && 'hidden'}>
        <SlidingPannel.Wrapper>
          <SlidingPannel.Logo src={MakeLogo} alt="Make.org" />
          <Form.LegalConsentTitle>
            {i18next.t('legal_consent.title')}
          </Form.LegalConsentTitle>
          <SlidingPannel.Sep className="grey" />
          <Form.LegalConsentLogo src={ConsentLogo} alt="" />
          <Form id={FORM_NAME} name={FORM_NAME} onSubmit={handleSubmit}>
            <Form.LegalConsentTitle as="h3">
              {i18next.t('legal_consent.subtitle')}
            </Form.LegalConsentTitle>
            <Form.LegalConsentParagraph>
              {i18next.t('legal_consent.description')}
            </Form.LegalConsentParagraph>
            <SlidingPannel.Sep className="grey adjusted-margins" />
            <Form.LegalConsentParagraph as="div" className="checkbox-wrapper">
              <input
                id="minor_consent"
                value={minorConsent}
                name="minor_consent"
                type="checkbox"
                onChange={this.toggleMinorConsent}
              />
              <label htmlFor="minor_consent">
                {i18next.t('legal_consent.minor_consent')}
              </label>
            </Form.LegalConsentParagraph>
            <Form.LegalConsentParagraph
              value={parentalConsent}
              as="div"
              className="checkbox-wrapper"
            >
              <input
                id="advisor_consent"
                name="advisor_consent"
                type="checkbox"
                onChange={this.toggleParentalConsent}
              />
              <label htmlFor="advisor_consent">
                {i18next.t('legal_consent.parental_consent')}
              </label>
            </Form.LegalConsentParagraph>
            <Form.LegalConsentButtonWrapper>
              <Form.LegalConsentCancelButton
                type="button"
                onClick={toggleLegalConsent}
              >
                {i18next.t('legal_consent.cancel')}
              </Form.LegalConsentCancelButton>
              <Form.SubmitButton
                form={FORM_NAME}
                type="submit"
                disabled={!enableSubmit}
              >
                {i18next.t('legal_consent.submit')}
              </Form.SubmitButton>
            </Form.LegalConsentButtonWrapper>
          </Form>
        </SlidingPannel.Wrapper>
      </SlidingPannel.WhiteInner>
    );
  }
}

export default LegalConsent;
