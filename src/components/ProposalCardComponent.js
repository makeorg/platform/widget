import React, { Component } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';
import ProposalCard from './ProposalCard';
import ProposalVoteComponent from './ProposalVoteComponent';
import BackButtonComponent from './BackButtonComponent';

const Age = ({ age }) =>
  age > 0 ? (
    <span>, {i18next.t('proposal_card.author.age', { age })}</span>
  ) : null;

/**
 * A card who describe a proposal
 * @param       obejct props
 * @constructor
 */
class ProposalCardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      proposal,
      index,
      decrementCardCounter,
      incrementCardCounter,
    } = this.props;
    const cardNumber = index + 1;
    const notFirst = cardNumber > 1;

    return (
      <ProposalCard>
        <ProposalCard.Header>
          <ProposalCard.BackButtonWrapper>
            <BackButtonComponent
              notFirst={notFirst}
              incrementCardCounter={incrementCardCounter}
              cardIndex={index}
            />
          </ProposalCard.BackButtonWrapper>
          <ProposalCard.UserInformations>
            {proposal.author.displayName}
            <Age age={proposal.author.age} />
          </ProposalCard.UserInformations>
        </ProposalCard.Header>
        <ProposalCard.Separator />
        <ProposalCard.UserProposal>
          {proposal.content}
        </ProposalCard.UserProposal>
        <ProposalVoteComponent
          cardIndex={index}
          decrementCardCounter={decrementCardCounter}
          proposalVotes={proposal.votes}
          proposalId={proposal.id}
          proposalKey={proposal.proposalKey}
        />
      </ProposalCard>
    );
  }
}

ProposalCardComponent.propTypes = {
  proposal: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  totalProposals: PropTypes.number.isRequired,
};

export default ProposalCardComponent;
