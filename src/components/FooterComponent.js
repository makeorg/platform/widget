import React, { Component } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import MakeLogo from '../assets/images/logo.svg';
import Footer from './Footer';
import { showPresentation } from '../shared/store/actions/pannelAction';
import Tracking from '../services/Tracking';

/**
 * A card who describe a proposal
 * @param       obejct props
 * @constructor
 */
class FooterComponent extends Component {
  constructor(props) {
    super(props);

    this.showPresentationPannel = this.showPresentationPannel.bind(this);
  }

  showPresentationPannel() {
    this.props.dispatch(showPresentation());
    Tracking.trackClickLearnMore();
  }

  render() {
    return (
      <Footer>
        <Footer.Logo src={MakeLogo} alt="Make.org" />
        <Footer.Description>
          {i18next.t('footer.description')}
        </Footer.Description>
        <Footer.Link onClick={this.showPresentationPannel}>
          {i18next.t('common.see_more')}
        </Footer.Link>
      </Footer>
    );
  }
}

export default connect()(FooterComponent);
