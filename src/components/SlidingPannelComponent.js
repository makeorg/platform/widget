import React, { Component } from 'react';
import { connect } from 'react-redux';
import SlidingPannel from './SlidingPannel';
import {
  closePannel,
  quitPrivacyPolicy,
} from '../shared/store/actions/pannelAction';
/**
 * Sliding Pannel
 * @param       obejct props
 * @constructor
 */
class SlidingPannelComponent extends Component {
  constructor(props) {
    super(props);

    this.closePannel = this.closePannel.bind(this);
  }

  closePannel() {
    const { dispatch, pannelRender } = this.props;

    if (pannelRender && pannelRender === 'PANNEL_RENDER_PRIVACY_POLICY') {
      return dispatch(quitPrivacyPolicy());
    }

    return dispatch(closePannel());
  }

  render() {
    const { showPannel } = this.props;
    return (
      <SlidingPannel pannel={showPannel === true ? '0' : '100'}>
        <SlidingPannel.CloseButton onClick={this.closePannel}>
          <SlidingPannel.CloseIcon />
        </SlidingPannel.CloseButton>
        {this.props.children}
      </SlidingPannel>
    );
  }
}

function mapStateToProps(state) {
  const { showPannel, pannelRender } = state.config;
  return {
    showPannel,
    pannelRender,
  };
}

export default connect(mapStateToProps)(SlidingPannelComponent);
