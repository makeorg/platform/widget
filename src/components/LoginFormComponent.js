import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { userActions } from '../shared/store/actions/userAction';
import {
  showRegister,
  showForgotPassword,
} from '../shared/store/actions/pannelAction';
import { throttle } from '../shared/helpers/throttle';
import MakeLogoWhite from '../assets/images/logo-white.svg';
import SlidingPannel from './SlidingPannel';
import Form from './Form';
import Authentification from './Authentification';

class LoginFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      showPassword: false,
    };

    this.showRegisteringPannel = this.showRegisteringPannel.bind(this);
    this.showForgotPasswordPannel = this.showForgotPasswordPannel.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.togglePasswordVisibility = this.togglePasswordVisibility.bind(this);

    this.throttleSubmit = throttle(this.handleSubmit);
  }

  /**
   * Show the pannel with RegisterForm
   */
  showRegisteringPannel() {
    this.props.dispatch(showRegister());
  }

  togglePasswordVisibility() {
    this.setState(prevState => {
      return {
        showPassword: !prevState.showPassword,
      };
    });
  }
  /**
   * Show the pannel with ForgotPassword
   */
  showForgotPasswordPannel() {
    this.props.dispatch(showForgotPassword());
  }

  handleChange = event => {
    const { id, value } = event.target;
    this.setState({
      [id]: value,
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { dispatch } = this.props;
    if (email && password) {
      dispatch(
        userActions.loginWithPrivacyCheck(
          email,
          password,
          this.props.privacyPolicyDate
        )
      );
    }
  };

  render() {
    const { email, password, showPassword } = this.state;
    const { errors } = this.props;
    const FORM_NAME = 'user_login';
    return (
      <SlidingPannel.Wrapper>
        <SlidingPannel.Logo src={MakeLogoWhite} alt="Make.org" />
        <SlidingPannel.Title>{i18next.t('login.title')}</SlidingPannel.Title>
        <SlidingPannel.Sep />
        <Form onSubmit={this.throttleSubmit} name={FORM_NAME} id={FORM_NAME}>
          {this.props.socialConnect && (
            <Fragment>
              <SlidingPannel.Title>
                {i18next.t('login.connexion_with_social')}
              </SlidingPannel.Title>
              <Authentification.ButtonsWrapperAlt>
                <Authentification.FacebookLoginButtonComponent />
                <Authentification.GoogleLoginButtonComponent />
              </Authentification.ButtonsWrapperAlt>
              <SlidingPannel.SepWrapper>
                <SlidingPannel.LargeSep />
                <SlidingPannel.SepText>
                  {i18next.t('common.or')}
                </SlidingPannel.SepText>
                <SlidingPannel.LargeSep />
              </SlidingPannel.SepWrapper>
            </Fragment>
          )}
          <SlidingPannel.Title>
            {i18next.t('login.connexion_with_email')}
          </SlidingPannel.Title>
          {errors.length > 0 && (
            <Form.FormErrors>
              {errors.map((error, index) => (
                <Form.FormError key={index}>{error}</Form.FormError>
              ))}
            </Form.FormErrors>
          )}
          <Form.FormInputsWrapperAlt>
            <Form.InputWrapper>
              <Form.Label htmlFor="email">
                <Form.EmailIcon />
              </Form.Label>
              <Form.Input
                id="email"
                value={email}
                required="true"
                placeholder={i18next.t('common.form.email_label')}
                onChange={this.handleChange}
                type="email"
              />
            </Form.InputWrapper>
            <Form.InputWrapper>
              <Form.Label htmlFor="password">
                <Form.PasswordIcon />
              </Form.Label>
              <Form.Input
                id="password"
                value={password}
                required="true"
                placeholder={i18next.t('common.form.password_label')}
                onChange={this.handleChange}
                type={this.state.showPassword ? 'text' : 'password'}
              />
              <Form.PasswordToggle
                type="button"
                onClick={this.togglePasswordVisibility}
              >
                {showPassword ? (
                  <Form.ShowPasswordIcon />
                ) : (
                  <Form.HidePasswordIcon />
                )}
              </Form.PasswordToggle>
            </Form.InputWrapper>
          </Form.FormInputsWrapperAlt>
          <Form.FormInputsWrapperAlt>
            <Form.SubmitButton type="submit" form={FORM_NAME}>
              <Form.OkIcon />
              {i18next.t('authentification.i_connect')}
            </Form.SubmitButton>
          </Form.FormInputsWrapperAlt>
          <Form.ConnectionLink>
            {i18next.t('login.i_forgot')}{' '}
            <SlidingPannel.Link onClick={this.showForgotPasswordPannel}>
              {i18next.t('login.my_password')}
            </SlidingPannel.Link>
          </Form.ConnectionLink>
          <Form.ConnectionLink>
            {i18next.t('login.i_dont_have_account')}{' '}
            <SlidingPannel.Link onClick={this.showRegisteringPannel}>
              {i18next.t('login.i_create_account')}
            </SlidingPannel.Link>
          </Form.ConnectionLink>
        </Form>
      </SlidingPannel.Wrapper>
    );
  }
}

function mapStateToProps(state) {
  const { socialConnect, privacyPolicyDate } = state.config;
  const { errors } = state.authentification;
  return {
    errors,
    socialConnect,
    privacyPolicyDate,
  };
}
export default connect(mapStateToProps)(LoginFormComponent);
