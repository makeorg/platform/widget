//@flow
import React from 'react';

export const SvgRightArrow = props => (
  <svg width="13" height="14" viewBox="0 0 13 14" {...props}>
    <path d="M11.393 5.57L2.473.295C1.748-.132.638.283.638 1.342v10.545c0 .95 1.032 1.523 1.835 1.046l8.92-5.27c.796-.47.798-1.625 0-2.094z" />
  </svg>
);
