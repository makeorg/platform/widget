export const Breakpoints = {
  smallmobile: '320px',
  mobile: '575px',
  appMaxWidth: '630px',
};
