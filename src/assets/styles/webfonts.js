export const Webfonts = {
  TradeGothicLTStd: {
    name: "'Trade Gothic LTStd'",
    family: "'Trade Gothic LTStd', Arial, sans-serif",
    woff: "'../../../fonts/TradeGothicLTStd-BdCn20.woff'",
    wof2f: "'../../../fonts/TradeGothicLTStd-BdCn20.woff2'",
  },
  CircularStdBold: {
    name: "'Circular Std Bold'",
    family: "'Circular Std Bold', Arial, sans-serif",
    woff: "'../../../fonts/CircularStd-Bold.woff'",
    wof2f: "'../../../fonts/CircularStd-Bold.woff2'",
  },
  CircularStdBook: {
    name: "'Circular Std Book'",
    family: "'Circular Std Book', Arial, sans-serif",
    woff: "'../../../fonts/CircularStd-Book.woff'",
    wof2f: "'../../../fonts/CircularStd-Book.woff2'",
  },
};
