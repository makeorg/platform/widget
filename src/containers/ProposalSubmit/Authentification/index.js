import React from 'react';
import { connect } from 'react-redux';
import AuthentificationComponent from '../../../components/AuthentificationComponent';
import {
  showRegister,
  showLogin,
} from '../../../shared/store/actions/pannelAction';

class ProposalSubmitAuthentification extends React.Component {
  render() {
    const { handleShowLogin, handleShowRegister } = this.props;

    return (
      <AuthentificationComponent
        handleShowLogin={handleShowLogin}
        handleShowRegister={handleShowRegister}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  handleShowLogin: () => dispatch(showLogin()),
  handleShowRegister: () => dispatch(showRegister()),
});
export default connect(
  null,
  mapDispatchToProps
)(ProposalSubmitAuthentification);
