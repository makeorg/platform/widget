import React from 'react';
import { connect } from 'react-redux';
import styled from '@emotion/styled';
import { CSSTransition } from 'react-transition-group';
import { rem } from 'polished';
import * as proposalActions from '../../shared/store/actions/proposalAction';
import { sequenceCollapse } from '../../shared/store/actions/sequenceAction';
import { getBaitText } from '../../shared/constants/proposal';
import {
  getProposalLength,
  getIsProposalValidLength,
} from '../../helpers/proposalHelper';
import Tracking from '../../services/Tracking';
import ProposalSubmitAuthentificationContainer from './Authentification';
import ProposalSubmitComponent from '../../components/ProposalSubmitComponent';
import ProposalSubmitDescriptionComponent from '../../components/ProposalSubmit/Description/index';
import ProposalSubmitSuccessComponent from '../../components/ProposalSubmit/Success';
import { Breakpoints } from '../../assets/styles/breakpoints';
import { throttle } from '../../shared/helpers/throttle';

export const ProposalSubmitWrapper = styled.div`
  padding: 0 ${rem('15px')};
  width: calc(100% - ${rem('30px')});
  margin: 0 auto;
  max-width: ${rem(Breakpoints.appMaxWidth)};
  @media (min-width: ${rem(Breakpoints.mobile)}) {
    padding: 0 ${rem('30px')};
    width: calc(100% - ${rem('60px')});
  }
`;

/**
 * ProposalSubmitContainer manage the proposal Submit Component business logic
 * @extends React
 */
export class ProposalSubmit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTyping: false,
      isExpanded: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.trackModerationText = this.trackModerationText.bind(this);
    this.trackModerationLink = this.trackModerationLink.bind(this);

    this.throttleSubmit = throttle(this.handleSubmit);
  }

  handleChange(event) {
    const content = event.target.value;
    const length = getProposalLength(content);
    const canSubmit = getIsProposalValidLength(length);
    const { handleTypingProposal } = this.props;

    handleTypingProposal(content, length, canSubmit);
  }

  handleFocus(event) {
    this.setState({
      isTyping: true,
    });

    const { handleCollapseSequence, isSequenceCollapsed } = this.props;
    if (!isSequenceCollapsed) handleCollapseSequence();
  }

  handleSubmit(event) {
    event.preventDefault();

    const { isLoggedIn, content, handleSubmitProposal } = this.props;

    Tracking.trackClickProposalSubmit(`${getBaitText()}${content}`);
    this.setState({
      isTyping: false,
    });

    if (isLoggedIn) {
      handleSubmitProposal(content);
    }
  }

  trackModerationText() {
    Tracking.trackDisplayModerationText();
    return this;
  }

  trackModerationLink() {
    Tracking.trackClickModerationLink();
    return this;
  }

  render() {
    const {
      content,
      length,
      canSubmit,
      isSubmitSuccess,
      isLoggedIn,
      isSequenceCollapsed,
    } = this.props;
    const { isTyping } = this.state;
    const isDescriptionShown =
      isTyping && !isSubmitSuccess && isSequenceCollapsed;
    const isAuthentificationShown =
      !isTyping && !isLoggedIn && isSequenceCollapsed;
    const isSuccessShown = !isTyping && isSubmitSuccess && isSequenceCollapsed;

    return (
      <React.Fragment>
        <ProposalSubmitWrapper>
          <ProposalSubmitComponent
            isTyping={isTyping}
            content={content}
            length={length}
            canSubmit={canSubmit}
            handleChange={this.handleChange}
            handleSubmit={this.throttleSubmit}
            handleFocus={this.handleFocus}
          />
        </ProposalSubmitWrapper>
        <ProposalSubmitWrapper>
          <CSSTransition
            in={isDescriptionShown}
            mountOnEnter={true}
            unmountOnExit={true}
            timeout={250}
            classNames="fadeIn"
          >
            <ProposalSubmitDescriptionComponent
              trackModerationText={this.trackModerationText}
              trackModerationLink={this.trackModerationLink}
            />
          </CSSTransition>
          <CSSTransition
            in={isSuccessShown}
            mountOnEnter={true}
            unmountOnExit={true}
            timeout={250}
            classNames="fadeIn"
          >
            <ProposalSubmitSuccessComponent />
          </CSSTransition>
          <CSSTransition
            in={isAuthentificationShown}
            mountOnEnter={true}
            unmountOnExit={true}
            timeout={250}
            classNames="fadeIn"
          >
            <ProposalSubmitAuthentificationContainer />
          </CSSTransition>
        </ProposalSubmitWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { isLoggedIn } = state.authentification;
  const { questionId } = state.config;
  const { content, length, canSubmit, isSubmitSuccess } = state.proposal;
  const { isSequenceCollapsed } = state.sequence;

  return {
    isLoggedIn,
    questionId,
    content,
    length,
    canSubmit,
    isSubmitSuccess,
    isSequenceCollapsed,
  };
};

const mapDispatchToProps = dispatch => ({
  handleCollapseSequence: () => dispatch(sequenceCollapse()),
  handleTypingProposal: (content, length, canSubmit) =>
    dispatch(proposalActions.typingProposal(content, length, canSubmit)),
  handleSubmitProposal: content =>
    dispatch(proposalActions.submitProposal(content)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalSubmit);
