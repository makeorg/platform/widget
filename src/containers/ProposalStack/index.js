import React, { Component } from 'react';
import { connect } from 'react-redux';
import QuestionService from '../../api/QuestionService';
import { sequenceExpand } from '../../shared/store/actions/sequenceAction';
import ProposalStackComponent from '../../components/ProposalStackComponent';

export const doDecrementCounter = prevState => {
  return {
    currentIndex: prevState.currentIndex - 1,
    progress: prevState.progress + 1,
    showTooltip: true,
  };
};

export const doIncrementCounter = prevState => {
  return {
    currentIndex: prevState.currentIndex + 1,
    progress: prevState.progress - 1,
    showTooltip: true,
  };
};
/**
 * stack of Proposals Component
 * @extends Component
 */
class ProposalStack extends Component {
  constructor(props) {
    super(props);
    this.state = {
      proposals: [],
      total: 0,
      currentIndex: 0,
      progress: 1,
      isTooltipShown: false,
    };
    this.decrementCardCounter = this.decrementCardCounter.bind(this);
    this.incrementCardCounter = this.incrementCardCounter.bind(this);
    this.showTooltip = this.showTooltip.bind(this);
    this.hideTooltip = this.hideTooltip.bind(this);
  }

  componentDidMount() {
    QuestionService.startSequence(this.props.question.slug, this.props.tagsIds)
      .then(response => this.setProposals(response))
      .catch(error => error);

    this.setState({
      isTooltipShown: true,
    });
  }

  setProposals = response => {
    this.setState({
      proposals: response.results,
      total: response.total,
    });
  };

  showTooltip() {
    this.setState({
      isTooltipShown: true,
    });
  }

  hideTooltip() {
    this.setState({
      isTooltipShown: false,
    });
  }

  decrementCardCounter() {
    this.setState(doDecrementCounter);
  }

  incrementCardCounter() {
    this.setState(doIncrementCounter);
  }

  render() {
    const { votes, isSequenceCollapsed, handleExpandSequence } = this.props;
    const {
      proposals,
      total,
      progress,
      currentIndex,
      isTooltipShown,
    } = this.state;
    const progressPercent = Math.floor(
      (this.state.progress / this.state.total) * 100
    );

    return (
      <ProposalStackComponent
        proposals={proposals}
        votes={votes}
        total={total}
        currentIndex={currentIndex}
        progress={progress}
        progressPercent={progressPercent}
        isTooltipShown={isTooltipShown}
        isSequenceCollapsed={isSequenceCollapsed}
        showTooltip={this.showTooltip}
        hideTooltip={this.hideTooltip}
        handleExpandSequence={handleExpandSequence}
        incrementCardCounter={this.incrementCardCounter}
        decrementCardCounter={this.decrementCardCounter}
      />
    );
  }
}

const mapStateToProps = state => {
  const { question } = state.config;
  const { votes } = state.vote;
  const { isSequenceCollapsed } = state.sequence;

  return {
    question,
    votes,
    isSequenceCollapsed,
  };
};

const mapDispatchToProps = dispatch => ({
  handleExpandSequence: () => dispatch(sequenceExpand()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalStack);
