export const Storage = {
  setItem: (key, value) => {
    try {
      localStorage.setItem(key, value);
    } catch (error) {
      console.error(`set "${key}" in local storage failed`);
    }
  },
  getItem: key => {
    try {
      return localStorage.getItem(key);
    } catch (error) {
      console.error(`get "${key}" in local storage failed`);

      return null;
    }
  },
  removeItem: key => {
    try {
      localStorage.removeItem(key);
    } catch (error) {
      console.error(`remove "${key}" in local storage failed`);
    }
  },
};
