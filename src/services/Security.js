const configuration = {
  retrieveAccessToken: () => null,
  refreshToken: null,
  accessToken: null,
};

const resetStoredTokens = () => {
  configuration.refreshToken = null;
  configuration.accessToken = null;
};

export const initSecurity = retrieveAccessToken => {
  configuration.retrieveAccessToken = retrieveAccessToken;
};

export const storeTokens = (accessToken, refreshToken) => {
  configuration.refreshToken = refreshToken;
  configuration.accessToken = accessToken;
};

export const refreshToken = async () => {
  const refreshToken = configuration.refreshToken;

  const OauthResponse = refreshToken
    ? await configuration.retrieveAccessToken(refreshToken)
    : null;
  if (!OauthResponse) {
    resetStoredTokens();
    return null;
  }

  const { access_token, refresh_token } = OauthResponse;

  storeTokens(access_token, refresh_token);

  return access_token;
};
