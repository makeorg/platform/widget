import ApiService from '../api/ApiService';
import * as trackingConstants from '../constants/tracking';

const PARENT_URL =
  window &&
  typeof window !== 'undefined' &&
  window.parent &&
  typeof window.document !== 'undefined' &&
  window.document.location &&
  window.location !== window.parent.location
    ? window.document.referrer
    : window.location.href;

const PATH_POST_TRACKING = '/tracking/front';

let instance = null;

const track = (eventName, parameters = {}) => {
  let eventParameters = {
    ...{
      location: 'widget',
      source: ApiService.source,
      country: ApiService.country,
      language: ApiService.language,
      url: PARENT_URL,
      version: 'v2',
    },
    ...parameters,
  };

  return ApiService.callApi(PATH_POST_TRACKING, {
    method: 'POST',
    body: JSON.stringify({
      eventName: eventName,
      eventParameters: eventParameters,
      eventType: 'trackCustom',
    }),
  });
};

class Tracking {
  constructor() {
    if (!instance) {
      instance = this;
    }

    return instance;
  }

  trackDisplayIntro = tags => {
    track(trackingConstants.DISPLAY_INTRO, { tags: tags });
  };

  trackClickLearnMore = () => {
    track(trackingConstants.CLICK_LEARN_MORE);
  };

  trackClickLearnMoreLink = () => {
    track(trackingConstants.CLICK_LEARN_MORE_LINK);
  };

  trackVote = (proposalId, nature, cardPosition) => {
    track(trackingConstants.CLICK_PROPOSAL_VOTE, {
      proposalId: proposalId,
      nature: nature,
      'card-position': cardPosition.toString(),
    });
  };

  trackUnvote = (proposalId, nature, cardPosition) => {
    track(trackingConstants.CLICK_PROPOSAL_UNVOTE, {
      proposalId: proposalId,
      nature: nature,
      'card-position': cardPosition.toString(),
    });
  };

  trackQualify = (proposalId, type, nature, cardPosition) => {
    return track(trackingConstants.CLICK_PROPOSAL_QUALIFY, {
      proposalId: proposalId,
      type: type,
      nature: nature,
      'card-position': cardPosition.toString(),
    });
  };

  trackUnqualify = (proposalId, type, nature, cardPosition) => {
    return track(trackingConstants.CLICK_PROPOSAL_UNQUALIFY, {
      proposalId: proposalId,
      type: type,
      nature: nature,
      'card-position': cardPosition.toString(),
    });
  };

  trackClickNextCard = cardPosition => {
    return track(trackingConstants.CLICK_SEQUENCE_NEXT_CARD, {
      'card-position': cardPosition.toString(),
    });
  };

  trackClickPreviousCard = cardPosition => {
    return track(trackingConstants.CLICK_SEQUENCE_PREVIOUS_CARD, {
      'card-position': cardPosition.toString(),
    });
  };

  trackClickSignupCardEmail = () => {
    return track(trackingConstants.CLICK_SIGN_UP_CARD_EMAIL);
  };

  trackClickSignupCardFacebook = () => {
    return track(trackingConstants.CLICK_SIGN_UP_CARD_FACEBOOK);
  };

  trackDisplaySignupForm = () => {
    return track(trackingConstants.DISPLAY_SIGN_UP_FORM);
  };

  trackDisplaySigninForm = () => {
    return track(trackingConstants.DISPLAY_SIGN_IN_FORM);
  };

  trackClickProposalSubmitFormOpen = tags => {
    return track(trackingConstants.CLICK_PROPOSAL_SUBMIT_FORM_OPEN, {
      tags: tags,
    });
  };

  trackDisplayProposalSubmitValidation = () => {
    return track(trackingConstants.DISPLAY_PROPOSAL_SUBMIT_VALIDATION);
  };

  trackClickProposalSubmit = content => {
    return track(trackingConstants.CLICK_PROPOSAL_SUBMIT, { content });
  };

  trackAuthenSocialSuccess = socialNetwork => {
    return track(trackingConstants.AUTHEN_SOCIAL_SUCCESS, {
      'social-network': socialNetwork,
    });
  };

  trackAuthenSocialFailure = socialNetwork => {
    return track(trackingConstants.AUTHEN_SOCIAL_FAILURE, {
      'social-network': socialNetwork,
    });
  };

  trackSignupEmailSuccess = socialNetwork => {
    return track(trackingConstants.SIGN_UP_EMAIL_SUCCESS);
  };

  trackSignupEmailFailure = socialNetwork => {
    return track(trackingConstants.SIGN_UP_EMAIL_FAILURE);
  };

  trackSigninEmailSuccess = socialNetwork => {
    return track(trackingConstants.SIGN_IN_EMAIL_SUCCESS);
  };

  trackSigninEmailFailure = socialNetwork => {
    return track(trackingConstants.SIGN_IN_EMAIL_FAILURE);
  };

  trackDisplayFinalCard = () => {
    return track(trackingConstants.DISPLAY_FINAL_CARD);
  };

  trackDisplayModerationText = () => {
    return track(trackingConstants.DISPLAY_MODERATION_TEXT);
  };

  trackClickModerationLink = () => {
    return track(trackingConstants.CLICK_MODERATION_LINK);
  };
}

export default new Tracking();
