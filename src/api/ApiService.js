import axios from 'axios';
import { APP_NAME } from '../constants/config';
import { env } from '../shared/constants/env';

const HOSTNAME =
  (typeof window !== 'undefined' &&
    window &&
    window.location &&
    window.location.hostname) ||
  null;
const BROWSER_API_URL =
  typeof window !== 'undefined' &&
  window &&
  window.API_URL &&
  window.API_URL !== '__API_URL__'
    ? window.API_URL
    : undefined;

export const API_URL =
  BROWSER_API_URL || process.env.API_URL || 'https://api.preprod.makeorg.tech';

let instance = null;

/**
 * Api calls service
 */
class ApiService {
  constructor() {
    if (!instance) {
      instance = this;
    }

    this._questionId = '';
    this._source = '';
    this._language = null;
    this._country = null;
    this._sessionId = '';
    this._visitorId = '';
    this._sessionIdExpiration = '';
    this._token = '';
    this._updateSessionExpiration = () => {};
    this._refreshSessionIdExpiration = () => {};
    this._refreshTokenCallback = () => {};
    this._onResponseCallback = () => {};

    return instance;
  }

  set questionId(questionId) {
    this._questionId = questionId;
  }

  get questionId() {
    return this._questionId;
  }

  set source(source) {
    this._source = source;
  }

  get source() {
    return this._source;
  }

  set language(language) {
    this._language = language;
  }

  get language() {
    return this._language;
  }

  set country(country) {
    this._country = country;
  }

  get country() {
    return this._country;
  }

  set sessionId(sessionId) {
    this._sessionId = sessionId;
  }

  get sessionId() {
    return this._sessionId;
  }

  set sessionIdExpiration(sessionIdExpiration) {
    this._sessionIdExpiration = sessionIdExpiration;
  }

  get sessionIdExpiration() {
    return this._sessionIdExpiration;
  }

  set visitorId(visitorId) {
    this._visitorId = visitorId;
  }

  get visitorId() {
    return this._visitorId;
  }

  set token(token) {
    this._token = token;
  }

  get token() {
    return this._token;
  }

  set updateSessionExpiration(callback) {
    this._updateSessionExpiration = callback;
  }

  get updateSessionExpiration() {
    return this._updateSessionExpiration;
  }

  set refreshTokenCallback(callback) {
    this._refreshTokenCallback = callback;
  }

  set onResponseCallback(callback) {
    this._onResponseCallback = callback;
  }

  _generateHeaders(optionHeaders) {
    let headers = {
      ...{
        'Content-Type': 'application/json; charset=UTF-8',
        'x-hostname': HOSTNAME,
        'x-make-country': this._country,
        'x-make-language': this._language,
        'x-make-location': 'widget',
        'x-make-source': this._source,
        'x-make-question': this._questionId,
        'x-make-question-id': this._questionId,
        'x-make-app-name': APP_NAME,
      },
      ...optionHeaders,
    };

    if (this._sessionId) {
      headers = {
        ...headers,
        'x-session-id': this._sessionId,
      };
    }

    if (this._visitorId) {
      headers = {
        ...headers,
        'x-visitor-id': this._visitorId,
      };
    }

    if (this._token) {
      headers = {
        ...headers,
        Authorization: `Bearer ${this._token}`,
      };
    }

    if (env.isDev()) {
      console.log(headers);
    }

    return headers;
  }

  _handleResponse = async response => {
    const { status } = response || {};
    switch (status) {
      case 200:
      case 201:
        return response.data;
      case 204:
        return response.status;
      default:
        throw response.status;
    }
  };

  _fetchRetry = async (url, options = {}, retry = 5) => {
    try {
      return await axios({
        method: options.method,
        url,
        headers: this._generateHeaders(options.headers),
        data: options.body,
        withCredentials: true,
        validateStatus: status => status < 400,
      });
    } catch (error) {
      if (retry <= 1) {
        throw error;
      }

      const { response } = error;
      const { status } = response || {};

      if (status === 400) {
        throw error.response.data;
      }

      if (status === 401) {
        if (!this._token) {
          throw error;
        }
        this._token = null;
        this._token = await this._refreshTokenCallback();
      }

      return this._fetchRetry(url, options, retry - 1);
    }
  };

  callApi(url, options = {}) {
    return this._fetchRetry(`${API_URL}${url}`, {
      method: options.method,
      headers: this._generateHeaders(options.headers),
      body: options.body,
    })
      .then(response => {
        this._onResponseCallback(response);
        return this._handleResponse(response);
      })
      .catch(error => {
        if (error.response) {
          this._onResponseCallback(error.response);
        }
        throw error;
      });
  }
}

export default new ApiService();
