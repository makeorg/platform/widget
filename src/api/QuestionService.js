import ApiService from './ApiService';

const PATH_QUESTION_START_SEQUENCE = '/sequences/tags/:questionSlug';
const PATH_QUESTION_DETAIL = '/questions/:questionSlug/details';

export default class QuestionService {
  static getDetail(questionSlug) {
    return ApiService.callApi(
      PATH_QUESTION_DETAIL.replace(':questionSlug', questionSlug),
      {
        method: 'GET',
      }
    );
  }

  static startSequence(questionSlug, tagsIds = []) {
    let startSequenceUrl = PATH_QUESTION_START_SEQUENCE.replace(
      ':questionSlug',
      questionSlug
    );

    const tagsParam = tagsIds
      .filter(tagId => !!tagId)
      .map(tagId => (tagsIds = `tagsIds=${tagId}`))
      .join('&');

    startSequenceUrl += tagsParam && `?${tagsParam}`;

    return ApiService.callApi(startSequenceUrl, { method: 'GET' });
  }
}
