import ApiService from './ApiService';
import { getDateOfBirthFromAge } from '../helpers/userHelper';

const PATH_USER_ME = '/user/me';
export const PATH_USER_LOGIN = '/oauth/make_access_token';
const PATH_USER_LOGOUT = '/logout';
const PATH_USER_LOGIN_SOCIAL = '/user/login/social';
const PATH_USER_REGISTER = '/user';
const PATH_USER_FORGOT_PASSWORD = '/user/reset-password/request-reset';
const PATH_USER_PRIVACY_POLICY = '/user/privacy-policy';
const PATH_USER_SOCIAL_PRIVACY_POLICY = '/user/social/privacy-policy';

export const FACEBOOK_PROVIDER_ENUM = 'facebook';
export const GOOGLE_PROVIDER_ENUM = 'google_people';

export class UserService {
  /**
   * Get user info
   * @return {Promise}
   */
  static me() {
    return ApiService.callApi(PATH_USER_ME, {
      method: 'GET',
    });
  }

  /**
   * Login the user
   * @param  {String} email
   * @param  {String} password
   * @param  {Boolean} approvePrivacyPolicy
   * @return {Promise}
   */
  static login(email, password, approvePrivacyPolicy) {
    const data = {
      username: email,
      password,
      grant_type: 'password',
      approvePrivacyPolicy,
    };

    return ApiService.callApi(PATH_USER_LOGIN, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: Object.keys(data)
        .map(key => {
          return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
        })
        .join('&'),
    });
  }

  /**
   * Logout the user
   *
   * @return {Promise}
   */
  static logout() {
    return ApiService.callApi(PATH_USER_LOGOUT, {
      method: 'POST',
    });
  }

  /**
   * Login the user vi a social account
   * @param  {String} provider login scoial type (google, facebook..)
   * @param  {String} token
   * @param  {Boolean} approvePrivacyPolicy
   * @return {Promise}
   */
  static loginSocial(provider, token, approvePrivacyPolicy) {
    return ApiService.callApi(PATH_USER_LOGIN_SOCIAL, {
      method: 'POST',
      body: JSON.stringify({
        provider,
        token,
        country: ApiService.country,
        language: ApiService.language,
        approvePrivacyPolicy,
      }),
    });
  }

  /**
   * Register a user
   * @param  {Object}  user
   * @param  {string}  questionId
   * @return {Promise}
   */
  static register(user) {
    return ApiService.callApi(PATH_USER_REGISTER, {
      method: 'POST',
      body: JSON.stringify({
        email: user.email,
        password: user.password,
        firstName: user.firstname,
        dateOfBirth: getDateOfBirthFromAge(user.age),
        postalCode: user.postalCode,
        legalMinorConsent: user.legalMinorConsent,
        legalAdvisorApproval: user.legalAdvisorApproval,
        approvePrivacyPolicy: user.approvePrivacyPolicy,
        country: ApiService.country,
        language: ApiService.language,
        questionId: ApiService.questionId,
      }),
    });
  }

  static forgotPassword(email) {
    return ApiService.callApi(PATH_USER_FORGOT_PASSWORD, {
      method: 'POST',
      body: JSON.stringify({
        email: email,
      }),
    });
  }

  /**
   * get user privacy policy acceptance date for login
   * @param  {String}  email
   * @param  {String}  password
   * @param  {ApiServiceHeadersType} headers
   */
  static loginPrivacyPolicy(email, password) {
    return ApiService.callApi(PATH_USER_PRIVACY_POLICY, {
      method: 'POST',
      body: JSON.stringify({ email, password }),
    });
  }

  /**
   * get user privacy policy acceptance date for social connect
   * @param  {String}  provider
   * @param  {String}  token
   * @param  {ApiServiceHeadersType} headers
   */
  static socialPrivacyPolicy(provider, token) {
    return ApiService.callApi(PATH_USER_SOCIAL_PRIVACY_POLICY, {
      method: 'POST',
      body: JSON.stringify({ provider, token }),
    });
  }
}
