import ApiService from './ApiService';

const PATH_GET_TAG = '/tags/:tagId';

export default class TagService {
  static getById(tagId) {
    return ApiService.callApi(PATH_GET_TAG.replace(':tagId', tagId), {
      method: 'GET',
    });
  }
}
