import ApiService from './ApiService';

const PATH_PROPOSAL_PROPOSE = '/proposals';

export default class ProposalService {
  static propose(content, questionId) {
    return ApiService.callApi(PATH_PROPOSAL_PROPOSE, {
      method: 'POST',
      body: JSON.stringify({
        content: content,
        questionId: questionId,
        country: 'FR',
        language: 'fr',
      }),
    });
  }
}
