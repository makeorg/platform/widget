import 'react-app-polyfill/ie11';
import 'core-js/fn/array/find';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import i18next from 'i18next';
import { env } from './shared/constants/env';
import { initialStateDebug } from './shared/store/initialState.debug';
import { configureStore, authenticationState } from './shared/store';
import ApiService from './api/ApiService';
import { unregister } from './registerServiceWorker';
import App from './app';
import { TRANSLATION_NAMESPACE } from './shared/constants/i18n';
import { refreshToken, initSecurity } from './services/Security';
import { PATH_USER_LOGIN } from './api/UserService';

let initialState = window.__INITIAL_STATE__;

if (env.isDev()) {
  initialState = initialStateDebug;
}

const { config } = initialState;
const tradLanguage = `${config.language}-${config.country}`;

i18next.init({
  lng: tradLanguage,
  debug: env.isDev(),
});
i18next.addResourceBundle(
  tradLanguage,
  TRANSLATION_NAMESPACE,
  config.translations
);
i18next.changeLanguage(tradLanguage);

const initApp = async state => {
  const { config } = state;
  ApiService.questionId = config.question.questionId;
  ApiService.source = config.source;
  ApiService.country = config.country;
  ApiService.language = config.language;
  ApiService.refreshTokenCallback = refreshToken;
  ApiService.sessionId = '';
  ApiService.visitorId = '';
  ApiService.sessionIdExpiration = '';

  const handleApiResponse = response => {
    // get values from response headers
    const sessionId = response.headers['x-session-id'];
    const sessionIdExpiration = response.headers['x-session-id-expiration'];
    const visitorId = response.headers['x-visitor-id'];

    // set sessionId
    if (sessionId && sessionIdExpiration) {
      ApiService.sessionId = sessionId;
      ApiService.sessionIdExpiration = sessionIdExpiration;
    }

    // set visitorID
    if (visitorId) {
      ApiService.visitorId = visitorId;
    }

    // get method to update Session Expiration value
    ApiService.updateSessionExpiration();
  };

  ApiService.onResponseCallback = handleApiResponse;

  const retrieveAccessToken = async refreshToken => {
    const data = {
      grant_type: 'refresh_token',
      refresh_token: refreshToken,
    };

    return await ApiService.callApi(PATH_USER_LOGIN.replace(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: Object.keys(data)
        .map(key => {
          return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
        })
        .join('&'),
    }).catch(error => {
      return null;
    });
  };

  initSecurity(retrieveAccessToken);

  const authentificationState = await authenticationState();

  const store = configureStore({
    ...state,
    authentification: {
      ...state.authentification,
      ...authentificationState,
    },
  });

  ReactDOM.hydrate(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );
};

initApp(initialState);

unregister();
