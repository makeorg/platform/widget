import * as URL from '../constants/url';
import ApiService from '../api/ApiService';

export const localizeLink = (link, country) => {
  if (!country) {
    return null;
  }

  return `https://make.org/${country.toUpperCase()}/${link}`;
};

export const localizeCguLink = () =>
  localizeLink(URL.CGU_LINK, ApiService.country);

export const localizePrivacyPolicy = () =>
  localizeLink(URL.DATA_POLICY_LINK, ApiService.country);

export const localizeDataPolicyLink = () =>
  localizeLink(URL.DATA_POLICY_LINK, ApiService.country);

export const localizeModerationCharterLink = () => {
  if (ApiService.country === 'FR') {
    return URL.MODERATION_CHARTER_FR_LINK;
  }

  return URL.MODERATION_CHARTER_EN_LINK;
};
