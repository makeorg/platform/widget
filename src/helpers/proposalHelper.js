import {
  getBaitText,
  MIN_PROPOSAL_LENGTH,
  MAX_PROPOSAL_LENGTH,
} from '../shared/constants/proposal';

export const getProposalLength = (content = null) => {
  if (content === null) {
    return getBaitText().length;
  }

  return (getBaitText() + content).length;
};

export const getIsProposalValidLength = (length = null) => {
  if (length === null) {
    return false;
  }

  return length >= MIN_PROPOSAL_LENGTH && length <= MAX_PROPOSAL_LENGTH;
};
