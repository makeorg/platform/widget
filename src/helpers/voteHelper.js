import {
  VOTE_AGREE_KEY,
  VOTE_DISAGREE_KEY,
  VOTE_NEUTRAL_KEY,
  COLOR_AGREE,
  COLOR_DISAGREE,
  COLOR_NEUTRAL,
  ORIENTATION_AGREE,
  ORIENTATION_DISAGREE,
  ORIENTATION_NEUTRAL,
} from '../shared/constants/vote';

export const getVoteByVoteKey = (voteKey, votes) => {
  return votes.find(vote => vote.voteKey === voteKey);
};

export const getVotePercent = (totalVoteCount, voteCount) => {
  if (voteCount === 0 || totalVoteCount === 0) {
    return 0;
  }

  return Math.round((voteCount / totalVoteCount) * 100);
};

export const getVoteColor = voteKey => {
  switch (voteKey) {
    case VOTE_AGREE_KEY:
      return COLOR_AGREE;
    case VOTE_DISAGREE_KEY:
      return COLOR_DISAGREE;
    case VOTE_NEUTRAL_KEY:
      return COLOR_NEUTRAL;
    default:
      return 'COLOR_NOT_FOUND';
  }
};

export const getVoteOrientation = voteKey => {
  switch (voteKey) {
    case VOTE_AGREE_KEY:
      return ORIENTATION_AGREE;
    case VOTE_DISAGREE_KEY:
      return ORIENTATION_DISAGREE;
    case VOTE_NEUTRAL_KEY:
      return ORIENTATION_NEUTRAL;
    default:
      return 'ORIENTATION_NOT_FOUND';
  }
};

export const getUserVoteSharePercent = votes => {
  if (votes.length === 0) {
    return 0;
  }

  const voteResult = votes.map(vote => vote.result);

  let totalVotesCount = 0;
  let userVotesCount = 0;

  voteResult.forEach(element => {
    totalVotesCount += element
      .map(vote => vote.count)
      .reduce((prev, next) => prev + next);
    userVotesCount += element
      .filter(vote => vote.hasVoted === true)
      .map(vote => vote.count)
      .reduce((prev, next) => prev + next, 0);
  });

  return Math.round((userVotesCount / totalVotesCount) * 100);
};
