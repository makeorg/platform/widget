import i18next from 'i18next';

export const errorTranslation = apiError => {
  if (/Email\s(.+)\salready exist/.test(apiError)) {
    return i18next.t('common.form.email_already_exist');
  }

  if (apiError === 'Privacy policy must be approved.') {
    return i18next.t('register.approve_privacy_policy');
  }

  const translatedError = i18next.t(`register.${apiError}`);
  if (translatedError === undefined) {
    return apiError;
  }

  return translatedError;
};
