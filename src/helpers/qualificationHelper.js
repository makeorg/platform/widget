/**
 * render qualification component
 * @param  {Array}       votes list of proposal vote
 *
 * @return {Object|null} an object of the user vote
 */
export const findQualificationByKey = (qualifications, qualificationKey) => {
  return qualifications.find(
    qualification => qualificationKey === qualification.qualificationKey
  );
};

export const getQualificationCount = (userQualifications, qualificationKey) => {
  const qualification = findQualificationByKey(
    userQualifications,
    qualificationKey
  );
  if (qualification != null) {
    return qualification.count;
  }

  return '+1';
};

export const userHasQualified = (userQualifications, qualificationKey) => {
  return (
    userQualifications.filter(
      qualification => qualification.qualificationKey === qualificationKey
    ).length > 0
  );
};
