export const getDateOfBirthFromAge = (age = '') => {
  if (!age) {
    return null;
  }

  const birthYear = new Date().getFullYear() - Number(age);

  return `${birthYear}-01-01`;
};

export const checkPrivacyPolicyDate = (
  privacyPolicyDate,
  apiResponse,
  modalAction,
  success
) => {
  const lastVersion = new Date(privacyPolicyDate);

  let userAcceptance;

  if (apiResponse.privacyPolicyApprovalDate != null) {
    userAcceptance = new Date(apiResponse.privacyPolicyApprovalDate);
  }

  if (!userAcceptance || userAcceptance < lastVersion) {
    modalAction();
    return;
  }

  success();
};
