export const getIndex = (index, currentIndex) => index + currentIndex;

export const getCardPosition = (index, currentIndex) =>
  getIndex(index, currentIndex) * 2;

export const getZIndex = (index, currentIndex) =>
  50 - getIndex(index, currentIndex);

export const getScale = (index, currentIndex) =>
  1 - getIndex(index, currentIndex) / 75;
