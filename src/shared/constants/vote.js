import { Colors } from '../../assets/styles/colors';

export const VOTE_AGREE_KEY = 'agree';
export const VOTE_DISAGREE_KEY = 'disagree';
export const VOTE_NEUTRAL_KEY = 'neutral';

// Qualifications of Agree vote
export const QUALIFICATION_LIKEIT_KEY = 'likeIt';
export const QUALIFICATION_DOABLE_KEY = 'doable';
export const QUALIFICATION_PLATITUDEAGREE_KEY = 'platitudeAgree';

// Qualifications of Disagree Vote
export const QUALIFICATION_NOWAY_KEY = 'noWay';
export const QUALIFICATION_IMPOSSIBLE_KEY = 'impossible';
export const QUALIFICATION_PLATITUDEDISAGREE_KEY = 'platitudeDisagree';

// Qualifications of Neutral Vote
export const QUALIFICATION_DONOTUNDERSTAND_KEY = 'doNotUnderstand';
export const QUALIFICATION_NOOPINION_KEY = 'noOpinion';
export const QUALIFICATION_DONOTCARE_KEY = 'doNotCare';

// Button of Agree Vote
export const COLOR_AGREE = Colors.GreenVote;
export const POSITION_AGREE = '22%, 18%';
export const ORIENTATION_AGREE = '0';
export const SCALE_AGREE = '1';

// Button of Disagree Vote
export const COLOR_DISAGREE = Colors.RedVote;
export const POSITION_DISAGREE = '24%, 76%';
export const ORIENTATION_DISAGREE = '180deg';
export const SCALE_DISAGREE = '-1';

// Button of Neutral Vote
export const COLOR_NEUTRAL = Colors.GreyVote;
export const POSITION_NEUTRAL = '18%, 74%';
export const ORIENTATION_NEUTRAL = '-90deg';
export const SCALE_NEUTRAL = '1';

export const VOTE_ACTION = 'USER_VOTE_ACTION';
export const UNVOTE_ACTION = 'USER_UNVOTE_ACTION';
