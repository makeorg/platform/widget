import i18next from 'i18next';

export const proposalConstants = {
  PROPOSE_TYPING: 'PROPOSAL_PROPOSE_TYPING',
  PROPOSE_REQUEST: 'PROPOSAL_PROPOSE_REQUEST',
  PROPOSE_SUCCESS: 'PROPOSAL_PROPOSE_SUCCESS',
  PROPOSE_FAILURE: 'PROPOSAL_PROPOSE_FAILURE',
};

export const MIN_PROPOSAL_LENGTH = 12;
export const MAX_PROPOSAL_LENGTH = 140;
export const getBaitText = () =>
  i18next.t('proposal_submit.bait') || 'il faut ';
