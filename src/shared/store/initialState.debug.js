import {
  DEFAULT_SOURCE,
  DEFAULT_COLOR,
  DEFAULT_FONT,
} from '../../constants/enum';
import { PRIVACY_POLICY_DATE } from '../constants/config';

export const initialStateDebug = {
  authentification: {
    isLoggedIn: false,
    user: null,
    errors: [],
  },
  config: {
    question: {
      slug: 'environnement',
      questionId: 'c8375a43-81e8-472d-9a3a-f23f8061d21f',
      canPropose: true,
    },
    tagsIds: [],
    showPannel: false,
    title: '',
    source: DEFAULT_SOURCE,
    themeColor: DEFAULT_COLOR,
    font: DEFAULT_FONT,
    country: 'FR',
    language: 'fr',
    socialConnect: true,
    translations: require('../../shared/i18n/static/fr-FR'),
    privacyPolicyDate: PRIVACY_POLICY_DATE,
  },
};
