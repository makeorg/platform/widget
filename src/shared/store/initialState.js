import { PRIVACY_POLICY_DATE } from '../constants/config';
import { getBaitText } from '../constants/proposal';

export const initialState = {
  session: { expirationDate: null, isExpired: false },
  config: {
    showPannel: false,
    pannelRender: null,
    privacyPolicyDate: PRIVACY_POLICY_DATE,
  },
  authentification: {
    isLoggedIn: false,
    user: null,
    errors: [],
  },
  forgotPassword: { errors: [], isSuccess: false },
  registration: { errors: [], user: null },
  sequence: {
    isSequenceCollapsed: false,
  },
  proposal: {
    isTyping: false,
    canSubmit: false,
    isSubmitSuccess: false,
    content: '',
    length: getBaitText().length,
    questionId: null,
    error: null,
  },
};
