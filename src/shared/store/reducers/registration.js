import { userConstants } from '../../constants/user';
import { initialState } from '../initialState';

export default function registration(
  state = initialState.registration,
  action
) {
  switch (action.type) {
    case userConstants.REGISTER_REQUEST:
      return {
        ...state,
        errors: [],
      };
    case userConstants.REGISTER_SUCCESS:
      return {
        ...state,
        user: action.user,
        errors: [],
      };
    case userConstants.REGISTER_FAILURE:
      return {
        ...state,
        errors: action.errors,
      };
    default:
      return state;
  }
}
