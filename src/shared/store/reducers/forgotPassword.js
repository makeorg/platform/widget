import { userConstants } from '../../constants/user';
import { initialState } from '../initialState';

export default function forgotPassword(
  state = initialState.forgotPassword,
  action
) {
  switch (action.type) {
    case userConstants.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        isSuccess: false,
        errors: [],
      };
    case userConstants.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        errors: [],
      };
    case userConstants.FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        errors: action.errors,
      };
    default:
      return state;
  }
}
