import * as actionTypes from '../../constants/session';
import { initialState } from '../initialState';

export default function session(state = initialState.session, action) {
  switch (action.type) {
    case actionTypes.SET_EXPIRATION_DATE:
      return {
        ...state,
        expirationDate: action.date,
      };
    case actionTypes.EXPIRED_SESSION:
      return {
        ...state,
        isExpired: true,
      };

    default:
      return state;
  }
}
