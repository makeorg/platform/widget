import { userConstants } from '../../constants/user';
import { initialState } from '../initialState';

export default function authentification(
  state = initialState.authentification,
  action
) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        ...state,
        errors: [],
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        errors: [],
      };
    case userConstants.GET_INFO:
      return {
        ...state,
        isLoggedIn: true,
        user: action.user,
      };
    case userConstants.LOGIN_FAILURE:
      return {
        ...state,
        errors: [...[action.error], ...state.errors],
      };
    case userConstants.LOGIN_SOCIAL_REQUEST:
      return {
        ...state,
        errors: [],
      };
    case userConstants.LOGIN_SOCIAL_FAILURE:
      return {
        ...state,
        errors: [],
      };
    case userConstants.LOGIN_SOCIAL_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        errors: [],
      };
    case userConstants.LOGOUT:
      return initialState.authentification;
    default:
      return state;
  }
}
