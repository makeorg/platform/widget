import { proposalConstants } from '../../constants/proposal';
import { initialState } from '../initialState';

export default function proposal(state = initialState.proposal, action) {
  switch (action.type) {
    case proposalConstants.PROPOSE_TYPING:
      return {
        ...state,
        isTyping: true,
        isSubmitSuccess: false,
        content: action.content,
        length: action.length,
        canSubmit: action.canSubmit,
      };
    case proposalConstants.PROPOSE_REQUEST:
      return {
        ...state,
        isTyping: false,
        questionId: action.questionId,
      };
    case proposalConstants.PROPOSE_SUCCESS:
      return {
        ...state,
        ...initialState.proposal,
        isSubmitSuccess: true,
      };
    case proposalConstants.PROPOSE_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}
