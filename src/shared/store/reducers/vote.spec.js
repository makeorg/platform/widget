import vote from './vote';
import { VOTE_ACTION, UNVOTE_ACTION } from '../../constants/vote';

describe('Vote Reducer', () => {
  it('vote event when previous state is empty', () => {
    const action = {
      type: VOTE_ACTION,
      proposalId: 'zoom',
      votes: ['boo', 'lom'],
    };
    const previousState = {
      votes: [],
    };

    const expectedState = {
      votes: [
        {
          result: ['boo', 'lom'],
          proposalId: 'zoom',
        },
      ],
    };

    expect(vote(previousState, action)).to.eql(expectedState);
  });
  it('vote event when previous state is not empty', () => {
    const action = {
      type: VOTE_ACTION,
      proposalId: 'zoom',
      votes: ['boo', 'lom'],
    };
    const previousState = {
      votes: [
        {
          result: ['foo', 'bar', 'baz'],
          proposalId: 'foo',
        },
      ],
    };

    const expectedState = {
      votes: [
        {
          result: ['foo', 'bar', 'baz'],
          proposalId: 'foo',
        },
        {
          result: ['boo', 'lom'],
          proposalId: 'zoom',
        },
      ],
    };

    expect(vote(previousState, action)).to.eql(expectedState);
  });

  it('unvote event', () => {
    const action = { type: UNVOTE_ACTION, proposalId: 'foo' };
    const previousState = {
      votes: [
        {
          result: ['foo', 'bar', 'baz'],
          proposalId: 'foo',
        },
      ],
    };

    const expectedState = {
      votes: [],
    };

    expect(vote(previousState, action)).to.eql(expectedState);
  });
});
