import {
  PANNEL_RENDER_REGISTER,
  PANNEL_RENDER_LOGIN,
  PANNEL_RENDER_PRESENTATION,
  PANNEL_RENDER_FORGOTPASSWORD,
  PANNEL_CLOSE,
  PANNEL_RENDER_PRIVACY_POLICY,
  PANNEL_QUIT_PRIVACY_POLICY,
} from '../../constants/pannel';
import { initialState } from '../initialState';

export default function config(state = initialState.config, action) {
  switch (action.type) {
    case PANNEL_RENDER_REGISTER:
      return {
        ...state,
        pannelRender: action.type,
        showPannel: true,
      };
    case PANNEL_RENDER_LOGIN:
      return {
        ...state,
        pannelRender: action.type,
        showPannel: true,
      };
    case PANNEL_RENDER_PRESENTATION:
      return {
        ...state,
        pannelRender: action.type,
        showPannel: true,
      };
    case PANNEL_RENDER_FORGOTPASSWORD:
      return {
        ...state,
        pannelRender: action.type,
        showPannel: true,
      };
    case PANNEL_RENDER_PRIVACY_POLICY:
      return {
        ...state,
        pannelRender: action.type,
        extraProps: action.extraProps,
        showPannel: true,
      };
    case PANNEL_QUIT_PRIVACY_POLICY:
      return {
        ...state,
        pannelRender: action.type,
      };
    case PANNEL_CLOSE:
      return {
        ...state,
        extraProps: {},
        showPannel: false,
      };
    default:
      return state;
  }
}
