import * as actionCreators from '../actions/userAction';
import authentification from './authentification';

describe('Authentification reducer', () => {
  describe('Login action reducers', () => {
    it('Login Request', () => {
      const action = actionCreators.loginRequest();
      const previousState = {
        isLoggedIn: false,
        errors: ['foo', 'bar'],
        user: null,
      };

      const expectedState = {
        isLoggedIn: false,
        errors: [],
        user: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });

    it('Login Success', () => {
      const action = actionCreators.loginSuccess({});
      const previousState = {
        isLoggedIn: false,
        errors: ['foo', 'bar'],
        user: null,
      };

      const expectedState = {
        isLoggedIn: true,
        errors: [],
        user: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });

    it('Login Failure', () => {
      const action = actionCreators.loginFailure('fooError');
      const previousState = {
        isLoggedIn: false,
        errors: ['bazError', 'barError'],
        user: null,
      };

      const expectedState = {
        isLoggedIn: false,
        errors: ['fooError', 'bazError', 'barError'],
        user: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });
  });

  describe('Login Social action reducers', () => {
    it('Login Social Request', () => {
      const action = actionCreators.loginSocialRequest();
      const previousState = {
        isLoggedIn: false,
        errors: ['foo', 'bar'],
        user: null,
      };

      const expectedState = {
        isLoggedIn: false,
        errors: [],
        user: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });

    it('Login Social Success', () => {
      const action = actionCreators.loginSocialSuccess({
        access_token: '12345',
        refresh_token: '12345',
      });
      const previousState = {
        isLoggedIn: false,
        errors: ['foo', 'bar'],
        user: null,
      };

      const expectedState = {
        isLoggedIn: true,
        errors: [],
        user: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });

    it('Login Social Failure', () => {
      const action = actionCreators.loginSocialFailure('fooError');
      const previousState = {
        isLoggedIn: false,
        errors: ['bazError', 'barError'],
        user: null,
        token: null,
      };

      const expectedState = {
        isLoggedIn: false,
        errors: [],
        user: null,
        token: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });
  });

  describe('Get user info action reducers', () => {
    it('Get user Info', () => {
      const user = {
        firstname: 'foo',
        lastname: 'bar',
      };
      const action = actionCreators.setUserInfo(user);
      const previousState = {
        isLoggedIn: false,
        errors: ['bazError', 'barError'],
        user: null,
        token: null,
      };

      const expectedState = {
        isLoggedIn: true,
        errors: ['bazError', 'barError'],
        user: user,
        token: null,
      };

      expect(authentification(previousState, action)).to.eql(expectedState);
    });
  });
});
