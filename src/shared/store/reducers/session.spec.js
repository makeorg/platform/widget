import * as actionTypes from '../../constants/session';
import session from './session';

describe('Session Reducer', () => {
  it('set session to expired state', () => {
    const action = { type: actionTypes.EXPIRED_SESSION };
    const previousState = {
      isExpired: false,
    };

    const expectedState = {
      isExpired: true,
    };

    expect(session(previousState, action)).to.eql(expectedState);
  });
});
