import { combineReducers } from 'redux';
import authentification from './authentification';
import registration from './registration';
import forgotPassword from './forgotPassword';
import proposal from './proposal';
import config from './config';
import vote from './vote';
import sequence from './sequence';
import session from './session';

export default combineReducers({
  config,
  authentification,
  registration,
  forgotPassword,
  proposal,
  vote,
  sequence,
  session,
});
