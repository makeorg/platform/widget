import * as actionTypes from '../../constants/sequence';
import sequence from './sequence';

describe('Sequence Reducer', () => {
  it('collapse sequence reducer', () => {
    const action = { type: actionTypes.SEQUENCE_COLLAPSE };
    const previousState = {
      isSequenceCollapsed: false,
    };

    const expectedState = {
      isSequenceCollapsed: true,
    };

    expect(sequence(previousState, action)).to.eql(expectedState);
  });

  it('expand sequence reducer', () => {
    const action = { type: actionTypes.SEQUENCE_EXPAND };
    const previousState = {
      isSequenceCollapsed: false,
    };

    const expectedState = {
      isSequenceCollapsed: false,
    };

    expect(sequence(previousState, action)).to.eql(expectedState);
  });
});
