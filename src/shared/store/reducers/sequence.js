import * as actionTypes from '../../constants/sequence';
import { initialState } from '../initialState';

export default function sequence(state = initialState.sequence, action) {
  switch (action.type) {
    case actionTypes.SEQUENCE_COLLAPSE:
      return {
        ...state,
        isSequenceCollapsed: true,
      };
    case actionTypes.SEQUENCE_EXPAND:
      return {
        ...state,
        isSequenceCollapsed: false,
      };
    default:
      return state;
  }
}
