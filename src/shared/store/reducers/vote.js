import { VOTE_ACTION, UNVOTE_ACTION } from '../../constants/vote';

export default function vote(state = { votes: [] }, action) {
  switch (action.type) {
    case VOTE_ACTION:
      return {
        ...state,
        votes: [
          ...state.votes,
          ...[
            {
              result: action.votes,
              proposalId: action.proposalId,
            },
          ],
        ],
      };
    case UNVOTE_ACTION:
      return {
        votes: state.votes.filter(
          vote => vote.proposalId !== action.proposalId
        ),
      };
    default:
      return state;
  }
}
