import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as actions from './sequenceAction';
import * as actionTypes from '../../constants/sequence';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore();

describe('Sequence Reducer', () => {
  beforeEach(() => {
    store.clearActions();
    fetchMock.reset();
    fetchMock.restore();
  });

  it('Collapse sequence', () => {
    const expectedActions = [{ type: actionTypes.SEQUENCE_COLLAPSE }];

    store.dispatch(actions.sequenceCollapse());
    expect(store.getActions()).to.deep.equal(expectedActions);
  });

  it('Expand sequence', () => {
    const expectedActions = [{ type: actionTypes.SEQUENCE_EXPAND }];

    store.dispatch(actions.sequenceExpand());
    expect(store.getActions()).to.deep.equal(expectedActions);
  });
});
