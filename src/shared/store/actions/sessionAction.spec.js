import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as actions from './sessionAction';
import * as actionTypes from '../../constants/session';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore();

describe('Session Actions', () => {
  beforeEach(() => {
    store.clearActions();
    fetchMock.reset();
    fetchMock.restore();
  });

  it('setExpiredSession', () => {
    const expectedActions = [{ type: actionTypes.EXPIRED_SESSION }];

    store.dispatch(actions.setExpiredSession());
    expect(store.getActions()).to.deep.equal(expectedActions);
  });
});
