import i18next from 'i18next';
import { userConstants } from '../../constants/user';
import { UserService } from '../../../api/UserService';
import { submitProposal } from './proposalAction';
import { closePannel, showPrivacyPolicy } from './pannelAction';
import Tracking from '../../../services/Tracking';
import { storeTokens } from '../../../services/Security';
import ApiService from '../../../api/ApiService';
import { checkPrivacyPolicyDate } from '../../../helpers/userHelper';

export const setUserInfo = user => ({ type: userConstants.GET_INFO, user });
export const loginRequest = user => ({
  type: userConstants.LOGIN_REQUEST,
  user,
});
export const loginSuccess = tokens => {
  storeTokens(tokens.access_token, tokens.refresh_token);
  ApiService.token = tokens.access_token;
  return {
    type: userConstants.LOGIN_SUCCESS,
  };
};
export const loginFailure = error => ({
  type: userConstants.LOGIN_FAILURE,
  error,
});
export const loginSocialRequest = () => ({
  type: userConstants.LOGIN_SOCIAL_REQUEST,
});
export const loginSocialSuccess = tokens => {
  storeTokens(tokens.access_token, tokens.refresh_token);
  ApiService.token = tokens.access_token;
  return {
    type: userConstants.LOGIN_SOCIAL_SUCCESS,
  };
};
export const loginSocialFailure = error => ({
  type: userConstants.LOGIN_SOCIAL_FAILURE,
  error,
});
export const registerRequest = user => ({
  type: userConstants.REGISTER_REQUEST,
  user,
});
export const registerSuccess = user => ({
  type: userConstants.REGISTER_SUCCESS,
  user,
});
export const registerFailure = errors => ({
  type: userConstants.REGISTER_FAILURE,
  errors,
});
export const forgotPasswordRequest = email => ({
  type: userConstants.FORGOT_PASSWORD_REQUEST,
  email,
});
export const forgotPasswordSuccess = () => ({
  type: userConstants.FORGOT_PASSWORD_SUCCESS,
});
export const forgotPasswordFailure = errors => ({
  type: userConstants.FORGOT_PASSWORD_FAILURE,
  errors,
});

const getUser = () => (dispatch, getState) => {
  const { showPannel } = getState().config;
  return UserService.me().then(user => {
    dispatch(setUserInfo(user));
    if (showPannel) {
      dispatch(closePannel());
    }

    Promise.resolve();
  });
};

const login = (email, password, approvePrivacyPolicy) => {
  return (dispatch, getState) => {
    const { canSubmit } = getState().proposal;

    dispatch(loginRequest({ email }));
    UserService.login(email, password, approvePrivacyPolicy)
      .then(tokens => {
        dispatch(loginSuccess(tokens));
        Tracking.trackSigninEmailSuccess();

        return dispatch(getUser()).then(() => {
          if (canSubmit) {
            const { content } = getState().proposal;
            const { question } = getState().config;

            dispatch(submitProposal(content, question.questionId));
          }
        });
      })
      .catch(error => {
        Tracking.trackSigninEmailFailure();
        dispatch(
          loginFailure(i18next.t('authentification.cant_found_account'))
        );
      });
  };
};

const loginSocial = (provider, socialToken, approvePrivacyPolicy) => {
  return (dispatch, getState) => {
    const { canSubmit } = getState().proposal;

    dispatch(loginSocialRequest());
    UserService.loginSocial(provider, socialToken, approvePrivacyPolicy)
      .then(tokens => {
        dispatch(loginSocialSuccess(tokens));
        Tracking.trackAuthenSocialSuccess(provider);

        return dispatch(getUser()).then(() => {
          if (canSubmit) {
            const { content } = getState().proposal;
            const { question } = getState().config;
            dispatch(submitProposal(content, question.questionId));
          }
        });
      })
      .catch(error => {
        Tracking.trackAuthenSocialFailure(provider);
        dispatch(
          loginSocialFailure(i18next.t('authentification.cant_found_account'))
        );
      });
  };
};

const register = user => {
  return (dispatch, getState) => {
    dispatch(registerRequest(user));
    UserService.register(user)
      .then(userResponse => {
        dispatch(registerSuccess(userResponse));
        Tracking.trackSignupEmailSuccess();

        UserService.login(user.email, user.password).then(tokens => {
          dispatch(loginSuccess(tokens));

          return dispatch(getUser()).then(() => {
            const { canSubmit, content } = getState().proposal;
            if (canSubmit) {
              const { question } = getState().config;
              dispatch(submitProposal(content, question.questionId));
            }
          });
        });
      })
      .catch(errors => {
        dispatch(registerFailure(errors));

        Tracking.trackSignupEmailFailure();
      });
  };
};

const forgotPassword = email => {
  return dispatch => {
    dispatch(forgotPasswordRequest(email));
    UserService.forgotPassword(email)
      .then(repsonse => {
        dispatch(forgotPasswordSuccess());
      })
      .catch(errors => {
        const errorMessages =
          errors === 404
            ? [{ message: i18next.t('authentification.cant_found_account') }]
            : errors;
        dispatch(forgotPasswordFailure(errorMessages));
      });
  };
};

const loginWithPrivacyCheck = (email, password, privacyPolicyDate) => {
  return dispatch => {
    UserService.loginPrivacyPolicy(email, password)
      .then(response => {
        return checkPrivacyPolicyDate(
          privacyPolicyDate,
          response,
          () =>
            dispatch(
              showPrivacyPolicy({
                email,
                password,
                isSocial: false,
              })
            ),
          () => dispatch(login(email, password))
        );
      })
      .catch(() => {
        Tracking.trackSigninEmailFailure();
        dispatch(
          loginFailure(i18next.t('authentification.cant_found_account'))
        );
      });
  };
};

const socialLoginWithPrivacyCheck = (provider, token, privacyPolicyDate) => {
  return dispatch => {
    UserService.socialPrivacyPolicy(provider, token)
      .then(response => {
        return checkPrivacyPolicyDate(
          privacyPolicyDate,
          response,
          () =>
            dispatch(
              showPrivacyPolicy({
                provider,
                token,
                isSocial: true,
              })
            ),
          () => dispatch(loginSocial(provider, token))
        );
      })
      .catch(() => {
        Tracking.trackAuthenSocialFailure(provider);
        dispatch(
          loginSocialFailure(i18next.t('authentification.cant_found_account'))
        );
      });
  };
};

export const userActions = {
  getUser,
  login,
  loginSocial,
  register,
  forgotPassword,
  loginWithPrivacyCheck,
  socialLoginWithPrivacyCheck,
};
