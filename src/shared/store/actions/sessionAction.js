import * as actionTypes from '../../constants/session';

export const setSessionExpirationDate = date => ({
  type: actionTypes.SET_EXPIRATION_DATE,
  date,
});

export const setExpiredSession = () => dispatch =>
  dispatch({ type: actionTypes.EXPIRED_SESSION });
