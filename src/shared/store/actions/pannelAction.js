import {
  PANNEL_RENDER_LOGIN,
  PANNEL_RENDER_REGISTER,
  PANNEL_RENDER_PRESENTATION,
  PANNEL_RENDER_FORGOTPASSWORD,
  PANNEL_CLOSE,
  PANNEL_RENDER_PRIVACY_POLICY,
  PANNEL_QUIT_PRIVACY_POLICY,
} from '../../constants/pannel';
import Tracking from '../../../services/Tracking';

export const showLogin = () => {
  return dispatch => {
    Tracking.trackDisplaySigninForm();
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_RENDER_LOGIN };
  }
};

export const showRegister = () => {
  return dispatch => {
    Tracking.trackDisplaySignupForm();
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_RENDER_REGISTER };
  }
};

export const showPresentation = () => {
  return dispatch => {
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_RENDER_PRESENTATION };
  }
};

export const showForgotPassword = () => {
  return dispatch => {
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_RENDER_FORGOTPASSWORD };
  }
};

export const showPrivacyPolicy = extraProps => {
  return dispatch => {
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_RENDER_PRIVACY_POLICY, extraProps };
  }
};

export const quitPrivacyPolicy = () => {
  return dispatch => {
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_QUIT_PRIVACY_POLICY };
  }
};

export const closePannel = () => {
  return dispatch => {
    dispatch(action());
  };

  function action() {
    return { type: PANNEL_CLOSE };
  }
};
