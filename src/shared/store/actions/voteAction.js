import { VOTE_ACTION, UNVOTE_ACTION } from '../../constants/vote';

export const vote = (proposalId, votes) => {
  return dispatch => {
    dispatch(action(proposalId, votes));
  };

  function action(proposalId, votes) {
    return { type: VOTE_ACTION, proposalId, votes };
  }
};

export const unvote = proposalId => {
  return dispatch => {
    dispatch(action(proposalId));
  };

  function action(proposalId) {
    return { type: UNVOTE_ACTION, proposalId };
  }
};
