import * as actionTypes from '../../constants/sequence';

export const sequenceCollapse = () => dispatch =>
  dispatch({ type: actionTypes.SEQUENCE_COLLAPSE });
export const sequenceExpand = () => dispatch =>
  dispatch({ type: actionTypes.SEQUENCE_EXPAND });
