import { proposalConstants, getBaitText } from '../../constants/proposal';
import ProposalService from '../../../api/ProposalService';
import Tracking from '../../../services/Tracking';

export const proposeTyping = (content, length, canSubmit) => ({
  type: proposalConstants.PROPOSE_TYPING,
  content,
  length,
  canSubmit,
});
export const proposeSuccess = proposalId => ({
  type: proposalConstants.PROPOSE_SUCCESS,
  proposalId,
});
export const proposeRequest = (content, questionId) => ({
  type: proposalConstants.PROPOSE_REQUEST,
  content,
  questionId,
});
export const proposeFailure = error => ({
  type: proposalConstants.PROPOSE_FAILURE,
  error,
});

export const typingProposal = (content, length, canSubmit) => dispatch => {
  dispatch(proposeTyping(content, length, canSubmit));
};

export const submitProposal = content => (dispatch, getState) => {
  const { isLoggedIn } = getState().authentification;
  const { question } = getState().config;

  if (!isLoggedIn) {
    dispatch(proposeRequest(content, question.questionId));
    return Promise.resolve();
  }

  if (!content || !question.questionId) {
    return Promise.resolve();
  }

  const proposalContent = getBaitText() + content;
  return ProposalService.propose(proposalContent, question.questionId)
    .then(proposalId => {
      dispatch(proposeSuccess(proposalId));

      Tracking.trackDisplayProposalSubmitValidation();
    })
    .catch(error => {
      dispatch(proposeFailure(error));
    });
};
