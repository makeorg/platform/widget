import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import { UserService } from '../../api/UserService';

export const authenticationState = async () => {
  let authentificationState;
  try {
    const user = await UserService.me();
    authentificationState = {
      isLoggedIn: !!user,
      user,
      errors: [],
    };
  } catch (error) {
    authentificationState = {
      isLoggedIn: false,
      user: undefined,
      errors: [],
    };
  }

  return authentificationState;
};

export const configureStore = (initialState = {}) => {
  const composeEnhancers =
    (typeof window !== 'undefined' &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
    compose;

  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(thunk))
  );
};
