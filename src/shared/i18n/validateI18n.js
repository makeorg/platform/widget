const path = require('path');
const fs = require('fs');

const APP_TRAD_DIR = path.resolve(__dirname, 'static');
const countriesLanguages = {
  AT: ['de'],
  BE: ['nl', 'fr'],
  BG: ['bg'],
  CY: ['el'],
  CZ: ['cs'],
  DE: ['de'],
  DK: ['da'],
  EE: ['et'],
  ES: ['es'],
  FI: ['fi'],
  FR: ['fr'],
  GB: ['en'],
  GR: ['el'],
  HR: ['hr'],
  HU: ['hu'],
  IE: ['en'],
  IT: ['it'],
  LT: ['lt'],
  LU: ['fr'],
  LV: ['lv'],
  MT: ['mt'],
  NL: ['nl'],
  PL: ['pl'],
  PT: ['pt'],
  RO: ['ro'],
  SE: ['sv'],
  SI: ['sl'],
  SK: ['sk'],
};
let hasError = false;

const writeJson = object => JSON.stringify(object, null, 2);

const countries = Object.keys(countriesLanguages);

let promises = [];
countries.forEach(country => {
  countriesLanguages[country].forEach(language => {
    promises.push(
      new Promise((resolve, reject) => {
        const appTradFilePath = `${APP_TRAD_DIR}/${language}-${country}.json`;
        try {
          const data = fs.readFileSync(appTradFilePath, 'utf8');
          if (!data) {
            console.error(
              `error when reading ${appTradFilePath} => ${readError}`
            );
            reject();
          }

          const trads = JSON.parse(data);
          console.info(`App Trad for ${language}-${country} is valid!`);
          resolve();
        } catch (error) {
          console.log(`error in content of ${appTradFilePath} => ${error}`);
          reject();
        }
      })
    );
  });
});

Promise.all(promises)
  .then(() => console.info('all i18n files are valid'))
  .catch(() => {
    console.log('Error on validate i18n files');
    process.exit(1);
  });
