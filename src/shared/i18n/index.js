import i18next from 'i18next';
import deAT from './static/de-AT.json';
import frBE from './static/fr-BE.json';
import nlBE from './static/nl-BE.json';
import bgBG from './static/bg-BG.json';
import elCY from './static/el-CY.json';
import csCZ from './static/cs-CZ.json';
import deDE from './static/de-DE.json';
import daDK from './static/da-DK.json';
import etEE from './static/et-EE.json';
import esES from './static/es-ES.json';
import fiFI from './static/fi-FI.json';
import frFR from './static/fr-FR.json';
import enGB from './static/en-GB.json';
import elGR from './static/el-GR.json';
import hrHR from './static/hr-HR.json';
import huHU from './static/hu-HU.json';
import enIE from './static/en-IE.json';
import itIT from './static/it-IT.json';
import ltLT from './static/lt-LT.json';
import frLU from './static/fr-LU.json';
import lvLV from './static/lv-LV.json';
import mtMT from './static/mt-MT.json';
import nlNL from './static/nl-NL.json';
import plPL from './static/pl-PL.json';
import ptPT from './static/pt-PT.json';
import roRO from './static/ro-RO.json';
import svSE from './static/sv-SE.json';
import slSI from './static/sl-SI.json';
import skSK from './static/sk-SK.json';
import { env } from '../constants/env';

i18next.init({
  lng: 'fr-FR',
  debug: env.isDev(),
  resources: {
    'de-AT': { translation: deAT },
    'fr-BE': { translation: frBE },
    'nl-BE': { translation: nlBE },
    'bg-BG': { translation: bgBG },
    'el-CY': { translation: elCY },
    'cs-CZ': { translation: csCZ },
    'de-DE': { translation: deDE },
    'da-DK': { translation: daDK },
    'et-EE': { translation: etEE },
    'es-ES': { translation: esES },
    'fi-FI': { translation: fiFI },
    'fr-FR': { translation: frFR },
    'en-GB': { translation: enGB },
    'el-GR': { translation: elGR },
    'hr-HR': { translation: hrHR },
    'hu-HU': { translation: huHU },
    'en-IE': { translation: enIE },
    'it-IT': { translation: itIT },
    'lt-LT': { translation: ltLT },
    'fr-LU': { translation: frLU },
    'lv-LV': { translation: lvLV },
    'mt-MT': { translation: mtMT },
    'nl-NL': { translation: nlNL },
    'pl-PL': { translation: plPL },
    'pt-PT': { translation: ptPT },
    'ro-RO': { translation: roRO },
    'sv-SE': { translation: svSE },
    'sl-SI': { translation: slSI },
    'sk-SK': { translation: skSK },
  },
});

export default i18next;
