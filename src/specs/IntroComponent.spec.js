import React from 'react';
import TagService from '../api/TagService';
import IntroComponent, {
  Intro,
  TagLink,
  Title,
  IntroTitle,
} from '../components/IntroComponent';

describe('IntroComponent', () => {
  const fooTag = { label: 'Foo' };
  const barTag = { label: 'Bar' };
  const promiseFoo = Promise.resolve(fooTag);
  const promiseBar = Promise.resolve(barTag);

  before(() => {
    const methoStub = sinon.stub(TagService, 'getById');
    methoStub.withArgs('foo-id').returns(promiseFoo);
    methoStub.withArgs('bar-id').returns(promiseBar);
  });

  after(() => {
    TagService.getById.restore();
  });

  it('renders the IntroComponent wrapper', () => {
    const wrapper = shallow(
      <IntroComponent
        tagsIds={['foo-id', 'bar-id']}
        title={'my widget'}
        themeColor={'foo'}
        canPropose={true}
      />
    );

    promiseBar.then(() => {
      expect(wrapper.state().tags).to.have.length(1);
      expect(wrapper.state().tags).to.include(fooTag);
      expect(wrapper.find(Intro)).to.have.length(1);
      expect(wrapper.find(Title)).to.have.length(1);
      expect(wrapper.find(IntroTitle)).to.have.length(1);
      expect(wrapper.find(IntroTitle).get(0).props.tags).to.equal('Foo');
      expect(wrapper.find(IntroTitle).get(0).props.title).to.equal('my widget');
    });
  });

  it('renders the IntroTitle Wrapper with tags', () => {
    const wrapper = shallow(<IntroTitle title={''} tags={'Foo'} />);
    expect(wrapper.find(TagLink)).to.have.length(1);
    expect(wrapper.find(TagLink).get(0).props.children).to.equals('Foo');
  });

  it('renders the IntroTitle Wrapper with title', () => {
    const wrapper = shallow(<IntroTitle title={'my widget'} tags={'foo-id'} />);

    expect(wrapper.text()).to.equals('my widget');
  });
});
