import React from 'react';

import ProposalQualificationComponent, {
  doQualifying,
  doUnqualifying,
} from '../components/ProposalQualificationComponent';
import Qualification from '../components/ProposalQualification';

import {
  VOTE_AGREE_KEY,
  QUALIFICATION_LIKEIT_KEY,
  QUALIFICATION_DOABLE_KEY,
  QUALIFICATION_PLATITUDEAGREE_KEY,
} from '../constants/enum';

describe('ProposalQualification Local State', () => {
  it('should append userQualifications in state', () => {
    const userQualifications = [
      { qualificationKey: 'bar' },
      { qualificationKey: 'baz' },
    ];
    const qualification = {
      qualificationKey: 'foo',
    };
    const state = { userQualifications: userQualifications };
    const newState = doQualifying(state, qualification);

    expect(newState.userQualifications).to.have.lengthOf(3);
    expect(newState.userQualifications).to.include(qualification);
  });
});

describe('ProposalQualification Component', () => {
  const qualificationsResult = [
    { qualificationKey: QUALIFICATION_LIKEIT_KEY },
    { qualificationKey: QUALIFICATION_DOABLE_KEY },
    { qualificationKey: QUALIFICATION_PLATITUDEAGREE_KEY },
  ];

  it('renders the Qualification wrapper', () => {
    const wrapper = shallow(
      <ProposalQualificationComponent
        qualificationsResult={qualificationsResult}
        voteKey={VOTE_AGREE_KEY}
        proposalKey={'bar'}
        proposalId={'foo-id'}
        cardIndex={1}
        color={'bar'}
      />
    );

    expect(wrapper.find(Qualification)).to.have.length(1);
    expect(wrapper.find(Qualification.QualificationListItem)).to.have.length(3);
    expect(wrapper.find(Qualification.QualificationButton)).to.have.length(3);
    expect(wrapper.find(Qualification.QualificationLabel)).to.have.length(3);
    expect(wrapper.find(Qualification.QualificationCounter)).to.have.length(3);
  });

  it('passes all props to QualificationButton wrapper', () => {
    const wrapper = shallow(
      <ProposalQualificationComponent
        qualificationsResult={qualificationsResult}
        voteKey={VOTE_AGREE_KEY}
        proposalKey={'bar'}
        proposalId={'foo-id'}
        cardIndex={1}
        color={'bar'}
      />
    );
    const QualificationButtonWrapper = wrapper.find(
      Qualification.QualificationButton
    );

    const QualificationButtonProps = QualificationButtonWrapper.first().props();

    expect(QualificationButtonProps.qualified).to.equal(false);
    expect(QualificationButtonProps.color).to.equal('bar');
    assert.isFunction(QualificationButtonProps.onClick);
  });
});
