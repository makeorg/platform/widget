module.exports = {
  apps: [
    {
      name: 'make-widget',
      script: 'bin/start',
      exec_mode: 'cluster',
      instances: 8,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
